package com.freightsoftware.adapter;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.freightsoftware.Constants.Constants;
import com.freightsoftware.Interface.RecyclerViewItemClickListener;
import com.freightsoftware.R;
import com.freightsoftware.model.GsonFreightDetailsList;

import java.util.List;

/**
 * Created by haresh on 2/27/18.
 */

public class FreightDetailsAdapter extends RecyclerView.Adapter<FreightDetailsAdapter.ViewHolder> {

    private static final String TAG = "FreightDetailsAdapter";
    private List<GsonFreightDetailsList> list;
    private FragmentActivity activity;
    private RecyclerViewItemClickListener listener = null;


    public FreightDetailsAdapter(FragmentActivity activity, List<GsonFreightDetailsList> list) {
        this.list = list;
        this.activity = activity;
    }

    @Override
    public FreightDetailsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ll_details, parent, false);
        FreightDetailsAdapter.ViewHolder viewHolder = new FreightDetailsAdapter.ViewHolder(view);



        return viewHolder;
    }

    @Override
    public void onBindViewHolder(FreightDetailsAdapter.ViewHolder holder, int position) {
        final GsonFreightDetailsList result = list.get(position);

        holder.tv_qty.setText(result.getQuantity() +  " " + result.getPiecesType());
        holder.tv_weight.setText(result.getWeight());
        holder.tv_length.setText(result.getL());
        holder.tv_width.setText(result.getW());
        holder.tv_height.setText(result.getH());
        holder.tv_dim.setText(result.getDimFactor());
        holder.tv_details.setText(result.getPieceDescription());


    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tv_qty,tv_weight,tv_length,tv_width,tv_height,tv_dim,tv_details;
        Button btn_delete;

        public ViewHolder(View itemView) {
            super(itemView);

            tv_qty = itemView.findViewById(R.id.tv_qty);
            tv_weight = itemView.findViewById(R.id.tv_weight);
            tv_length = itemView.findViewById(R.id.tv_length);
            tv_width = itemView.findViewById(R.id.tv_width);
            tv_height = itemView.findViewById(R.id.tv_height);
            tv_dim = itemView.findViewById(R.id.tv_dim);
            tv_details = itemView.findViewById(R.id.tv_details);
            btn_delete = itemView.findViewById(R.id.btn_delete);

            itemView.setOnClickListener(this);
            btn_delete.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            if (v == itemView) {
                if (listener != null) {
                    listener.onItemClick(getAdapterPosition(), Constants.ITEM_CLICK, v);
                }
            } else if (v == btn_delete) {
                if (listener != null) {
                    listener.onItemClick(getAdapterPosition(), Constants.ITEM_DELETE, v);
                }
            }
        }


    }

    public void setOnRecyclerViewItemClickListener(RecyclerViewItemClickListener recyclerViewItemClickListener) {
        this.listener = recyclerViewItemClickListener;
    }
}
