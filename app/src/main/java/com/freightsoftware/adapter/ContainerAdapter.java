package com.freightsoftware.adapter;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.freightsoftware.Constants.Constants;
import com.freightsoftware.Interface.RecyclerViewItemClickListener;
import com.freightsoftware.R;
import com.freightsoftware.model.GsonContainerList;

import java.util.List;

/**
 * Created by haresh on 2/22/18.
 */

public class ContainerAdapter extends RecyclerView.Adapter<ContainerAdapter.ViewHolder> {
    private static final String TAG = "ContainerAdapter";
    private List<GsonContainerList> list;
    private FragmentActivity activity;
    private RecyclerViewItemClickListener listener = null;


    public ContainerAdapter(FragmentActivity activity, List<GsonContainerList> list) {
        this.list = list;
        this.activity = activity;
    }

    @Override
    public ContainerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ll_home_list, parent, false);
        ContainerAdapter.ViewHolder viewHolder = new ContainerAdapter.ViewHolder(view);


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ContainerAdapter.ViewHolder holder, int position) {
        final GsonContainerList result = list.get(position);

        holder.tv_cotainer_code.setText("# "+result.getClientCN());
        holder.tv_move_type.setText(result.getMoveType());
        holder.tv_type.setText(result.getShipmentType());
        holder.tv_details.setText(result.getFreightDetails());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tv_cotainer_code,tv_move_type,tv_type,tv_details;

        public ViewHolder(View itemView) {
            super(itemView);

            tv_cotainer_code = itemView.findViewById(R.id.tv_cotainer_code);
            tv_move_type = itemView.findViewById(R.id.tv_move_type);
            tv_type = itemView.findViewById(R.id.tv_type);
            tv_details = itemView.findViewById(R.id.tv_details);

            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            if (v == itemView) {
                if (listener != null) {
                    listener.onItemClick(getAdapterPosition(), Constants.ITEM_CLICK, v);
                }
            }
        }


    }

    public void setOnRecyclerViewItemClickListener(RecyclerViewItemClickListener recyclerViewItemClickListener) {
        this.listener = recyclerViewItemClickListener;
    }


}


