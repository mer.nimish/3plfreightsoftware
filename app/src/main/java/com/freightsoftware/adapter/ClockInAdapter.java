package com.freightsoftware.adapter;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.freightsoftware.Constants.Constants;
import com.freightsoftware.Interface.RecyclerViewItemClickListener;
import com.freightsoftware.R;
import com.freightsoftware.Utility.MyUtils;
import com.freightsoftware.model.GsonClockInList;
import com.freightsoftware.model.GsonClockInList;

import java.util.List;

/**
 * Created by haresh on 2/16/18.
 */

public class ClockInAdapter extends RecyclerView.Adapter<ClockInAdapter.ViewHolder> {
    private static final String TAG = "ClockInAdapter";
    private List<GsonClockInList> list;
    private FragmentActivity activity;
    private RecyclerViewItemClickListener listener = null;


    public ClockInAdapter(FragmentActivity activity, List<GsonClockInList> list) {
        this.list = list;
        this.activity = activity;
    }

    @Override
    public ClockInAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ll_item_clock, parent, false);
        ClockInAdapter.ViewHolder viewHolder = new ClockInAdapter.ViewHolder(view);


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ClockInAdapter.ViewHolder holder, int position) {
        final GsonClockInList result = list.get(position);

        holder.tv_out_time.setText(MyUtils.CovertDate(result.getClockOut()));
        holder.tv_in_time.setText(MyUtils.CovertDate(result.getClockIn()));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tv_out_time,tv_in_time;

        public ViewHolder(View itemView) {
            super(itemView);

            tv_in_time = itemView.findViewById(R.id.tv_in_time);
            tv_out_time = itemView.findViewById(R.id.tv_out_time);
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            if (v == itemView) {
                if (listener != null) {
                    listener.onItemClick(getAdapterPosition(), Constants.ITEM_CLICK, v);
                }
            }
        }


    }

    public void setOnRecyclerViewItemClickListener(RecyclerViewItemClickListener recyclerViewItemClickListener) {
        this.listener = recyclerViewItemClickListener;
    }


}
