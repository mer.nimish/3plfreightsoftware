package com.freightsoftware.adapter;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.freightsoftware.Constants.Constants;
import com.freightsoftware.Interface.RecyclerViewItemClickListener;
import com.freightsoftware.R;
import com.freightsoftware.model.ModelStatic;

import java.util.List;

/**
 * Created by haresh on 2/16/18.
 */

public class AdapterStatic extends RecyclerView.Adapter<AdapterStatic.ViewHolder> {
    private static final String TAG = "AdapterStatic";
    private List<ModelStatic> list;
    private FragmentActivity activity;
    private RecyclerViewItemClickListener listener = null;


    public AdapterStatic(FragmentActivity activity, List<ModelStatic> list) {
        this.list = list;
        this.activity = activity;
    }

    @Override
    public AdapterStatic.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ll_home_list, parent, false);
        AdapterStatic.ViewHolder viewHolder = new AdapterStatic.ViewHolder(view);


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AdapterStatic.ViewHolder holder, int position) {
        final ModelStatic result = list.get(position);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {



        public ViewHolder(View itemView) {
            super(itemView);

           itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            if (v == itemView) {
                if (listener != null) {
                    listener.onItemClick(getAdapterPosition(), Constants.ITEM_CLICK, v);
                }
            }
        }


    }

    public void setOnRecyclerViewItemClickListener(RecyclerViewItemClickListener recyclerViewItemClickListener) {
        this.listener = recyclerViewItemClickListener;
    }


}

