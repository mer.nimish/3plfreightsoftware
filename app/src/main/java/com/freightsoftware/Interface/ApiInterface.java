package com.freightsoftware.Interface;

import com.freightsoftware.model.GsonClockInList;
import com.freightsoftware.model.GsonCompleted;
import com.freightsoftware.model.GsonContainerList;
import com.freightsoftware.model.GsonDeleteDims;
import com.freightsoftware.model.GsonDetails;
import com.freightsoftware.model.GsonDocList;
import com.freightsoftware.model.GsonFreightDetails;
import com.freightsoftware.model.GsonFreightDetailsList;
import com.freightsoftware.model.GsonLogin;
import com.freightsoftware.model.GsonNotesType;
import com.freightsoftware.model.GsonSaveFcm;
import com.freightsoftware.model.GsonSelectConatinerChasis;
import com.freightsoftware.model.GsonSelectEmptyCotainer;
import com.freightsoftware.model.GsonSelectWaitingTime;
import com.freightsoftware.model.GsonShipmentStatusList;
import com.freightsoftware.model.GsonTerminalList;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("genericdriverservice.asmx/driverLogin")
    Call<GsonLogin> getJSONLogin(@FieldMap Map<String, String> user);

    @FormUrlEncoded
    @POST("genericdriverservice.asmx/shipmentList")
    Call<List<GsonContainerList>> getContainerList(@FieldMap Map<String, String> user);

    @FormUrlEncoded
    @POST("genericdriverservice.asmx/searchShipmentList")
    Call<List<GsonContainerList>> getSearchContainerList(@FieldMap Map<String, String> user);

    @FormUrlEncoded
    @POST("genericdriverservice.asmx/shipmentDetails")
    Call<GsonDetails> getContainerDetails(@FieldMap Map<String, String> user);

    @FormUrlEncoded
    @POST("genericdriverservice.asmx/drpShipmentStatus")
    Call<List<GsonShipmentStatusList>> getShipStatusList(@FieldMap Map<String, String> user);

    @FormUrlEncoded
    @POST("genericdriverservice.asmx/drpTerminals")
    Call<List<GsonTerminalList>> getTerminalList(@FieldMap Map<String, String> user);

    @FormUrlEncoded
    @POST("genericdriverservice.asmx/drpDocuments")
    Call<List<GsonDocList>> getDocList(@FieldMap Map<String, String> user);

    @FormUrlEncoded
    @POST("genericdriverservice.asmx/completeShipment")
    Call<GsonCompleted> getJSONCompleted(@FieldMap Map<String, String> user);

    @FormUrlEncoded
    @POST("genericdriverservice.asmx/updateShipmentStatus")
    Call<GsonCompleted> getUpdateShipmentStatus(@FieldMap Map<String, String> user);

    @FormUrlEncoded
    @POST("genericdriverservice.asmx/selectContainerChassis")
    Call<GsonSelectConatinerChasis> getSelectContinerChasis(@FieldMap Map<String, String> user);

    @FormUrlEncoded
    @POST("genericdriverservice.asmx/updateContainerChassis")
    Call<GsonCompleted> getUpdateContinerStatus(@FieldMap Map<String, String> user);

    @FormUrlEncoded
    @POST("genericdriverservice.asmx/selecEmptyContainer")
    Call<GsonSelectEmptyCotainer> selectEmptyContainer(@FieldMap Map<String, String> user);


    @FormUrlEncoded
    @POST("genericdriverservice.asmx/updateEmptyContainer")
    Call<GsonCompleted> getUpdateContainer(@FieldMap Map<String, String> user);

    @FormUrlEncoded
    @POST("genericdriverservice.asmx/uploadShipmentDocument")
    Call<GsonCompleted> updateCaptureImage(@FieldMap Map<String, String> user);

    @FormUrlEncoded
    @POST("genericdriverservice.asmx/uploadShipmentSignature")
    Call<GsonCompleted> updateCaptureSignature(@FieldMap Map<String, String> user);


    @FormUrlEncoded
    @POST("genericdriverservice.asmx/drpShipmentNotes")
    Call<List<GsonNotesType>> getNotesList(@FieldMap Map<String, String> user);

    @FormUrlEncoded
    @POST("genericdriverservice.asmx/saveShipmentNotes")
    Call<GsonCompleted> uploadNotes(@FieldMap Map<String, String> user);


    @FormUrlEncoded
    @POST("genericdriverservice.asmx/dimsList")
    Call<List<GsonFreightDetailsList>> getFreightList(@FieldMap Map<String, String> user);

    @FormUrlEncoded
    @POST("genericdriverservice.asmx/addDims")
    Call<GsonDeleteDims> addDims(@FieldMap Map<String, String> user);

    @FormUrlEncoded
    @POST("genericdriverservice.asmx/deleteDims")
    Call<GsonDeleteDims> deleteDims(@FieldMap Map<String, String> user);

    @FormUrlEncoded
    @POST("genericdriverservice.asmx/drpFreightDetails")
    Call<List<GsonFreightDetails>> getFreightDetails(@FieldMap Map<String, String> user);

    @FormUrlEncoded
    @POST("genericdriverservice.asmx/selectWaitingTime")
    Call<GsonSelectWaitingTime> selectWaitingTime(@FieldMap Map<String, String> user);

    @FormUrlEncoded
    @POST("genericdriverservice.asmx/updateWaitingTime")
    Call<GsonCompleted> updateWaitingTime(@FieldMap Map<String, String> user);

    @FormUrlEncoded
    @POST("genericdriverservice.asmx/driverLogOut")
    Call<GsonCompleted> driverLogout(@FieldMap Map<String, String> user);

    //@Multipart
    //@POST("genericdriverservice.asmx/updateWaitingTime")
    //Call<GsonCompleted> updateWaitingTime(@PartMap() Map<String, RequestBody> user);

    @FormUrlEncoded
    @POST("genericdriverservice.asmx/driverClockInList")
    Call<List<GsonClockInList>> getClockInList(@FieldMap Map<String, String> user);

    @FormUrlEncoded
    @POST("genericdriverservice.asmx/driverClockIn")
    Call<GsonCompleted> driverClockIn(@FieldMap Map<String, String> user);

    @FormUrlEncoded
    @POST("genericdriverservice.asmx/saveDriverLatLon")
    Call<GsonCompleted> saveDriverLatLon(@FieldMap Map<String, String> user);

    @FormUrlEncoded
    @POST("genericdriverservice.asmx/shipmentLog")
    Call<GsonCompleted> saveShipmentLog(@FieldMap Map<String, String> user);

    @FormUrlEncoded
    @POST("genericdriverservice.asmx/saveDriverGeoFence")
    Call<GsonCompleted> saveDriverGeoFence(@FieldMap Map<String, String> user);

    @FormUrlEncoded
    @POST("genericdriverservice.asmx/saveFCMUserToken")
    Call<GsonSaveFcm> saveFcmUserAPI(@FieldMap Map<String, String> user);

}