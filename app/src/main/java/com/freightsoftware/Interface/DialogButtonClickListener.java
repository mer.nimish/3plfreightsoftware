package com.freightsoftware.Interface;


public interface DialogButtonClickListener {
    void onPositiveButtonClick();
    void onNegativeButtonClick();
}
