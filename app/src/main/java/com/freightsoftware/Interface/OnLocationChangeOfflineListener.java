package com.freightsoftware.Interface;


public interface OnLocationChangeOfflineListener {
    void onNotificationGetListener(Double lat, Double lng);
}
