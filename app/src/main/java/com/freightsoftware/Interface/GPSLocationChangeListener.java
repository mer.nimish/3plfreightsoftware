package com.freightsoftware.Interface;


import android.location.Location;

public interface GPSLocationChangeListener {
    void onLocationChangeListener(Location location);
}
