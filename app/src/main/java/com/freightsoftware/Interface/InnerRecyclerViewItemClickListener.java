package com.freightsoftware.Interface;

import android.view.View;

/**
 * Created by siddharth on 12/20/2017.
 */
public interface InnerRecyclerViewItemClickListener {
    public void onInnerItemClick(int hPosition, int position, int flag, View view);
}
