package com.freightsoftware.Constants;

import android.os.Environment;

import com.google.android.gms.maps.model.LatLng;

import java.util.HashMap;

/**
 * Created by Umesh on 7/26/2017.
 */
public class Constants {

    public static final String APP_PdfKG = "com.freightsoftware";
    public static final String AUTH_USERNAME = "";
    public static final String AUTH_PASSWORD = "";

    public static final String DATE_YYYY_MM_DD_FORMAT = "yyyy-MM-dd";
    public static final String DATE_FORMAT = "M/d/yyyy hh:mm:ss a";
    public static final String DATE_FORMAT_INVERSE = "hh:mm:ss a M/d/yyyy";
    public static final int ITEM_CLICK = 10;
    public static final int ITEM_DELETE = 20;
    public static final int REQUEST_EXTERNAL_STORAGE = 1;
    public static final int REQUEST_CODE_STORAGE_PERMISSION = 114;
    public static final int REQUEST_CODE_CAMERA = 117;
    public static final int ACCESS_FINE_LOCATION = 110;
    public static final long LOCATION_INTERVAL = 1000 * 60 * 1;
    public static final long FASTEST_LOCATION_INTERVAL = 1000 * 60 * 1;

    //crop

    public static String image = "image";
    public static String FLAG_IS_SQUARE = "flag_is_square";
    public static int FLAG_CROP = 1314;

    public static final String App_ROOT_FOLDER = Environment
            .getExternalStorageDirectory()
            .getAbsolutePath() + "/" + ".CAM";

    //images

    public static final String IMAGE_FILE_NAME_PREFIX = "IMG_CAM" + "_X" + ".jpg";

    public static final String IMAGE_CROP_RATIO_WIDTH = "image_crop_ratio_width";
    public static final String IMAGE_CROP_RATIO_HEIGHT = "image_crop_ratio_height";

    /*pagination*/
    public static String pagination_last_offset = "-1";


    //    login
    public static String userName = "userName";
    public static String userPassword = "userPassword";
    public static String user_id = "user_id";
    public static String driver_id = "driver_id";
    public static String driverID = "driverID";
    public static String data = "data";
    public static String company_id = "company_id";
    public static String username = "username";
    public static String userEmail = "userEmail";


    //    update shipment
    public static String company_ID = "company_ID";
    public static String controlNumber = "controlNumber";

    //    completed
    public static String id = "id";

    //    update Shipment Status
    public static String drpStatusID = "drpStatusID";

    //    update container chasis
    public static String containerNumber = "containerNumber";
    public static String chassisNumber = "chassisNumber";
    public static String sealNumber = "sealNumber";

    //  Empty Container
    public static String terminal = "terminal";

    //Capture Image
    public static String drpDocumentTypeID = "drpDocumentTypeID";
    public static String drpDocumentType = "drpDocumentType";
    public static String userID = "userID";
    public static String base64Image = "base64Image";
    public static String fileSize = "fileSize";
    public static String publicStatus = "publicStatus";
    public static String contactName = "contactName";


    public static String freightShipment = "freightShipment";
    public static String userNotes = "userNotes";


    //    freight details
    public static String cn = "cn";


    public static String dimFactor = "dimFactor";
    public static String pieces = "pieces";
    public static String txtDesc = "txtDesc";
    public static String l = "l";
    public static String w = "w";
    public static String h = "h";
    public static String drpID = "drpID";
    public static String drpDesc = "drpDesc";
    public static String hazMat = "hazMat";
    public static String statedWeight = "statedWeight";
    public static String pickupFrom = "pickupFrom";
    public static String pickupTo = "pickupTo";
    public static String deliveryFrom = "deliveryFrom";
    public static String deliveryTo = "deliveryTo";
    public static String returnFrom = "returnFrom";
    public static String returnTo = "returnTo";
    public static String text_refresh = "refresh";


    public static final HashMap<String, LatLng> LANDMARKS = new HashMap<String, LatLng>();
    public static String latitude = "latitude";
    public static String longitude = "longitude";
    public static String logID = "logID";
    public static String arrivedFromLocation = "116";
    public static String leaveFromLocation = "117";
    public static String arrivedtoLocation = "118";
    public static String leavetoLocation = "119";
    public static String notifyID = "notifyID";
    public static String searchTerm = "searchTerm";
    public static String logType = "logType";
    public static String fcm_registration_id = "fcm_registration_id";
    public static String message = "message";
    public static String deviceType = "deviceType";
    public static String token = "token";
    public static String myFunction = "myFunction";


    static {
        // San Francisco International Airport.
        LANDMARKS.put("Feed Details", new LatLng(21.235152, 72.858983));

        // Googleplex.
        LANDMARKS.put("Left", new LatLng(21.2422629, 72.858290));
//        LANDMARKS.put("Left", new LatLng(33.958449,-117.452223));

        // Test
        LANDMARKS.put("LeftDown", new LatLng(21.2151529, 72.828980));

        LANDMARKS.put("Leftup", new LatLng(21.2251529, 72.928980));
//        LANDMARKS.put("LeftDown", new LatLng(33.957575,-117.445970));
    }

    public static String push = "push";
    public static int fromPush = 2;
    public static int fromSplash = 1;

    public static final int REQUEST_CODE_GPS_SETTING = 129;

    public static final long GEOFENCE_EXPIRATION_IN_HOURS = 12;
    public static final long GEOFENCE_EXPIRATION_IN_MILLISECONDS = GEOFENCE_EXPIRATION_IN_HOURS * 60 * 60 * 1000;
    public static final float GEOFENCE_RADIUS_IN_METERS = 0.1f;


    public final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    public static String DEVICE_TYPE_ANDROID = "1";

}
