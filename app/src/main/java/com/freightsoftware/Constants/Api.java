package com.freightsoftware.Constants;


import com.freightsoftware.App;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Umesh on 7/6/2017.
 */
public class Api {



//    public static String BASE_URL = "https://devapp.3plfreightsoftware.com/misc/";
    public static String BASE_URL = "https://app.3plfreightsoftware.com/misc/";


    static OkHttpClient okHttpClient = UnsafeOkHttpClient.getUnsafeOkHttpClient();
    public static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(Api.BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
}









