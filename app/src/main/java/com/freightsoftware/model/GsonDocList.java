package com.freightsoftware.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by haresh on 2/22/18.
 */

public class GsonDocList {

    @SerializedName("ID")
    @Expose
    private Integer iD;
    @SerializedName("docName")
    @Expose
    private String docName;

    public Integer getID() {
        return iD;
    }

    public void setID(Integer iD) {
        this.iD = iD;
    }

    public String getDocName() {
        return docName;
    }

    public void setDocName(String docName) {
        this.docName = docName;
    }
}
