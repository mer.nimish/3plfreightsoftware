package com.freightsoftware.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by haresh on 2/27/18.
 */

public class GsonDeleteDims {

    @SerializedName("dimID")
    @Expose
    private String dimID;

    public String getDimID() {
        return dimID;
    }

    public void setDimID(String dimID) {
        this.dimID = dimID;
    }
}
