package com.freightsoftware.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by haresh on 2/22/18.
 */

public class GsonTerminalList {

    @SerializedName("ID")
    @Expose
    private Integer iD;
    @SerializedName("terminal")
    @Expose
    private String terminal;

    public Integer getID() {
        return iD;
    }

    public void setID(Integer iD) {
        this.iD = iD;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }
}
