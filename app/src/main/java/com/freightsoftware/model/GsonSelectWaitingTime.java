package com.freightsoftware.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by haresh on 2/28/18.
 */

public class GsonSelectWaitingTime {

    @SerializedName("pickupFrom")
    @Expose
    private String pickupFrom;
    @SerializedName("pickupTo")
    @Expose
    private String pickupTo;
    @SerializedName("deliveryFrom")
    @Expose
    private String deliveryFrom;
    @SerializedName("deliveryTo")
    @Expose
    private String deliveryTo;
    @SerializedName("returnFrom")
    @Expose
    private String returnFrom;
    @SerializedName("returnTo")
    @Expose
    private String returnTo;

    public String getPickupFrom() {
        return pickupFrom;
    }

    public void setPickupFrom(String pickupFrom) {
        this.pickupFrom = pickupFrom;
    }

    public String getPickupTo() {
        return pickupTo;
    }

    public void setPickupTo(String pickupTo) {
        this.pickupTo = pickupTo;
    }

    public String getDeliveryFrom() {
        return deliveryFrom;
    }

    public void setDeliveryFrom(String deliveryFrom) {
        this.deliveryFrom = deliveryFrom;
    }

    public String getDeliveryTo() {
        return deliveryTo;
    }

    public void setDeliveryTo(String deliveryTo) {
        this.deliveryTo = deliveryTo;
    }

    public String getReturnFrom() {
        return returnFrom;
    }

    public void setReturnFrom(String returnFrom) {
        this.returnFrom = returnFrom;
    }

    public String getReturnTo() {
        return returnTo;
    }

    public void setReturnTo(String returnTo) {
        this.returnTo = returnTo;
    }
}
