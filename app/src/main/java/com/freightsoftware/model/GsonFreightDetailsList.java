package com.freightsoftware.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by haresh on 2/27/18.
 */

public class GsonFreightDetailsList {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("quantity")
    @Expose
    private String quantity;
    @SerializedName("piecesType")
    @Expose
    private String piecesType;
    @SerializedName("weight")
    @Expose
    private String weight;
    @SerializedName("l")
    @Expose
    private String l;
    @SerializedName("w")
    @Expose
    private String w;
    @SerializedName("h")
    @Expose
    private String h;
    @SerializedName("dimFactor")
    @Expose
    private String dimFactor;
    @SerializedName("pieceDescription")
    @Expose
    private String pieceDescription;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPiecesType() {
        return piecesType;
    }

    public void setPiecesType(String piecesType) {
        this.piecesType = piecesType;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getL() {
        return l;
    }

    public void setL(String l) {
        this.l = l;
    }

    public String getW() {
        return w;
    }

    public void setW(String w) {
        this.w = w;
    }

    public String getH() {
        return h;
    }

    public void setH(String h) {
        this.h = h;
    }

    public String getDimFactor() {
        return dimFactor;
    }

    public void setDimFactor(String dimFactor) {
        this.dimFactor = dimFactor;
    }

    public String getPieceDescription() {
        return pieceDescription;
    }

    public void setPieceDescription(String pieceDescription) {
        this.pieceDescription = pieceDescription;
    }
}
