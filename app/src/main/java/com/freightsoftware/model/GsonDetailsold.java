package com.freightsoftware.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by haresh on 2/26/18.
 */

public class GsonDetailsold {

    @SerializedName("tblFPShipmentDriverID")
    @Expose
    private Integer tblFPShipmentDriverID;
    @SerializedName("controlNumber")
    @Expose
    private Integer controlNumber;
    @SerializedName("clientCN")
    @Expose
    private Integer clientCN;
    @SerializedName("transactionType")
    @Expose
    private String transactionType;
    @SerializedName("containerNumber")
    @Expose
    private String containerNumber;
    @SerializedName("freightDetails")
    @Expose
    private String freightDetails;
    @SerializedName("moveType")
    @Expose
    private String moveType;
    @SerializedName("bookingNumber")
    @Expose
    private String bookingNumber;
    @SerializedName("pin")
    @Expose
    private String pin;
    @SerializedName("sealNumber")
    @Expose
    private String sealNumber;
    @SerializedName("railReleaseNumber")
    @Expose
    private String railReleaseNumber;
    @SerializedName("railLocation")
    @Expose
    private String railLocation;
    @SerializedName("shipmentInstructions")
    @Expose
    private String shipmentInstructions;
    @SerializedName("steamShipline")
    @Expose
    private String steamShipline;
    @SerializedName("terminalAppointmentNum")
    @Expose
    private String terminalAppointmentNum;
    @SerializedName("fromName")
    @Expose
    private String fromName;
    @SerializedName("fromAddresss")
    @Expose
    private String fromAddresss;
//    @SerializedName("fromDate")
//    @Expose
//    private String fromDate;

    @SerializedName("fromDate")
    @Expose
    private Object fromDateo;
//    @SerializedName("fromTime")
//    @Expose
//    private String fromTime;

    @SerializedName("fromTime")
    @Expose
    private Object fromTimeo;
    @SerializedName("fromCoordinates")
    @Expose
    private String fromCoordinates;
    @SerializedName("toName")
    @Expose
    private String toName;
    @SerializedName("toAddress")
    @Expose
    private String toAddress;
//    @SerializedName("toDate")
//    @Expose
//    private String toDate;
//    @SerializedName("toTime")
//    @Expose
//    private String toTime;
    @SerializedName("toCoordinates")
    @Expose
    private String toCoordinates;
    @SerializedName("fromRef")
    @Expose
    private String fromRef;
    @SerializedName("toRef")
    @Expose
    private String toRef;
    @SerializedName("stopNumber")
    @Expose
    private String stopNumber;
    @SerializedName("moveTypeID")
    @Expose
    private String moveTypeID;

    @SerializedName("toDate")
    @Expose
    private Object toDateo;
    @SerializedName("toTime")
    @Expose
    private Object toTimeo;

    public Integer getTblFPShipmentDriverID() {
        return tblFPShipmentDriverID;
    }

    public void setTblFPShipmentDriverID(Integer tblFPShipmentDriverID) {
        this.tblFPShipmentDriverID = tblFPShipmentDriverID;
    }

    public Integer getControlNumber() {
        return controlNumber;
    }

    public void setControlNumber(Integer controlNumber) {
        this.controlNumber = controlNumber;
    }

    public Integer getClientCN() {
        return clientCN;
    }

    public void setClientCN(Integer clientCN) {
        this.clientCN = clientCN;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getContainerNumber() {
        return containerNumber;
    }

    public void setContainerNumber(String containerNumber) {
        this.containerNumber = containerNumber;
    }

    public String getFreightDetails() {
        return freightDetails;
    }

    public void setFreightDetails(String freightDetails) {
        this.freightDetails = freightDetails;
    }

    public String getMoveType() {
        return moveType;
    }

    public void setMoveType(String moveType) {
        this.moveType = moveType;
    }

    public String getBookingNumber() {
        return bookingNumber;
    }

    public void setBookingNumber(String bookingNumber) {
        this.bookingNumber = bookingNumber;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getSealNumber() {
        return sealNumber;
    }

    public void setSealNumber(String sealNumber) {
        this.sealNumber = sealNumber;
    }

    public String getRailReleaseNumber() {
        return railReleaseNumber;
    }

    public void setRailReleaseNumber(String railReleaseNumber) {
        this.railReleaseNumber = railReleaseNumber;
    }

    public String getRailLocation() {
        return railLocation;
    }

    public void setRailLocation(String railLocation) {
        this.railLocation = railLocation;
    }

    public String getShipmentInstructions() {
        return shipmentInstructions;
    }

    public void setShipmentInstructions(String shipmentInstructions) {
        this.shipmentInstructions = shipmentInstructions;
    }

    public String getSteamShipline() {
        return steamShipline;
    }

    public void setSteamShipline(String steamShipline) {
        this.steamShipline = steamShipline;
    }

    public String getTerminalAppointmentNum() {
        return terminalAppointmentNum;
    }

    public void setTerminalAppointmentNum(String terminalAppointmentNum) {
        this.terminalAppointmentNum = terminalAppointmentNum;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public String getFromAddresss() {
        return fromAddresss;
    }

    public void setFromAddresss(String fromAddresss) {
        this.fromAddresss = fromAddresss;
    }

//    public String getFromDate() {
//        return fromDate;
//    }
//
//    public void setFromDate(String fromDate) {
//        this.fromDate = fromDate;
//    }
//
//
//
//    public String getFromTime() {
//        return fromTime;
//    }
//
//    public void setFromTime(String fromTime) {
//        this.fromTime = fromTime;
//    }

    public String getFromCoordinates() {
        return fromCoordinates;
    }

    public void setFromCoordinates(String fromCoordinates) {
        this.fromCoordinates = fromCoordinates;
    }

    public String getToName() {
        return toName;
    }

    public void setToName(String toName) {
        this.toName = toName;
    }

    public String getToAddress() {
        return toAddress;
    }

    public void setToAddress(String toAddress) {
        this.toAddress = toAddress;
    }

//    public String getToDate() {
//        return toDate;
//    }
//
//    public void setToDate(String toDate) {
//        this.toDate = toDate;
//    }
//
//    public String getToTime() {
//        return toTime;
//    }
//
//    public void setToTime(String toTime) {
//        this.toTime = toTime;
//    }

    public String getToCoordinates() {
        return toCoordinates;
    }

    public void setToCoordinates(String toCoordinates) {
        this.toCoordinates = toCoordinates;
    }

    public String getFromRef() {
        return fromRef;
    }

    public void setFromRef(String fromRef) {
        this.fromRef = fromRef;
    }

    public String getToRef() {
        return toRef;
    }

    public void setToRef(String toRef) {
        this.toRef = toRef;
    }

    public String getStopNumber() {
        return stopNumber;
    }

    public void setStopNumber(String stopNumber) {
        this.stopNumber = stopNumber;
    }

    public String getMoveTypeID() {
        return moveTypeID;
    }

    public void setMoveTypeID(String moveTypeID) {
        this.moveTypeID = moveTypeID;
    }

    //new
    public Object getFromDateo() {
        return fromDateo;
    }

    public void setFromDateo(Object fromDateo) {
        this.fromDateo = fromDateo;
    }

    public Object getFromTimeo() {
        return fromTimeo;
    }

    public void setFromTimeo(Object fromTimeo) {
        this.fromTimeo = fromTimeo;
    }

    public Object getToDateo() {
        return toDateo;
    }

    public void setToDateo(Object toDateo) {
        this.toDateo = toDateo;
    }

    public Object getToTimeo() {
        return toTimeo;
    }

    public void setToTime(Object toTime) {
        this.toTimeo = toTimeo;
    }
}
