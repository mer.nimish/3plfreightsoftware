package com.freightsoftware.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by haresh on 2/23/18.
 */

public class GsonSelectEmptyCotainer {
    @SerializedName("containerNumber")
    @Expose
    private String containerNumber;
    @SerializedName("chassisNumber")
    @Expose
    private String chassisNumber;
    @SerializedName("terminal")
    @Expose
    private String terminal;

    public String getContainerNumber() {
        return containerNumber;
    }

    public void setContainerNumber(String containerNumber) {
        this.containerNumber = containerNumber;
    }

    public String getChassisNumber() {
        return chassisNumber;
    }

    public void setChassisNumber(String chassisNumber) {
        this.chassisNumber = chassisNumber;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }
}
