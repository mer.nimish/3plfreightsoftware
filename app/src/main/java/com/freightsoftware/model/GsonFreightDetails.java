package com.freightsoftware.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by haresh on 2/28/18.
 */

public class GsonFreightDetails {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("drpTypeDesc")
    @Expose
    private String drpTypeDesc;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDrpTypeDesc() {
        return drpTypeDesc;
    }

    public void setDrpTypeDesc(String drpTypeDesc) {
        this.drpTypeDesc = drpTypeDesc;
    }
}
