package com.freightsoftware.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by haresh on 3/1/18.
 */

public class GsonClockInList {

    @SerializedName("clockIn")
    @Expose
    private String clockIn;
    @SerializedName("clockOut")
    @Expose
    private String clockOut;

    public String getClockIn() {
        return clockIn;
    }

    public void setClockIn(String clockIn) {
        this.clockIn = clockIn;
    }

    public String getClockOut() {
        return clockOut;
    }

    public void setClockOut(String clockOut) {
        this.clockOut = clockOut;
    }
}
