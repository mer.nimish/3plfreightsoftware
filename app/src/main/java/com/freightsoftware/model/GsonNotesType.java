package com.freightsoftware.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by haresh on 2/27/18.
 */

public class GsonNotesType {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("noteType")
    @Expose
    private String noteType;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNoteType() {
        return noteType;
    }

    public void setNoteType(String noteType) {
        this.noteType = noteType;
    }
}
