package com.freightsoftware.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by haresh on 2/22/18.
 */

public class GsonContainerList {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("priority")
    @Expose
    private Integer priority;
    @SerializedName("controlNumber")
    @Expose
    private Integer controlNumber;
    @SerializedName("clientCN")
    @Expose
    private Integer clientCN;
    @SerializedName("freightShipment")
    @Expose
    private Boolean freightShipment;
    @SerializedName("freightDetails")
    @Expose
    private String freightDetails;
    @SerializedName("moveType")
    @Expose
    private String moveType;
    @SerializedName("shipmentType")
    @Expose
    private String shipmentType;
    @SerializedName("shipmentLeg")
    @Expose
    private Integer shipmentLeg;
    @SerializedName("stopNumber")
    @Expose
    private Integer stopNumber;
    @SerializedName("toCoordinates")
    @Expose
    private String toCoordinates;
    @SerializedName("fromCoordinates")
    @Expose
    private String fromCoordinates;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Integer getControlNumber() {
        return controlNumber;
    }

    public void setControlNumber(Integer controlNumber) {
        this.controlNumber = controlNumber;
    }

    public Integer getClientCN() {
        return clientCN;
    }

    public void setClientCN(Integer clientCN) {
        this.clientCN = clientCN;
    }

    public Boolean getFreightShipment() {
        return freightShipment;
    }

    public void setFreightShipment(Boolean freightShipment) {
        this.freightShipment = freightShipment;
    }

    public String getFreightDetails() {
        return freightDetails;
    }

    public void setFreightDetails(String freightDetails) {
        this.freightDetails = freightDetails;
    }

    public String getMoveType() {
        return moveType;
    }

    public void setMoveType(String moveType) {
        this.moveType = moveType;
    }

    public String getShipmentType() {
        return shipmentType;
    }

    public void setShipmentType(String shipmentType) {
        this.shipmentType = shipmentType;
    }

    public Integer getShipmentLeg() {
        return shipmentLeg;
    }

    public void setShipmentLeg(Integer shipmentLeg) {
        this.shipmentLeg = shipmentLeg;
    }

    public Integer getStopNumber() {
        return stopNumber;
    }

    public void setStopNumber(Integer stopNumber) {
        this.stopNumber = stopNumber;
    }

    public String getToCoordinates() {
        return toCoordinates;
    }

    public void setToCoordinates(String toCoordinates) {
        this.toCoordinates = toCoordinates;
    }

    public String getFromCoordinates() {
        return fromCoordinates;
    }

    public void setFromCoordinates(String fromCoordinates) {
        this.fromCoordinates = fromCoordinates;
    }
}
