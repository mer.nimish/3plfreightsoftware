package com.freightsoftware.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by haresh on 2/23/18.
 */

public class GsonSelectConatinerChasis {

    @SerializedName("container")
    @Expose
    private String container;
    @SerializedName("chassisNumber")
    @Expose
    private String chassisNumber;
    @SerializedName("sealNumber")
    @Expose
    private String sealNumber;

    public String getContainer() {
        return container;
    }

    public void setContainer(String container) {
        this.container = container;
    }

    public String getChassisNumber() {
        return chassisNumber;
    }

    public void setChassisNumber(String chassisNumber) {
        this.chassisNumber = chassisNumber;
    }

    public String getSealNumber() {
        return sealNumber;
    }

    public void setSealNumber(String sealNumber) {
        this.sealNumber = sealNumber;
    }

}
