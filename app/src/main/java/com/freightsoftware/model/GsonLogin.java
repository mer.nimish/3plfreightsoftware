package com.freightsoftware.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by haresh on 2/21/18.
 */

public class GsonLogin {

    @SerializedName("company_ID")
    @Expose
    private String companyID;
    @SerializedName("userID")
    @Expose
    private String userID;
    @SerializedName("driverID")
    @Expose
    private String driverID;
    @SerializedName("userName")
    @Expose
    private String userName;

    public String getCompanyID() {
        return companyID;
    }

    public void setCompanyID(String companyID) {
        this.companyID = companyID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getDriverID() {
        return driverID;
    }

    public void setDriverID(String driverID) {
        this.driverID = driverID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
