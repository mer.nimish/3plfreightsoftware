package com.freightsoftware;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;

import com.freightsoftware.Constants.Constants;
import com.freightsoftware.activity.Home;
import com.freightsoftware.activity.Login;

public class Splash extends AppCompatActivity {

    private static final int SPLASH_WAIT = 3000;
    private static final String TAG = "Splash";
    Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.RegisterActivityForBugsense(Splash.this);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        BGThread();
    }

    /**
     * splash thread upto 3s
     */
    private void BGThread() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {


//                if (App.Utils.getString(Constants.user_id).equalsIgnoreCase("")) {
//                    Intent i = new Intent(Splash.this, Login.class);
//                    startActivity(i);
//                    finish();
//                } else {
                if ((App.Utils.getString(Constants.user_id)).equalsIgnoreCase("")) {
                    Intent i1 = new Intent(Splash.this, Login.class);
                    startActivity(i1);
                    finish();
                }
                else
                {
                    Intent i1 = new Intent(Splash.this, Home.class);
                    i1.putExtra(Constants.push, Constants.fromSplash);
                    startActivity(i1);
                    finish();
                }

//                }
            }
        }, SPLASH_WAIT);
    }
}
