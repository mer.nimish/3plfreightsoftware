package com.freightsoftware.firebase;

/**
 * Created by AndroidBash on 20-Aug-16.
 */

import com.freightsoftware.Constants.Constants;
import com.freightsoftware.Utility.LogUtil;
import com.freightsoftware.Utility.MyUtils;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

//import com.google.firebase.database.DatabaseReference;
//import com.google.firebase.database.FirebaseDatabase;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        // Saving reg id to shared preferences
        storeRegIdInPref(refreshedToken);

        // sending reg id to your server
        sendRegistrationToServer(refreshedToken);

        // Notify UI that registration has completed, so the progress indicator can be hidden.
//        Intent registrationComplete = new Intent(Constants.REGISTRATION_COMPLETE);
//        registrationComplete.putExtra("token", refreshedToken);
//        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);

        //update token when refresh token. not first time.
        if (!MyUtils.getString(Constants.fcm_registration_id).equalsIgnoreCase(""))
            updateFCMToken();
    }

    private void updateFCMToken() {

    }

    private void sendRegistrationToServer(final String token) {
        // sending gcm token to server
        LogUtil.Print(TAG, "sendRegistrationToServer: " + token);
    }

    private void storeRegIdInPref(String token) {
        LogUtil.Print(TAG, "REGISTER TOKEN IN PRE..: " + token);
        MyUtils.putString(Constants.fcm_registration_id, token);
    }

}