package com.freightsoftware.firebase;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.freightsoftware.App;
import com.freightsoftware.Constants.Constants;
import com.freightsoftware.R;
import com.freightsoftware.Utility.LogUtil;
import com.freightsoftware.activity.Home;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import java.util.Random;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "FirebaseMessageService";
    public static boolean isChatScreenOpen = false;
    public static String chat_screen_id = "";
    public static int notifyID = 9001;
    private static int SPLASH_WAIT = 850;
    private PendingIntent resultPendingIntent;
    Bitmap bitmap;
    JSONObject customdata = new JSONObject();
    Handler handler = new Handler();
    private String type;
    private String message;
    private String weddng_side;
    private String wedding_id;
    private String conversation_id;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        LogUtil.Print(TAG, "From: " + remoteMessage.getFrom());
        if (remoteMessage == null)
            return;
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            SendPush(remoteMessage, 1);
        } else {
            LogUtil.Print("remoteMessage", "Null");
            SendPush(remoteMessage, 0);
        }
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Title: " + remoteMessage.getNotification().getTitle());
        }
    }

    private void SendPush(RemoteMessage remoteMessage, int from) {
        String content = "";
        if (from == 1) {
            try {
                content = remoteMessage.getData().get(Constants.myFunction);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            content = remoteMessage.getNotification().getBody();
        }
        Intent intent = new Intent(this, Home.class);
        intent.putExtra(Constants.push, Constants.fromPush);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder mNotifyBuilder = null;
        NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= 26) {
            String channelId = "com.freightsoftware";
            CharSequence channelName = "3PLFreightSoftware";
            int importance = NotificationManager.IMPORTANCE_LOW;
            NotificationChannel adminChannel = new NotificationChannel(channelId, channelName, importance);
            adminChannel.enableLights(true);
            adminChannel.setLightColor(Color.RED);
            adminChannel.enableVibration(true);
            adminChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            mNotificationManager.createNotificationChannel(adminChannel);
            mNotifyBuilder = new NotificationCompat.Builder(this, channelId)
                    .setSmallIcon(R.drawable.ic_launcher_transparent)
                    .setBadgeIconType(R.drawable.ic_launcher_transparent)
                    .setContentTitle(this.getResources().getString(R.string.app_name))
                    .setAutoCancel(true).setContentIntent(contentIntent)
                    .setNumber(1)
                    .setColor(255)
                    .setContentText(content)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(content))
                    .setWhen(System.currentTimeMillis());
        } else if (Build.VERSION.SDK_INT >= 21) {
            mNotifyBuilder = new NotificationCompat.Builder(this)
                    .setContentTitle(this.getResources().getString(R.string.app_name))
                    .setContentText(content)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(content))
                    .setSmallIcon(R.mipmap.ic_launcher_transparent)
                    .setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.mipmap.ic_launcher_round))
                    .setColor(Color.RED);
        } else {
            mNotifyBuilder = new NotificationCompat.Builder(this)
                    .setContentTitle(this.getResources().getString(R.string.app_name))
                    .setContentText(content)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(content))
                    .setSmallIcon(R.mipmap.ic_launcher);
        }
        mNotifyBuilder.setContentIntent(contentIntent);
        int defaults = 0;
        defaults = defaults | Notification.DEFAULT_LIGHTS;
        defaults = defaults | Notification.DEFAULT_VIBRATE;
        defaults = defaults | Notification.DEFAULT_SOUND;
        mNotifyBuilder.setDefaults(defaults);
        if (content != null) {
            mNotifyBuilder.setContentText(content);
        }
        mNotifyBuilder.setAutoCancel(true);
        notifyID = +1;
        App.Utils.putInt(Constants.notifyID, notifyID);
        mNotifyBuilder.setAutoCancel(true);
        Random random = new Random();
        int randomNumber = random.nextInt(9999 - 1000) + 1000;
        Log.e("RandomNumber", "" + randomNumber);
        mNotificationManager.notify(randomNumber, mNotifyBuilder.build());
    }

}