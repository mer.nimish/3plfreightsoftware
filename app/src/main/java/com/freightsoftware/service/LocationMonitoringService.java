package com.freightsoftware.service;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.freightsoftware.App;
import com.freightsoftware.Constants.Api;
import com.freightsoftware.Constants.Constants;
import com.freightsoftware.Interface.ApiInterface;
import com.freightsoftware.R;
import com.freightsoftware.Utility.LogUtil;
import com.freightsoftware.Utility.MyUtils;
import com.freightsoftware.model.GsonCompleted;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LocationMonitoringService extends Service {

    private static final String TAG = "LocationMonitoringService";
    private static final int LOCATION_INTERVAL = 5000;
    private static final float LOCATION_DISTANCE = 10f;
    LocationListener[] mLocationListeners = new LocationListener[]{
            new LocationListener(LocationManager.GPS_PROVIDER),
            new LocationListener(LocationManager.NETWORK_PROVIDER)
    };
    DatabaseReference ref;
    GeoFire geoFire;
    private LocationManager mLocationManager = null;
    private Double lastLat = 0.0, lastLng = 0.0;

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @SuppressLint("LongLogTag")
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand");
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onCreate() {
        Log.e(TAG, "onCreate");
        ref = FirebaseDatabase.getInstance().getReference("MyLocation");
        geoFire = new GeoFire(ref);
        initializeLocationManager();
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[1]);
        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "network provider does not exist, " + ex.getMessage());
        }
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[0]);
        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "gps provider does not exist " + ex.getMessage());
        }
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        super.onDestroy();
        if (mLocationManager != null) {
            for (int i = 0; i < mLocationListeners.length; i++) {
                try {
                    mLocationManager.removeUpdates(mLocationListeners[i]);
                } catch (Exception ex) {
                    Log.i(TAG, "fail to remove location listners, ignore", ex);
                }
            }
        }
    }

    @SuppressLint("LongLogTag")
    private void initializeLocationManager() {
        Log.e(TAG, "initializeLocationManager");
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
    }

    private void displayLocation(Double lastLat, Double lastLng) {
        if (lastLat != 0.0 && lastLng != 0.0) {


            //Update to Firebase
            geoFire.setLocation(MyUtils.getString(Constants.driverID), new GeoLocation(lastLat, lastLng),
                    new GeoFire.CompletionListener() {
                        @Override
                        public void onComplete(String key, DatabaseError error) {
                            //Add marker
                            LogUtil.Print(TAG, "SaveDriverLatLon_params in firebase");

                        }
                    });


            Log.d("GeoFences", String.format("Your location was changes : %f / %f", lastLat, lastLng));

        } else {
            Log.d("GeoFences", "can not get your location");
        }
    }

    private void SaveDriverLatLon(final double latitude, final double longitude) {
        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.driverID, App.Utils.getString(Constants.driverID));
            params.put(Constants.latitude, "" + latitude);
            params.put(Constants.longitude, "" + longitude);
//            params.put(Constants.driverID, App.Utils.getString(Constants.driverID));

            LogUtil.Print(TAG, "SaveDriverLatLon_params" + params);

            Call<GsonCompleted> call = request.saveDriverLatLon(params);
//            cpb.setVisibility(View.VISIBLE);
            call.enqueue(new Callback<GsonCompleted>() {
                @Override
                public void onResponse(Call<GsonCompleted> call, Response<GsonCompleted> response) {
                    LogUtil.Print("response", response.toString());


                    GsonCompleted gson = response.body();
//                    cpb.setVisibility(View.GONE);
                    if (response.body() == null) {
                        MyUtils.ShowAlert(getApplicationContext(), "Falied to Save Driver Location", getResources().getString(R.string.app_name));
                    } else {
                        lastLat = latitude;
                        lastLng = longitude;
                        LogUtil.Print(TAG, "SaveDriverLatLon_params true");
//                        MyUtils.makeToast("success");
                    }
                }

                @Override
                public void onFailure(Call<GsonCompleted> call, Throwable t) {
                    LogUtil.Print("Error", t.getMessage());
//                    cpb.setVisibility(View.GONE);
                    LogUtil.Print(TAG, "Failure ");
                }
            });

        } else {

            MyUtils.ShowAlert(getApplicationContext(), getResources().getString(R.string.text_internet), getResources().getString(R.string.app_name));
        }
    }

    private class LocationListener implements android.location.LocationListener {
        Location mLastLocation;

        @SuppressLint("LongLogTag")
        public LocationListener(String provider) {
            Log.e(TAG, "LocationListener " + provider);
            mLastLocation = new Location(provider);
        }

        @SuppressLint("LongLogTag")
        @Override
        public void onLocationChanged(Location location) {
            Log.e(TAG, "onLocationChanged: " + location);
            mLastLocation.set(location);
            if (mLastLocation != null) {
                if (!MyUtils.getString(Constants.driverID).equals("")) {
                    if (lastLat != location.getLatitude() && lastLng != location.getLongitude()) {
                        //check app is background
                        lastLat = location.getLatitude();
                        lastLng = location.getLongitude();
                        SaveDriverLatLon(lastLat, lastLng);
                    }
                }
            }
            displayLocation(lastLat, lastLng);
        }

        @SuppressLint("LongLogTag")
        @Override
        public void onProviderDisabled(String provider) {
            Log.e(TAG, "onProviderDisabled: " + provider);
        }

        @SuppressLint("LongLogTag")
        @Override
        public void onProviderEnabled(String provider) {
            Log.e(TAG, "onProviderEnabled: " + provider);
        }

        @SuppressLint("LongLogTag")
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.e(TAG, "onStatusChanged: " + provider);
        }
    }

}