package com.freightsoftware.service;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.freightsoftware.App;
import com.freightsoftware.Constants.Api;
import com.freightsoftware.Constants.Constants;
import com.freightsoftware.Interface.ApiInterface;
import com.freightsoftware.Interface.GPSLocationChangeListener;
import com.freightsoftware.R;
import com.freightsoftware.Utility.GPSTracker;
import com.freightsoftware.Utility.LogUtil;
import com.freightsoftware.Utility.MyUtils;
import com.freightsoftware.model.GsonCompleted;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UploadTrackingDataToFirebaseBGService extends Service implements GPSLocationChangeListener {
    // TAG
    private static final String TAG = "GPSTracker UploadTrackingDataToFirebaseBGService";
    // This is the object that receives interactions from clients. See
    // RemoteService for a more complete example.

    private final IBinder mBinder = new UploadBinder();

    private Double lastLat = 0.0, lastLng = 0.0;
    private List<LatLng> latLngList = new ArrayList<>();
    private GeoFire geoFire;

    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;

    private DatabaseReference ref;
    private String userId = "emp" + App.Utils.getString(Constants.user_id);

    @Override
    public void onLocationChangeListener(Location location) {
        LogUtil.Print(TAG, "onLocationChanged Location: " + location);
        if (lastLat != location.getLatitude() && lastLng != location.getLongitude()) {
            //check app is background
            lastLat = location.getLatitude();
            lastLng = location.getLongitude();
            updateLatLng(location);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // TODO Auto-generated method stub
        LogUtil.Print(TAG, "onStartCommand ...");
        super.onStartCommand(intent, flags, startId);
        // We want this service to continue running until it is explicitly stopped, so return sticky.
        return START_STICKY;
    }

    @Override
    public boolean stopService(Intent name) {
        // TODO Auto-generated method stub
        LogUtil.Print(TAG, "stopService ...");
        GPSTracker.setOnGPSLocationChangeListener(null);
        return super.stopService(name);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        LogUtil.Print(TAG, "Service creating");
        GPSTracker.setOnGPSLocationChangeListener(this);
        ref = FirebaseDatabase.getInstance().getReference("MyLocation");
        geoFire = new GeoFire(ref);
        setUpFirebase();
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        LogUtil.Print(TAG, "Service destroying ...");

        //----------------auto start if user kill-----------------------//
        //sendBroadcast(new Intent("YouWillNeverKillMe"));
        //----------------auto start if user kill-----------------------//

    }

    /**
     * set up FireBase
     */
    private void setUpFirebase() {

    }

    private void updateLatLng(final Location location) {
        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.driverID, App.Utils.getString(Constants.driverID));
            params.put(Constants.latitude, "" + location.getLatitude());
            params.put(Constants.longitude, "" + location.getLongitude());
//            params.put(Constants.driverID, App.Utils.getString(Constants.driverID));

            LogUtil.Print("SaveDriverLatLon_params", "" + params);

            Call<GsonCompleted> call = request.saveDriverLatLon(params);
//            cpb.setVisibility(View.VISIBLE);
            call.enqueue(new Callback<GsonCompleted>() {
                @Override
                public void onResponse(Call<GsonCompleted> call, Response<GsonCompleted> response) {
                    LogUtil.Print("response", response.toString());
                    GsonCompleted gson = response.body();
//                    cpb.setVisibility(View.GONE);
                    if (response.body() == null) {
                        MyUtils.ShowAlert(getApplicationContext(), "Falied to Save Driver Location", getResources().getString(R.string.app_name));
                    } else {
                        LogUtil.Print(TAG, "SaveDriverLatLon_params success");
                        /*MyUtils.makeToast("Location Updated\n"
                                + location.getLatitude() + "\n"
                                + location.getLongitude());*/
                        Log.d("TAG_saveDriverLatLon", "lat: " + location.getLatitude() + ", lng: " + location.getLongitude());
                    }
                }

                @Override
                public void onFailure(Call<GsonCompleted> call, Throwable t) {
                    LogUtil.Print("Error", t.getMessage());
//                    cpb.setVisibility(View.GONE);
                    LogUtil.Print(TAG, "Failure ");
                }
            });

        } else {
            MyUtils.ShowAlert(getApplicationContext(), getResources().getString(R.string.text_internet), getResources().getString(R.string.app_name));
        }

        if (location != null) {
            final double latitude = location.getLatitude();
            final double longitude = location.getLongitude();

            //Update to Firebase
            geoFire.setLocation(MyUtils.getString(Constants.driverID), new GeoLocation(latitude, longitude),
                    new GeoFire.CompletionListener() {
                        @Override
                        public void onComplete(String key, DatabaseError error) {
                            //Add marker
                            LogUtil.Print(TAG, "SaveDriverLatLon_params in FireBase");
                        }
                    });

            Log.d("GeoFences", String.format("Your location was changes : %f / %f", latitude, longitude));
        } else {
            Log.d("GeoFences", "can not get your location");
        }
    }

    /**
     * Class for clients to access. Because we know this service always runs in
     * the same process as its clients, we don't need to deal with IPC.
     */
    public class UploadBinder extends Binder {
        UploadTrackingDataToFirebaseBGService getService() {
            return UploadTrackingDataToFirebaseBGService.this;
        }
    }

}