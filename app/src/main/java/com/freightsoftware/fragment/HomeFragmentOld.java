package com.freightsoftware.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.freightsoftware.App;
import com.freightsoftware.Constants.Api;
import com.freightsoftware.Constants.Constants;
import com.freightsoftware.Interface.ApiInterface;
import com.freightsoftware.Interface.RecyclerViewItemClickListener;
import com.freightsoftware.Interface.RefreshListener;
import com.freightsoftware.R;
import com.freightsoftware.Utility.LogUtil;
import com.freightsoftware.Utility.MyUtils;
import com.freightsoftware.Utility.NavigatorManager;
import com.freightsoftware.activity.Details;
import com.freightsoftware.activity.Home;
import com.freightsoftware.activity.ShipmentComplete;
import com.freightsoftware.adapter.ContainerAdapter;
import com.freightsoftware.model.GsonContainerList;
import com.freightsoftware.model.ModelStatic;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragmentOld extends Fragment implements View.OnClickListener, RecyclerViewItemClickListener, RefreshListener, TextView.OnEditorActionListener {

    public static final HashMap<String, LatLng> LANDMARKS = new HashMap<String, LatLng>();
    private static final String TAG = "HomeFragment";
    public static ImageView iv_drawer;
    public static List<ModelStatic> list = new ArrayList<>();
    public static List<GsonContainerList> list_container = new ArrayList<>();
    RecyclerView rv_home;
    EditText et_search;
    TextView tv_err_msg;
    ContainerAdapter Adp;
    ProgressBar cpb;
    String userId;
    /*header View*/
    private TextView tv_title;
    private LinearLayout lv_main_header;
    private RecyclerView.LayoutManager mLayoutManager;

    public HomeFragmentOld() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        HeaderView(rootView);
        findViewById(rootView);
//        if (list == null || list.size() == 0) {
//            AddMenu();
//        }
        main();

        if ((App.Utils.getString(Constants.user_id)).equalsIgnoreCase("")) {
            userId = getArguments().getString(Constants.user_id);
//            MyUtils.makeToast("ID:--"+userId);
        }

        ShipmentComplete.onRefreshClickListener(this);
//        UpdateShipmentStatus.onRefreshClickListener(this);
//        UpdateContainerStatus.onRefreshClickListener(this);
//        EmptyContainer.onRefreshClickListener(this);
//        CaptureSignature.onRefreshClickListener(this);
//        CaptureImage.onRefreshClickListener(this);
//        Notes.onRefreshClickListener(this);
        callContainerAPI();
        return rootView;
    }


    private void HeaderView(View rootView) {
        lv_main_header = (LinearLayout) rootView.findViewById(R.id.lv_main_header);
        tv_title = (TextView) rootView.findViewById(R.id.tv_title);

        tv_title.setVisibility(View.VISIBLE);
        tv_title.setText(getResources().getString(R.string.text_3pl));
        iv_drawer = (ImageView) rootView.findViewById(R.id.iv_drawer);
        iv_drawer.setVisibility(View.VISIBLE);
        iv_drawer.setOnClickListener(this);
    }

    private void findViewById(View rootView) {
        et_search = rootView.findViewById(R.id.et_search);
        tv_err_msg = rootView.findViewById(R.id.tv_err_msg);
        cpb = rootView.findViewById(R.id.cpb);
        rv_home = rootView.findViewById(R.id.rv_home);
        et_search.setOnEditorActionListener(this);
    }

//    private void AddMenu() {
//        String[] menu = getResources().getStringArray(R.array.array_home_menu);
//        for (int i = 0; i < menu.length; i++) {
//            ModelStatic modelStatic = new ModelStatic();
//            modelStatic.setCategory(menu[i]);
//            list.add(modelStatic);
//        }
//        LogUtil.Print(TAG, "size==============" + list.size());
//    }

    private void main() {
        mLayoutManager = new LinearLayoutManager(getActivity());
        rv_home.setLayoutManager(mLayoutManager);
        Adp = new ContainerAdapter(getActivity(), list_container);
        Adp.setOnRecyclerViewItemClickListener(this);
       /* int space = getResources().getDimensionPixelSize(R.dimen.margin_5dp);
        rv_menu.addItemDecoration(new SpacesItemDecoration(space));*/
        rv_home.setAdapter(Adp);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_drawer:
                View view = getActivity().getCurrentFocus();
                if (view != null) {

                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                if (Home.drawer_layout.isDrawerOpen(GravityCompat.START)) {
                    Home.drawer_layout.closeDrawer(GravityCompat.START);
                } else {
                    Home.drawer_layout.openDrawer(GravityCompat.START);
                }
                break;
        }
    }

    @Override
    public void onItemClick(int position, int flag, View view) {
        Intent intent = new Intent(getActivity(), Details.class);

        intent.putExtra(Constants.id, list_container.get(position).getId());
        intent.putExtra(Constants.controlNumber, list_container.get(position).getControlNumber());
        intent.putExtra(Constants.cn, list_container.get(position).getClientCN());
        intent.putExtra(Constants.freightShipment, list_container.get(position).getFreightShipment());

        NavigatorManager.startNewActivity(getActivity(), intent);
//        Intent i = new Intent(getActivity(), Details.class);
////        i.putExtra(Constants.food_id, dataDietFoodList.get(position).getFoodId());
//        startActivity(i);
    }

    private void callContainerAPI() {

        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.driverID, App.Utils.getString(Constants.driverID));

            LogUtil.Print("ContainerList ApiRequest params", "" + params);
//            Api. service = retrofit.create(RetrofitArrayAPI.class);
            Call<List<GsonContainerList>> call = request.getContainerList(params);

            if (list_container.size() == 0) {
                cpb.setVisibility(View.VISIBLE);
            }
            call.enqueue(new Callback<List<GsonContainerList>>() {
                @Override
                public void onResponse(Call<List<GsonContainerList>> call, Response<List<GsonContainerList>> response) {
                    cpb.setVisibility(View.GONE);
                    List<GsonContainerList> gson = response.body();

                    if (response.body() == null) {
                        tv_err_msg.setText(getResources().getString(R.string.text_no_conatiner_found));
                        tv_err_msg.setVisibility(View.VISIBLE);
                    } else {
                        tv_err_msg.setVisibility(View.GONE);
                        if (list_container.size() > 0)
                            list_container.clear();

                        list_container.addAll(gson);
                        Adp.notifyDataSetChanged();
                        LogUtil.Print(TAG, "Gson:--" + gson);
                    }

                }

                @Override
                public void onFailure(Call<List<GsonContainerList>> call, Throwable t) {
                    if (list_container.size() == 0)
                        cpb.setVisibility(View.GONE);
                    MyUtils.makeSnackbar(rv_home, getResources().getString(R.string.text_server_error));
                    LogUtil.Print("===ContainerList====", t.getMessage());
                }
            });

        } else {
            MyUtils.ShowAlert(getActivity(), getResources().getString(R.string.text_internet), getResources().getString(R.string.app_name));
        }
    }

    private void callSearchContainerListAPI() {
        if (App.Utils.IsInternetOn()) {

            final ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.driverID, App.Utils.getString(Constants.driverID));
            params.put(Constants.searchTerm, et_search.getText().toString());

            LogUtil.Print("Search ContainerList ApiRequest params", "" + params);
//            Api. service = retrofit.create(RetrofitArrayAPI.class);
            Call<List<GsonContainerList>> call = request.getSearchContainerList(params);

            if (list_container.size() == 0) {
                cpb.setVisibility(View.VISIBLE);
            }
            call.enqueue(new Callback<List<GsonContainerList>>() {
                @Override
                public void onResponse(Call<List<GsonContainerList>> call, Response<List<GsonContainerList>> response) {
                    cpb.setVisibility(View.GONE);
                    List<GsonContainerList> gson = response.body();

                    if (response.body() == null) {
                        tv_err_msg.setText(getResources().getString(R.string.text_no_record_found));
                        tv_err_msg.setVisibility(View.VISIBLE);
                    } else {
                        tv_err_msg.setVisibility(View.GONE);
                        if (list_container.size() > 0)
                            list_container.clear();

                        list_container.addAll(gson);
                        Adp.notifyDataSetChanged();
                        LogUtil.Print(TAG, "Gson:--" + gson);
                    }

                }

                @Override
                public void onFailure(Call<List<GsonContainerList>> call, Throwable t) {
                    if (list_container.size() == 0)
                        cpb.setVisibility(View.GONE);
                    MyUtils.makeSnackbar(rv_home, getResources().getString(R.string.text_server_error));
                    LogUtil.Print("===ContainerList====", t.getMessage());
                }
            });

        } else {
            MyUtils.ShowAlert(getActivity(), getResources().getString(R.string.text_internet), getResources().getString(R.string.app_name));
        }
    }

    @Override
    public void onItemClickRefresh(boolean flag) {
        if (flag) {
            list_container.clear();
            callContainerAPI();
        }
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) ||
                (actionId == EditorInfo.IME_ACTION_DONE)) {
            if (et_search.getText().toString().trim().length() > 0) {
//                MyUtils.makeSnackbar(rv_home, et_search.getText().toString());
                callSearchContainerListAPI();
            } else {
                callContainerAPI();
//                MyUtils.makeSnackbar(rv_home, getResources().getString(R.string.text_search_empty_err));
            }
        }
        return false;
    }


}
