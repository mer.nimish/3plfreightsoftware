package com.freightsoftware.fragment;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.freightsoftware.App;
import com.freightsoftware.Constants.Api;
import com.freightsoftware.Constants.Constants;
import com.freightsoftware.Interface.ApiInterface;
import com.freightsoftware.Interface.RecyclerViewItemClickListener;
import com.freightsoftware.R;
import com.freightsoftware.Utility.LogUtil;
import com.freightsoftware.Utility.MyUtils;
import com.freightsoftware.activity.Home;
import com.freightsoftware.adapter.ClockInAdapter;
import com.freightsoftware.model.GsonClockInList;
import com.freightsoftware.model.GsonCompleted;
import com.freightsoftware.model.ModelStatic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ClockIn extends Fragment implements View.OnClickListener, RecyclerViewItemClickListener {

    private static final String TAG = "ClockIn";
    public static ImageView iv_drawer;
    public static List<ModelStatic> list = new ArrayList<>();
    public static List<GsonClockInList> list_clock = new ArrayList<>();
    RecyclerView rv_clock;
    ClockInAdapter Adp;
    ProgressBar cpb;
    String userId;
    /*header View*/
    private TextView tv_title;
    private Button btn_clock;
    private LinearLayout lv_main_header;
    private RecyclerView.LayoutManager mLayoutManager;

    private Dialog DialogLogout;
    private DisplayMetrics metrics;
    /*Dialog*/
    private ImageView img_close;
    private TextView tv_yes, tv_no, tv_title_text;



    public ClockIn() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_clock_in, container, false);

        if ((App.Utils.getString(Constants.user_id)).equalsIgnoreCase("")) {
            userId = getArguments().getString(Constants.user_id);
//            MyUtils.makeToast("ID:--"+userId);
        } else {
            userId = App.Utils.getString(Constants.user_id);
        }
        HeaderView(rootView);
        findViewById(rootView);
//        if (list == null || list.size() == 0) {
//            AddMenu();
//        }
        main();
        CallClockInAPI();
        return rootView;
    }

    private void CallClockInAPI() {
        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.userID, userId);

            LogUtil.Print("ClockInList ApiRequest params", "" + params);
//            Api. service = retrofit.create(RetrofitArrayAPI.class);
            Call<List<GsonClockInList>> call = request.getClockInList(params);

            if (list_clock.size() == 0) {
                cpb.setVisibility(View.VISIBLE);
            }
            call.enqueue(new Callback<List<GsonClockInList>>() {
                @Override
                public void onResponse(Call<List<GsonClockInList>> call, Response<List<GsonClockInList>> response) {
                    cpb.setVisibility(View.GONE);
                    List<GsonClockInList> gson = response.body();

                    if (list_clock.size() > 0)
                        list_clock.clear();

                    list_clock.addAll(gson);
                    Adp.notifyDataSetChanged();
                    LogUtil.Print(TAG, "Gson:--" + gson);
                }

                @Override
                public void onFailure(Call<List<GsonClockInList>> call, Throwable t) {
                    if (list_clock.size() == 0)
                        cpb.setVisibility(View.GONE);
                    MyUtils.ShowAlert(getActivity(), "Failure", getResources().getString(R.string.app_name));
//                    if (MyUtils.setRefresh(true)){
//                        callContainerAPI();
//                    }
                    LogUtil.Print("===ClockInList====", t.getMessage());
                }
            });

        } else {
            MyUtils.ShowAlert(getActivity(), getResources().getString(R.string.text_internet), getResources().getString(R.string.app_name));
        }
    }

    private void HeaderView(View rootView) {
        lv_main_header = (LinearLayout) rootView.findViewById(R.id.lv_main_header);
        tv_title = (TextView) rootView.findViewById(R.id.tv_title);

        tv_title.setVisibility(View.VISIBLE);
        tv_title.setText(getResources().getString(R.string.text_header_clock));
        iv_drawer = (ImageView) rootView.findViewById(R.id.iv_drawer);
        iv_drawer.setVisibility(View.VISIBLE);
        iv_drawer.setOnClickListener(this);
    }

    private void findViewById(View rootView) {
        cpb = rootView.findViewById(R.id.cpb);
        rv_clock = rootView.findViewById(R.id.rv_clock);
        btn_clock = rootView.findViewById(R.id.btn_clock);
        btn_clock.setOnClickListener(this);
    }

//    private void AddMenu() {
//        String[] menu = getResources().getStringArray(R.array.array_home_menu);
//        for (int i = 0; i < menu.length; i++) {
//            ModelStatic modelStatic = new ModelStatic();
//            modelStatic.setCategory(menu[i]);
//            list.add(modelStatic);
//        }
//        LogUtil.Print(TAG, "size==============" + list.size());
//    }

    private void main() {
        mLayoutManager = new LinearLayoutManager(getActivity());
        rv_clock.setLayoutManager(mLayoutManager);
        Adp = new ClockInAdapter(getActivity(), list_clock);
        Adp.setOnRecyclerViewItemClickListener(this);
        rv_clock.setAdapter(Adp);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_drawer:
                View view = getActivity().getCurrentFocus();
                if (view != null) {

                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                if (Home.drawer_layout.isDrawerOpen(GravityCompat.START)) {
                    Home.drawer_layout.closeDrawer(GravityCompat.START);
                } else {
                    Home.drawer_layout.openDrawer(GravityCompat.START);
                }
                break;
            case R.id.btn_clock:
                Dialog_Clock();
                break;
            case R.id.tv_no:

                if (DialogLogout.isShowing()) {
                    DialogLogout.dismiss();
                }
                break;
            case R.id.img_close:
                if (DialogLogout.isShowing()) {
                    DialogLogout.dismiss();
                }
                break;
            case R.id.tv_yes:

                CallClockInSubmitAPI();

                if (DialogLogout.isShowing()) {
                    DialogLogout.dismiss();
                }
        }
    }

    private void CallClockInSubmitAPI() {

        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.userID, userId);
            LogUtil.Print("ClickIn_Submit_params", "" + params);

            Call<GsonCompleted> call = request.driverClockIn(params);
            cpb.setVisibility(View.VISIBLE);
            call.enqueue(new Callback<GsonCompleted>() {
                @Override
                public void onResponse(Call<GsonCompleted> call, Response<GsonCompleted> response) {
                    LogUtil.Print("response", response.toString());


                    GsonCompleted gson = response.body();
                    LogUtil.Print(TAG, "Responce===" + response.body());
                    cpb.setVisibility(View.GONE);
                    if (response.body() == null) {
                        MyUtils.ShowAlert(getActivity(), "Falied to Submit", getResources().getString(R.string.app_name));
                    } else {
                        if (gson.getStatus() == 1) {
                            if (list_clock.size() > 0)
                                list_clock.clear();
                            CallClockInAPI();
                        }
                    }
                }


                @Override
                public void onFailure(Call<GsonCompleted> call, Throwable t) {
                    LogUtil.Print("Error", t.getMessage());
                    cpb.setVisibility(View.GONE);
                    LogUtil.Print(TAG, "Failure ");
                }
            });

        } else {

            MyUtils.ShowAlert(getActivity(), getResources().getString(R.string.text_internet), getResources().getString(R.string.app_name));
        }

    }

    @Override
    public void onItemClick(int position, int flag, View view) {

    }


//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//
//        try {
//            SM = (SendMessage) getActivity();
//        } catch (ClassCastException e) {
//            throw new ClassCastException("Error in retrieving data. Please try again");
//        }
//    }

    /**
     * Dialog for Logout
     */
    private void Dialog_Clock() {
        metrics = getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        DialogLogout = new Dialog(getActivity());
        DialogLogout.requestWindowFeature(Window.FEATURE_NO_TITLE);
        DialogLogout.setCancelable(true);
        DialogLogout.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        DialogLogout.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        DialogLogout.setContentView(R.layout.dialog_layout);
        img_close = (ImageView) DialogLogout.findViewById(R.id.img_close);
        tv_title_text = (TextView) DialogLogout.findViewById(R.id.tv_title_text);
        tv_yes = (TextView) DialogLogout.findViewById(R.id.tv_yes);
        tv_no = (TextView) DialogLogout.findViewById(R.id.tv_no);
        tv_title_text.setText(getResources().getString(R.string.text_are_you_sure_to_clockin));
        img_close.setOnClickListener(this);
        tv_yes.setOnClickListener(this);
        tv_no.setOnClickListener(this);

        DialogLogout.show();
        DialogLogout.getWindow().setLayout((6 * width) / 7, ViewGroup.LayoutParams.WRAP_CONTENT);
        DialogLogout.getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_dialog_rounded));

    }
}
