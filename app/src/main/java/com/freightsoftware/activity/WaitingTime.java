package com.freightsoftware.activity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;

import com.freightsoftware.App;
import com.freightsoftware.Constants.Api;
import com.freightsoftware.Constants.Constants;
import com.freightsoftware.Interface.ApiInterface;
import com.freightsoftware.Interface.RefreshListener;
import com.freightsoftware.R;
import com.freightsoftware.Utility.LogUtil;
import com.freightsoftware.Utility.MyUtils;
import com.freightsoftware.model.GsonCompleted;
import com.freightsoftware.model.GsonSelectWaitingTime;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WaitingTime extends AppCompatActivity implements View.OnClickListener {


    private static final String TAG = "WaitingTime";
    private static final SimpleDateFormat FORMATTERDATEPRINT = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    public static ImageView iv_back, iv_more;
    public static RefreshListener listener;
    int flag, tflag;
    String format;
    int hour, minute;
    LinearLayout lv_from, lv_to, lv_return;
    String wdate;
    ProgressBar cpb;
    String userID;
    TextView tv_clear_fstm,tv_clear_fetm,tv_clear_tstm,tv_clear_tetm,tv_clear_rstm,tv_clear_retm;
    /*header View*/
    private TextView tv_title;
    private LinearLayout lv_main_header;
    private TextView et_from_start_date, et_from_end_date, tv_from;
    private TextView et_to_start_date, et_to_end_date, tv_to;
    private TextView et_return_start_date, et_return_end_date, tv_return;
    private Button btn_save;
    private int mYear, mMonth, mDay;
    private Calendar calendar;

    public static void onRefreshClickListener(RefreshListener listener1) {
        listener = listener1;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waiting_time);
        if ((App.Utils.getString(Constants.user_id)).equalsIgnoreCase("")) {
            userID = App.Utils.getString(Constants.userID);

        } else {
            userID = App.Utils.getString(Constants.user_id);
        }
        HeaderView();
        findViewById();
        OnClickListener();

        if (getIntent().getExtras().getBoolean(Constants.freightShipment)) {
            lv_return.setVisibility(View.GONE);
            tv_from.setText("From");
            tv_to.setText("Delivery");
        } else {
            lv_return.setVisibility(View.VISIBLE);
            tv_from.setText("Terminal");
            tv_to.setText("Delivery");
        }
        CallSelectWaitingTimeAPI();


    }

    private void CallSelectWaitingTimeAPI() {
        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.controlNumber, String.valueOf(getIntent().getExtras().getInt(Constants.controlNumber)));
            LogUtil.Print("Select_Waiting_params", "" + params);

            Call<GsonSelectWaitingTime> call = request.selectWaitingTime(params);
            cpb.setVisibility(View.VISIBLE);
            call.enqueue(new Callback<GsonSelectWaitingTime>() {
                @Override
                public void onResponse(Call<GsonSelectWaitingTime> call, Response<GsonSelectWaitingTime> response) {
                    LogUtil.Print("response", response.toString());


                    GsonSelectWaitingTime gson = response.body();
                    LogUtil.Print(TAG, "" + response.body());
                    cpb.setVisibility(View.GONE);
//                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.DATE_FORMAT);
//
//                    String s = gson.getPickupFrom();
//                    long l = Long.parseLong(s.substring(s.indexOf("(")+1, s.indexOf(")")));
////                    Date date = new Date(l);
//
//                    String date1 = simpleDateFormat.format(l);
                    et_from_start_date.setText(CovertDate(gson.getPickupFrom()));
                    if (!(gson.getPickupFrom() == null))
                        tv_clear_fstm.setVisibility(View.VISIBLE);
                    et_from_end_date.setText(CovertDate(gson.getPickupTo()));
                    if (!(gson.getPickupTo() == null))
                        tv_clear_fetm.setVisibility(View.VISIBLE);
                    et_to_start_date.setText(CovertDate(gson.getDeliveryFrom()));
                    if (!(gson.getDeliveryFrom() == null))
                        tv_clear_tstm.setVisibility(View.VISIBLE);
                    et_to_end_date.setText(CovertDate(gson.getDeliveryTo()));
                    if (!(gson.getDeliveryTo()==null))
                        tv_clear_tetm.setVisibility(View.VISIBLE);
                    et_return_start_date.setText(CovertDate(gson.getReturnFrom()));
                    if (!(gson.getReturnFrom()==null))
                        tv_clear_rstm.setVisibility(View.VISIBLE);
                    et_return_end_date.setText(CovertDate(gson.getReturnTo()));
                    if (!(gson.getReturnTo()==null))
                        tv_clear_retm.setVisibility(View.VISIBLE);

                }


                @Override
                public void onFailure(Call<GsonSelectWaitingTime> call, Throwable t) {
                    LogUtil.Print("Error", t.getMessage());
                    cpb.setVisibility(View.GONE);
                    MyUtils.makeSnackbar(btn_save, getResources().getString(R.string.text_server_error));
                    LogUtil.Print(TAG, "Failure ");
                }
            });

        } else {

            MyUtils.ShowAlert(WaitingTime.this, getResources().getString(R.string.text_internet), getResources().getString(R.string.app_name));
        }
    }

    private String CovertDate(String date) {
        LogUtil.Print("DATE===", date);
        if (date == null) {
            return "";
        } else {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.DATE_FORMAT);

            String s = date;
            long l = Long.parseLong(s.substring(s.indexOf("(") + 1, s.indexOf(")")));
//                    Date date = new Date(l);

            String date1 = simpleDateFormat.format(l);
            return date1;
        }
    }

    private void HeaderView() {
        lv_main_header = findViewById(R.id.lv_main_header);
        tv_title = findViewById(R.id.tv_title);

        tv_title.setVisibility(View.VISIBLE);
        tv_title.setText(getResources().getString(R.string.text_waiting_time));
        iv_back = findViewById(R.id.iv_back);
        iv_back.setVisibility(View.VISIBLE);
        iv_back.setOnClickListener(this);

    }

    private void findViewById() {
        cpb = findViewById(R.id.cpb);
        lv_from = findViewById(R.id.lv_from);
        lv_to = findViewById(R.id.lv_to);
        lv_return = findViewById(R.id.lv_return);
        tv_from = findViewById(R.id.tv_from);
        et_from_start_date = findViewById(R.id.et_from_start_date);
//        et_from_start_time = findViewById(R.id.et_from_start_time);
        et_from_end_date = findViewById(R.id.et_from_end_date);
//        et_from_end_time = findViewById(R.id.et_from_end_time);
        tv_to = findViewById(R.id.tv_to);
        et_to_start_date = findViewById(R.id.et_to_start_date);
//        et_to_start_time = findViewById(R.id.et_to_start_time);
        et_to_end_date = findViewById(R.id.et_to_end_date);
//        et_to_end_time = findViewById(R.id.et_to_end_time);
        et_return_start_date = findViewById(R.id.et_return_start_date);
        et_return_end_date = findViewById(R.id.et_return_end_date);
        tv_return = findViewById(R.id.tv_return);
        btn_save = findViewById(R.id.btn_save);

        tv_clear_fstm = findViewById(R.id.tv_clear_fstm);
        tv_clear_fetm = findViewById(R.id.tv_clear_fetm);
        tv_clear_tstm = findViewById(R.id.tv_clear_tstm);
        tv_clear_tetm = findViewById(R.id.tv_clear_tetm);
        tv_clear_rstm = findViewById(R.id.tv_clear_rstm);
        tv_clear_retm = findViewById(R.id.tv_clear_retm);

    }

    private void OnClickListener() {
        et_from_start_date.setOnClickListener(this);
//        et_from_start_time.setOnClickListener(this);
        et_from_end_date.setOnClickListener(this);
//        et_from_end_time.setOnClickListener(this);
        et_to_start_date.setOnClickListener(this);
//        et_to_start_time.setOnClickListener(this);
        et_to_end_date.setOnClickListener(this);
        et_return_start_date.setOnClickListener(this);
        et_return_end_date.setOnClickListener(this);

        lv_return.setOnClickListener(this);
//        et_to_end_time.setOnClickListener(this);
        btn_save.setOnClickListener(this);

        tv_clear_fstm.setOnClickListener(this);
        tv_clear_fetm.setOnClickListener(this);
        tv_clear_tstm.setOnClickListener(this);
        tv_clear_tetm.setOnClickListener(this);
        tv_clear_rstm.setOnClickListener(this);
        tv_clear_retm.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.iv_back:
                onBackPressed();
                break;
            case R.id.et_from_start_date:
                flag = 1;
                DatePickerDialog();
                break;
//            case R.id.et_from_start_time:
//                tflag = 1;
////                settimeDialog();
//                break;
            case R.id.et_from_end_date:
                flag = 2;
                DatePickerDialog();
                break;
//            case R.id.et_from_end_time:
//                tflag = 2;
////                settimeDialog();
//                break;
            case R.id.et_to_start_date:
                flag = 3;
                DatePickerDialog();
                break;
//            case R.id.et_to_start_time:
//                tflag = 3;
////                settimeDialog();
//                break;
            case R.id.et_to_end_date:
                flag = 4;
                DatePickerDialog();
                break;
            case R.id.et_return_end_date:
                flag = 5;
                DatePickerDialog();
                break;
            case R.id.et_return_start_date:
                flag = 6;
                DatePickerDialog();
                break;
            case R.id.btn_save:


                CallUpdateWaitingTimeAPI();

//                DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
//                DateTime dt = formatter.parseDateTime(et_from_start_date.getText().toString());
                break;
            case R.id.tv_clear_fstm:
                et_from_start_date.setText("");
                tv_clear_fstm.setVisibility(View.GONE);
                break;
            case R.id.tv_clear_fetm:
                et_from_end_date.setText("");
                tv_clear_fetm.setVisibility(View.GONE);
                break;
            case R.id.tv_clear_tstm:
                et_to_start_date.setText("");
                tv_clear_tstm.setVisibility(View.GONE);
                break;
            case R.id.tv_clear_tetm:
                et_to_end_date.setText("");
                tv_clear_tetm.setVisibility(View.GONE);
                break;
            case R.id.tv_clear_rstm:
                et_return_start_date.setText("");
                tv_clear_rstm.setVisibility(View.GONE);
                break;
            case  R.id.tv_clear_retm:
                et_return_end_date.setText("");
                tv_clear_retm.setVisibility(View.GONE);
                break;
        }
    }

    private void CallUpdateWaitingTimeAPI() {
        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
//            params.put(Constants.controlNumber, App.createRequestBody(String.valueOf(getIntent().getExtras().getInt(Constants.controlNumber))));

            params.put(Constants.controlNumber, String.valueOf(getIntent().getExtras().getInt(Constants.controlNumber)));
//            params.put(Constants.pickupFrom, ""+new Date());
            params.put(Constants.userID, userID);
            params.put(Constants.userName, App.Utils.getString(Constants.username));
            params.put(Constants.pickupFrom, et_from_start_date.getText().toString());
            params.put(Constants.pickupTo, et_from_end_date.getText().toString());
            params.put(Constants.deliveryFrom, et_to_start_date.getText().toString());
            params.put(Constants.deliveryTo, et_to_end_date.getText().toString());
            params.put(Constants.returnFrom, et_return_start_date.getText().toString());
            params.put(Constants.returnTo, et_return_end_date.getText().toString());

            LogUtil.Print("Waiting_Time_params", "" + params);

            Call<GsonCompleted> call = request.updateWaitingTime(params);
            cpb.setVisibility(View.VISIBLE);
            call.enqueue(new Callback<GsonCompleted>() {
                @Override
                public void onResponse(Call<GsonCompleted> call, Response<GsonCompleted> response) {
                    LogUtil.Print("response", response.toString());


                    GsonCompleted gson = response.body();
                    LogUtil.Print(TAG, "Responce===" + response.body());
                    cpb.setVisibility(View.GONE);
                    if (response.body() == null) {
                        MyUtils.ShowAlert(WaitingTime.this, "Falied to Waiting", getResources().getString(R.string.app_name));
                    } else {
                        if (gson.getStatus() == 1) {
                            if (listener != null) {
                                listener.onItemClickRefresh(true);
                            }
                            MyUtils.ShowSuccessAlert(WaitingTime.this, "Successfully Update Waiting Time", getResources().getString(R.string.app_name));
//                            finish();
                        }
                    }
                }


                @Override
                public void onFailure(Call<GsonCompleted> call, Throwable t) {
                    LogUtil.Print("Error", t.getMessage());
                    cpb.setVisibility(View.GONE);
                    MyUtils.makeSnackbar(btn_save, getResources().getString(R.string.text_server_error));
                    LogUtil.Print(TAG, "Failure ");
                }
            });

        } else {

            MyUtils.ShowAlert(WaitingTime.this, getResources().getString(R.string.text_internet), getResources().getString(R.string.app_name));
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    /**
     * DatePicker Dialog
     */
    private void DatePickerDialog() {
        java.util.Calendar c = java.util.Calendar.getInstance();
        mYear = c.get(java.util.Calendar.YEAR);
        mMonth = c.get(java.util.Calendar.MONTH);
        mDay = c.get(java.util.Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = null;
        if (flag == 1) {
            datePickerDialog = new DatePickerDialog(WaitingTime.this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            wdate = (monthOfYear + 1) + "/" + dayOfMonth + "/" + year;
                            settimeDialog(flag);
                        }
                    }, mYear, mMonth, mDay);
        } else if (flag == 2) {
            datePickerDialog = new DatePickerDialog(WaitingTime.this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            wdate = (monthOfYear + 1) + "/" + dayOfMonth + "/" + year;
//                            et_from_end_date.setText((monthOfYear + 1) + "/" + dayOfMonth + "/" + year);
                            settimeDialog(flag);
                        }
                    }, mYear, mMonth, mDay);
        } else if (flag == 3) {
            datePickerDialog = new DatePickerDialog(WaitingTime.this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            wdate = (monthOfYear + 1) + "/" + dayOfMonth + "/" + year;
//                            et_to_start_date.setText((monthOfYear + 1) + "/" + dayOfMonth + "/" + year);
                            settimeDialog(flag);
                        }
                    }, mYear, mMonth, mDay);
        } else if (flag == 4) {
            datePickerDialog = new DatePickerDialog(WaitingTime.this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            wdate = (monthOfYear + 1) + "/" + dayOfMonth + "/" + year;
//                            et_to_end_date.setText((monthOfYear + 1) + "/" + dayOfMonth + "/" + year);
                            settimeDialog(flag);
                        }
                    }, mYear, mMonth, mDay);
        } else if (flag == 5) {
            datePickerDialog = new DatePickerDialog(WaitingTime.this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            wdate = (monthOfYear + 1) + "/" + dayOfMonth + "/" + year;
//                            et_return_end_date.setText((monthOfYear + 1) + "/" + dayOfMonth + "/" + year);
                            settimeDialog(flag);
                        }
                    }, mYear, mMonth, mDay);
        } else if (flag == 6) {
            datePickerDialog = new DatePickerDialog(WaitingTime.this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            wdate = (monthOfYear + 1) + "/" + dayOfMonth + "/" + year;
//                            et_return_end_date.setText((monthOfYear + 1) + "/" + dayOfMonth + "/" + year);
                            settimeDialog(flag);
                        }
                    }, mYear, mMonth, mDay);
        }


        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }


    private void settimeDialog(int tflag) {
        calendar = Calendar.getInstance();
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        minute = calendar.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker = null;
        if (tflag == 1) {
            mTimePicker = new TimePickerDialog(WaitingTime.this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                    if (selectedHour == 0) {
                        selectedHour += 12;
                        format = "AM";
                    } else if (selectedHour == 12) {
                        format = "PM";
                    } else if (selectedHour > 12) {
                        selectedHour -= 12;
                        format = "PM";
                    } else {
                        format = "AM";
                    }
                    et_from_start_date.setText(wdate + " " + selectedHour + ":" + selectedMinute + " " + format);
                    tv_clear_fstm.setVisibility(View.VISIBLE);
                }


            }, hour, minute, false);
        } else if (tflag == 2) {
            mTimePicker = new TimePickerDialog(WaitingTime.this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                    if (selectedHour == 0) {
                        selectedHour += 12;
                        format = "AM";
                    } else if (selectedHour == 12) {
                        format = "PM";
                    } else if (selectedHour > 12) {
                        selectedHour -= 12;
                        format = "PM";
                    } else {
                        format = "AM";
                    }
                    et_from_end_date.setText(wdate + " " + selectedHour + ":" + selectedMinute + " " + format);
                    tv_clear_fetm.setVisibility(View.VISIBLE);
//                    et_from_end_time.setText(selectedHour + ":" + selectedMinute + " " + format);
                }


            }, hour, minute, false);
        } else if (tflag == 3) {
            mTimePicker = new TimePickerDialog(WaitingTime.this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                    if (selectedHour == 0) {
                        selectedHour += 12;
                        format = "AM";
                    } else if (selectedHour == 12) {
                        format = "PM";
                    } else if (selectedHour > 12) {
                        selectedHour -= 12;
                        format = "PM";
                    } else {
                        format = "AM";
                    }
                    et_to_start_date.setText(wdate + " " + selectedHour + ":" + selectedMinute + " " + format);
                    tv_clear_tstm.setVisibility(View.VISIBLE);
//                    et_to_start_time.setText(selectedHour + ":" + selectedMinute + " " + format);
                }


            }, hour, minute, false);
        } else if (tflag == 4) {
            mTimePicker = new TimePickerDialog(WaitingTime.this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                    if (selectedHour == 0) {
                        selectedHour += 12;
                        format = "AM";
                    } else if (selectedHour == 12) {
                        format = "PM";
                    } else if (selectedHour > 12) {
                        selectedHour -= 12;
                        format = "PM";
                    } else {
                        format = "AM";
                    }
                    et_to_end_date.setText(wdate + " " + selectedHour + ":" + selectedMinute + " " + format);
                    tv_clear_tetm.setVisibility(View.VISIBLE);
//                    et_to_end_time.setText(selectedHour + ":" + selectedMinute + " " + format);
                }


            }, hour, minute, false);
        } else if (tflag == 5) {
            mTimePicker = new TimePickerDialog(WaitingTime.this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                    if (selectedHour == 0) {
                        selectedHour += 12;
                        format = "AM";
                    } else if (selectedHour == 12) {
                        format = "PM";
                    } else if (selectedHour > 12) {
                        selectedHour -= 12;
                        format = "PM";
                    } else {
                        format = "AM";
                    }
                    et_return_end_date.setText(wdate + " " + selectedHour + ":" + selectedMinute + " " + format);
                    tv_clear_retm.setVisibility(View.VISIBLE);
//                    et_to_end_time.setText(selectedHour + ":" + selectedMinute + " " + format);
                }


            }, hour, minute, false);
        } else if (tflag == 6) {
            mTimePicker = new TimePickerDialog(WaitingTime.this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                    if (selectedHour == 0) {
                        selectedHour += 12;
                        format = "AM";
                    } else if (selectedHour == 12) {
                        format = "PM";
                    } else if (selectedHour > 12) {
                        selectedHour -= 12;
                        format = "PM";
                    } else {
                        format = "AM";
                    }
                    et_return_start_date.setText(wdate + " " + selectedHour + ":" + selectedMinute + " " + format);
                    tv_clear_rstm.setVisibility(View.VISIBLE);
//                    et_to_end_time.setText(selectedHour + ":" + selectedMinute + " " + format);
                }


            }, hour, minute, false);
        }
        //Yes 24 hour time
        /*if (!time.equals(""))
            mTimePicker.*/
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }
}
