package com.freightsoftware.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.freightsoftware.App;
import com.freightsoftware.Constants.Api;
import com.freightsoftware.Constants.Constants;
import com.freightsoftware.Interface.ApiInterface;
import com.freightsoftware.Interface.RefreshListener;
import com.freightsoftware.R;
import com.freightsoftware.Utility.LogUtil;
import com.freightsoftware.Utility.MyUtils;
import com.freightsoftware.Utility.NavigatorManager;
import com.freightsoftware.model.GsonCompleted;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShipmentComplete extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "ShipmentComplete";
    public static RefreshListener listener;
    ProgressBar cpb;
    String userID;
    private ImageView ib_close;
    private Button btn_yes, btn_no;

    public static void onRefreshClickListener(RefreshListener listener1) {
        listener = listener1;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shipment_complete);
        if ((App.Utils.getString(Constants.user_id)).equalsIgnoreCase("")) {
            userID = App.Utils.getString(Constants.userID);

        } else {
            userID = App.Utils.getString(Constants.user_id);
        }
        HeaderView();
        findViewById();
        onClickListener();
    }


    private void HeaderView() {
        ib_close = findViewById(R.id.ib_close);
        ib_close.setVisibility(View.VISIBLE);
        ib_close.setOnClickListener(this);
    }

    private void findViewById() {
        cpb = findViewById(R.id.cpb);
        btn_yes = findViewById(R.id.btn_yes);
        btn_no = findViewById(R.id.btn_no);
    }

    private void onClickListener() {
        btn_yes.setOnClickListener(this);
        btn_no.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ib_close:
                onBackPressed();
                finish();
                break;
            case R.id.btn_yes:
                callCompleteAPI();
                break;
            case R.id.btn_no:
                onBackPressed();
                finish();
                break;
        }
    }

    private void callCompleteAPI() {
        if (App.Utils.IsInternetOn()) {
            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.id, "" + getIntent().getExtras().getInt(Constants.id));
            params.put(Constants.userID, userID);
            params.put(Constants.userName, App.Utils.getString(Constants.username));
            LogUtil.Print("Completed_params", "" + params);

            Call<GsonCompleted> call = request.getJSONCompleted(params);
            cpb.setVisibility(View.VISIBLE);
            call.enqueue(new Callback<GsonCompleted>() {
                @Override
                public void onResponse(Call<GsonCompleted> call, Response<GsonCompleted> response) {
                    LogUtil.Print("response", response.toString());
                    GsonCompleted gson = response.body();
                    cpb.setVisibility(View.GONE);
                    if (gson.getStatus() == 1) {
                        if (listener != null) {
                            listener.onItemClickRefresh(true);
                        }
                        Intent sign = new Intent(ShipmentComplete.this, Home.class);
                        NavigatorManager.startNewActivity(ShipmentComplete.this, sign);
                        finish();
                    }
                }
                @Override
                public void onFailure(Call<GsonCompleted> call, Throwable t) {
                    LogUtil.Print("Error", t.getMessage());
                    cpb.setVisibility(View.GONE);
                    MyUtils.makeSnackbar(btn_no, getResources().getString(R.string.text_server_error));
                    LogUtil.Print(TAG, "Failure ");
                }
            });
        } else {
            MyUtils.ShowAlert(ShipmentComplete.this, getResources().getString(R.string.text_internet), getResources().getString(R.string.app_name));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}