package com.freightsoftware.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.freightsoftware.App;
import com.freightsoftware.Constants.Api;
import com.freightsoftware.Constants.Constants;
import com.freightsoftware.Interface.ApiInterface;
import com.freightsoftware.Interface.RefreshListener;
import com.freightsoftware.R;
import com.freightsoftware.Utility.LogUtil;
import com.freightsoftware.Utility.MyUtils;
import com.freightsoftware.model.GsonCompleted;
import com.freightsoftware.model.GsonSelectConatinerChasis;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateContainerStatus extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "UpdateContainerStatus";
    public static ImageView iv_back, iv_more;
    public static RefreshListener listener;
    ProgressBar cpb;
    /*header View*/
    private TextView tv_title;
    private LinearLayout lv_main_header;
    private EditText et_container, et_chasis, et_seal;
    private Button btn_update;
    String userID;

    public static void onRefreshClickListener(RefreshListener listener1) {
        listener = listener1;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_container_status);

        if ((App.Utils.getString(Constants.user_id)).equalsIgnoreCase("")) {
            userID = App.Utils.getString(Constants.userID);

        } else {
            userID = App.Utils.getString(Constants.user_id);
        }
        HeaderView();
        findViewById();
        CallSelectChasisAPI();
    }

    private void CallSelectChasisAPI() {
        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.controlNumber, String.valueOf(getIntent().getExtras().getInt(Constants.controlNumber)));
            LogUtil.Print("Update_Container_status_params", "" + params);

            Call<GsonSelectConatinerChasis> call = request.getSelectContinerChasis(params);
            cpb.setVisibility(View.VISIBLE);
            call.enqueue(new Callback<GsonSelectConatinerChasis>() {
                @Override
                public void onResponse(Call<GsonSelectConatinerChasis> call, Response<GsonSelectConatinerChasis> response) {
                    LogUtil.Print("response", response.toString());


                    GsonSelectConatinerChasis gson = response.body();
                    cpb.setVisibility(View.GONE);
                    et_container.setText(gson.getContainer());
                    et_chasis.setText(gson.getChassisNumber());
                    et_seal.setText(gson.getSealNumber());


                }

                @Override
                public void onFailure(Call<GsonSelectConatinerChasis> call, Throwable t) {
                    LogUtil.Print("Error", t.getMessage());
                    cpb.setVisibility(View.GONE);
                    MyUtils.makeSnackbar(et_seal, getResources().getString(R.string.text_server_error));
                    LogUtil.Print(TAG, "Failure ");
                }
            });

        } else {

            MyUtils.ShowAlert(UpdateContainerStatus.this, getResources().getString(R.string.text_internet), getResources().getString(R.string.app_name));
        }
    }

    private void HeaderView() {
        lv_main_header = findViewById(R.id.lv_main_header);
        tv_title = findViewById(R.id.tv_title);

        tv_title.setVisibility(View.VISIBLE);
        tv_title.setText(getResources().getString(R.string.text_update_container));
        iv_back = findViewById(R.id.iv_back);
        iv_back.setVisibility(View.VISIBLE);
        iv_back.setOnClickListener(this);

    }

    private void findViewById() {
        cpb = findViewById(R.id.cpb);
        et_container = findViewById(R.id.et_container);
        et_seal = findViewById(R.id.et_seal);
        et_chasis = findViewById(R.id.et_chasis);
        btn_update = findViewById(R.id.btn_update);
        btn_update.setOnClickListener(this);
        et_container.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        et_seal.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        et_chasis.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.iv_back:
                onBackPressed();
                finish();
                break;
            case R.id.btn_update:
                if (et_container.getText().toString().isEmpty()) {
                    MyUtils.ShowAlert(UpdateContainerStatus.this, getResources().getString(R.string.text_err_conatiner), getResources().getString(R.string.app_name));
                } else if (et_chasis.getText().toString().trim().isEmpty()) {
                    MyUtils.ShowAlert(UpdateContainerStatus.this, getResources().getString(R.string.text_err_chasis), getResources().getString(R.string.app_name));

                } else {
                    CallUpdateConatinerAPI();
                }
//                else if (et_seal.getText().toString().isEmpty()) {
//                    MyUtils.ShowAlert(UpdateContainerStatus.this, getResources().getString(R.string.text_err_seal), getResources().getString(R.string.app_name));
//                }

                break;

        }
    }

    private void CallUpdateConatinerAPI() {
        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.controlNumber, String.valueOf(getIntent().getExtras().getInt(Constants.controlNumber)));
            params.put(Constants.containerNumber, et_container.getText().toString());
            params.put(Constants.chassisNumber, et_chasis.getText().toString());
            params.put(Constants.sealNumber, et_seal.getText().toString());
            params.put(Constants.userID, userID);
            params.put(Constants.userName, App.Utils.getString(Constants.username));
            LogUtil.Print("Update_Container_status_params", "" + params);

            Call<GsonCompleted> call = request.getUpdateContinerStatus(params);
            cpb.setVisibility(View.VISIBLE);
            call.enqueue(new Callback<GsonCompleted>() {
                @Override
                public void onResponse(Call<GsonCompleted> call, Response<GsonCompleted> response) {
                    LogUtil.Print("response", response.toString());

                    cpb.setVisibility(View.GONE);
                    if (response.body() == null) {
                        MyUtils.ShowAlert(UpdateContainerStatus.this, "Falied to update container chassis", getResources().getString(R.string.app_name));
                    } else {
                        GsonCompleted gson = response.body();
                        if (gson.getStatus() == 1) {
                            if (listener != null) {
                                listener.onItemClickRefresh(true);
                            }
                            MyUtils.ShowSuccessAlert(UpdateContainerStatus.this, "Successfully Update Container Chassis", getResources().getString(R.string.app_name));
//                            finish();
                        }
                    }


                }

                @Override
                public void onFailure(Call<GsonCompleted> call, Throwable t) {
                    LogUtil.Print("Error", t.getMessage());
                    cpb.setVisibility(View.GONE);
                    MyUtils.makeSnackbar(btn_update, getResources().getString(R.string.text_server_error));
                    LogUtil.Print(TAG, "Failure ");
                }
            });

        } else {
            MyUtils.ShowAlert(UpdateContainerStatus.this, getResources().getString(R.string.text_internet), getResources().getString(R.string.app_name));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
