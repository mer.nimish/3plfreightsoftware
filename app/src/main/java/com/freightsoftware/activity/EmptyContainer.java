package com.freightsoftware.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.freightsoftware.App;
import com.freightsoftware.Constants.Api;
import com.freightsoftware.Constants.Constants;
import com.freightsoftware.Interface.ApiInterface;
import com.freightsoftware.Interface.RefreshListener;
import com.freightsoftware.R;
import com.freightsoftware.Utility.LogUtil;
import com.freightsoftware.Utility.MyUtils;
import com.freightsoftware.model.GsonCompleted;
import com.freightsoftware.model.GsonSelectEmptyCotainer;
import com.freightsoftware.model.GsonTerminalList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmptyContainer extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "EmptyContainer";
    public static ImageView iv_back, iv_more;
    public static List<GsonTerminalList> list_terminal = new ArrayList<>();
    public static RefreshListener listener;
    ProgressBar progressBar_terminal, cpb;
    ArrayAdapter<String> adp_Type;
    String type = "";
    /*header View*/
    private TextView tv_title;
    private LinearLayout lv_main_header;
    private EditText et_container, et_chasis;
    private Spinner sp_terminal;
    private Button btn_save;
    String userID;

    public static void onRefreshClickListener(RefreshListener listener1) {
        listener = listener1;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empty_container);
        if ((App.Utils.getString(Constants.user_id)).equalsIgnoreCase("")) {
            userID = App.Utils.getString(Constants.userID);

        } else {
            userID = App.Utils.getString(Constants.user_id);
        }
        HeaderView();
        findViewById();
        OnClickListener();
        CallSelectEmptyContainerAPI();
        if (list_terminal.size() == 0)
            getTypeListAPIRequest();
        else
            FillTypeSpinnerData();


    }

    private void CallSelectEmptyContainerAPI() {
        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.controlNumber, "" + getIntent().getExtras().getInt(Constants.controlNumber));
            LogUtil.Print("Select_empty_Container_params", "" + params);

            Call<GsonSelectEmptyCotainer> call = request.selectEmptyContainer(params);
            cpb.setVisibility(View.VISIBLE);
            call.enqueue(new Callback<GsonSelectEmptyCotainer>() {
                @Override
                public void onResponse(Call<GsonSelectEmptyCotainer> call, Response<GsonSelectEmptyCotainer> response) {
                    LogUtil.Print("response", response.toString());


                    GsonSelectEmptyCotainer gson = response.body();
                    cpb.setVisibility(View.GONE);
                    et_container.setText(gson.getContainerNumber());
                    et_chasis.setText(gson.getChassisNumber());
                    type = gson.getTerminal();

                    FillTypeSpinnerData();


                }

                @Override
                public void onFailure(Call<GsonSelectEmptyCotainer> call, Throwable t) {
                    LogUtil.Print("Error", t.getMessage());
                    cpb.setVisibility(View.GONE);
                    LogUtil.Print(TAG, "Failure ");
                    MyUtils.makeSnackbar(btn_save, getResources().getString(R.string.text_server_error));
                }
            });

        } else {

            MyUtils.ShowAlert(EmptyContainer.this, getResources().getString(R.string.text_internet), getResources().getString(R.string.app_name));
        }
    }

    private void HeaderView() {
        lv_main_header = findViewById(R.id.lv_main_header);
        tv_title = findViewById(R.id.tv_title);

        tv_title.setVisibility(View.VISIBLE);
        tv_title.setText(getResources().getString(R.string.text_empty_container));
        iv_back = findViewById(R.id.iv_back);
        iv_back.setVisibility(View.VISIBLE);
        iv_back.setOnClickListener(this);

    }

    private void findViewById() {
        cpb = findViewById(R.id.cpb);
        progressBar_terminal = findViewById(R.id.progressBar_terminal);
        et_container = findViewById(R.id.et_container);
        et_chasis = findViewById(R.id.et_chasis);
        sp_terminal = findViewById(R.id.sp_terminal);
        btn_save = findViewById(R.id.btn_save);
        et_container.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        et_chasis.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
    }

    /**
     * OnClickListener of Views
     */
    private void OnClickListener() {

        btn_save.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.iv_back:
                onBackPressed();
                break;
            case R.id.btn_save:
                if (et_container.getText().toString().isEmpty()) {
                    App.Utils.ShowAlert(EmptyContainer.this, getResources().getString(R.string.text_err_conatiner), getResources().getString(R.string.app_name));
                } else if (et_chasis.getText().toString().trim().isEmpty()) {
                    App.Utils.ShowAlert(EmptyContainer.this, getResources().getString(R.string.text_err_chasis), getResources().getString(R.string.app_name));

                } else if (sp_terminal.getSelectedItemPosition() == 0) {
                    sp_terminal.requestFocus();
                    App.Utils.ShowAlert(EmptyContainer.this, getResources().getString(R.string.text_err_terminal), getResources().getString(R.string.app_name));

                } else {
                    CallUpdateEmptyConatinerAPI();
                }
                break;

        }
    }

    private void CallUpdateEmptyConatinerAPI() {
        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.controlNumber, "" + getIntent().getExtras().getInt(Constants.controlNumber));
            params.put(Constants.containerNumber, et_container.getText().toString());
            params.put(Constants.chassisNumber, et_chasis.getText().toString());
            params.put(Constants.driverID, App.Utils.getString(Constants.driverID));
            params.put(Constants.userID, userID);
            params.put(Constants.userName, App.Utils.getString(Constants.username));
            params.put(Constants.terminal, "" + list_terminal.get(sp_terminal.getSelectedItemPosition() - 1).getID());
            LogUtil.Print("Update_Shipment_status_params", "" + params);

            Call<GsonCompleted> call = request.getUpdateContainer(params);
            cpb.setVisibility(View.VISIBLE);
            call.enqueue(new Callback<GsonCompleted>() {
                @Override
                public void onResponse(Call<GsonCompleted> call, Response<GsonCompleted> response) {
                    LogUtil.Print("response", response.toString());


                    GsonCompleted gson = response.body();
                    cpb.setVisibility(View.GONE);
                    if (response.body() == null) {
                        MyUtils.ShowAlert(EmptyContainer.this, "Falied to update Empty Container", getResources().getString(R.string.app_name));
                    } else {
                        if (gson.getStatus() == 1) {
                            if (listener != null) {
                                listener.onItemClickRefresh(true);
                            }
                            MyUtils.ShowSuccessAlert(EmptyContainer.this, "Successfully Update Empty Container", getResources().getString(R.string.app_name));
//                            finish();
                        }
                    }
                }

                @Override
                public void onFailure(Call<GsonCompleted> call, Throwable t) {
                    LogUtil.Print("Error", t.getMessage());
                    cpb.setVisibility(View.GONE);
                    LogUtil.Print(TAG, "Failure ");
                    MyUtils.makeSnackbar(btn_save, getResources().getString(R.string.text_server_error));
                }
            });

        } else {

            MyUtils.ShowAlert(EmptyContainer.this, getResources().getString(R.string.text_internet), getResources().getString(R.string.app_name));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void getTypeListAPIRequest() {
        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.company_ID, App.Utils.getString(Constants.company_ID));

            LogUtil.Print("Shipment Status List ApiRequest params", "" + params);
//            Api. service = retrofit.create(RetrofitArrayAPI.class);
            Call<List<GsonTerminalList>> call = request.getTerminalList(params);

            if (list_terminal.size() == 0) {
                progressBar_terminal.setVisibility(View.VISIBLE);
                cpb.setVisibility(View.VISIBLE);
            }
            call.enqueue(new Callback<List<GsonTerminalList>>() {
                @Override
                public void onResponse(Call<List<GsonTerminalList>> call, Response<List<GsonTerminalList>> response) {
                    progressBar_terminal.setVisibility(View.GONE);
                    cpb.setVisibility(View.GONE);
                    List<GsonTerminalList> gson = response.body();

                    if (list_terminal.size() > 0)
                        list_terminal.clear();

                    list_terminal.addAll(gson);
                    FillTypeSpinnerData();
                }

                @Override
                public void onFailure(Call<List<GsonTerminalList>> call, Throwable t) {
                    if (list_terminal.size() == 0) {
                        progressBar_terminal.setVisibility(View.GONE);
                        cpb.setVisibility(View.GONE);
                    }
                    MyUtils.makeSnackbar(btn_save, getResources().getString(R.string.text_server_error));
                    LogUtil.Print("===Shipment Status List====", t.getMessage());
                }
            });

        } else {
            MyUtils.makeToast(getResources().getString(R.string.text_internet));
        }
    }

    private void FillTypeSpinnerData() {

        final List<String> temp_list = new ArrayList<>();
        temp_list.add("" + getResources().getString(R.string.text_select));
        for (int i = 0; i < list_terminal.size(); i++) {
            temp_list.add("" + list_terminal.get(i).getTerminal());
        }


        if (type.equals("")) {
            adp_Type = new ArrayAdapter<>(EmptyContainer.this, R.layout.lv_spinner_status, R.id.tv_spn_pool, temp_list);
            adp_Type.setDropDownViewResource(R.layout.lv_dropdown);
            sp_terminal.setAdapter(adp_Type);

        } else {
            adp_Type = new ArrayAdapter<>(EmptyContainer.this, R.layout.lv_spinner_status, R.id.tv_spn_pool, temp_list);
            adp_Type.setDropDownViewResource(R.layout.lv_dropdown);
            sp_terminal.setAdapter(adp_Type);
            for (int i = 0; i < list_terminal.size(); i++) {

                if (list_terminal.get(i).getID() == Integer.parseInt(type)) {
                    int spinnerPosition = adp_Type.getPosition(list_terminal.get(i).getTerminal());
                    sp_terminal.setSelection(spinnerPosition);
                    break;
                }


            }

        }


    }

}
