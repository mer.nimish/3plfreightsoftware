package com.freightsoftware.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.freightsoftware.App;
import com.freightsoftware.Constants.Api;
import com.freightsoftware.Constants.Constants;
import com.freightsoftware.Interface.ApiInterface;
import com.freightsoftware.Interface.RefreshListener;
//import com.freightsoftware.Manifest;
import com.freightsoftware.R;
import com.freightsoftware.Utility.LogUtil;
import com.freightsoftware.Utility.MyUtils;
import com.freightsoftware.model.GsonCompleted;
import com.freightsoftware.model.GsonDocList;
import com.freightsoftware.model.GsonShipmentStatusList;
import com.github.gcacace.signaturepad.views.SignaturePad;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CaptureSignature extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "CaptureSignature";
    /*header View*/
    private TextView tv_title;
    public static ImageView iv_back,iv_more;
    private LinearLayout lv_main_header;

    private CheckBox ck_check;
    ProgressBar progressBar_doc,cpb;
    private Spinner sp_doc;
    private EditText et_name;
    private Button btn_save,btn_reset;
    public static List<GsonDocList> list_doc = new ArrayList<>();
    ArrayAdapter<String> adp_Type;
    private SignaturePad signature_pad;

    String encodedImage = "";
    Bitmap signatureBitmap;
    String p;
    String userId;

    public static RefreshListener listener;

    public static void onRefreshClickListener(RefreshListener listener1) {
        listener = listener1;
    }


    private void chkPermission() {

        if (!(App.Utils.checkPermission(CaptureSignature.this, android.Manifest.permission.READ_EXTERNAL_STORAGE))) {

            App.Utils.showPermissionsDialog(CaptureSignature.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                    Constants.REQUEST_EXTERNAL_STORAGE);
            return;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        chkPermission();
        setContentView(R.layout.activity_capture_signature);

        if ((App.Utils.getString(Constants.user_id)).equalsIgnoreCase("")) {
            userId = App.Utils.getString(Constants.userID);
//            MyUtils.makeToast("ID:--"+userId);
        } else {
            userId = App.Utils.getString(Constants.user_id);
//            MyUtils.makeToast("SessionID:--"+userId);
        }
        HeaderView();
        findViewById();
        OnClickListener();
        if (list_doc.size() == 0)
            getTypeListAPIRequest();
        else
            FillTypeSpinnerData();
    }

    private void HeaderView() {
        lv_main_header = findViewById(R.id.lv_main_header);
        tv_title = findViewById(R.id.tv_title);

        tv_title.setVisibility(View.VISIBLE);
        tv_title.setText(getResources().getString(R.string.text_capture_signature));
        iv_back = findViewById(R.id.iv_back);
        iv_back.setVisibility(View.VISIBLE);
        iv_back.setOnClickListener(this);

    }

    private void findViewById() {
        cpb = findViewById(R.id.cpb);
        progressBar_doc = findViewById(R.id.progressBar_doc);
        ck_check = findViewById(R.id.ck_check);
        sp_doc = findViewById(R.id.sp_doc);
        et_name = findViewById(R.id.et_name);
        btn_save = findViewById(R.id.btn_save);
        btn_reset = findViewById(R.id.btn_reset);
        signature_pad = findViewById(R.id.signature_pad);

        signature_pad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
//                MyUtils.makeToast("start signature");
            }

            @Override
            public void onSigned() {
                btn_reset.setVisibility(View.VISIBLE);
            }

            @Override
            public void onClear() {
                btn_reset.setVisibility(View.GONE);
            }
        });
    }

    /**
     * OnClickListener of Views
     */
    private void OnClickListener() {

        btn_save.setOnClickListener(this);
        btn_reset.setOnClickListener(this);
        ck_check.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.iv_back:
                onBackPressed();
                break;
            case R.id.btn_save:
                if (et_name.getText().toString().isEmpty()) {
                    App.Utils.ShowAlert(CaptureSignature.this, getResources().getString(R.string.text_err_name), getResources().getString(R.string.app_name));
                }  else if (sp_doc.getSelectedItemPosition() == 0) {
                    sp_doc.requestFocus();
                    App.Utils.ShowAlert(CaptureSignature.this, getResources().getString(R.string.text_err_doc), getResources().getString(R.string.app_name));

                } else {
//                    CallUpdateEmptyConatinerAPI();

                    if (ck_check.isChecked()) {
//                        MyUtils.makeToast("true");
                        p = "true";
                    } else {
//                        MyUtils.makeToast("false");
                        p = "false";
                    }
                    signatureBitmap = signature_pad.getSignatureBitmap();
                    upload();


//                    if (addJpgSignatureToGallery(signatureBitmap)) {
//                        Toast.makeText(CaptureSignature.this, "Signature saved into the Gallery", Toast.LENGTH_SHORT).show();
//                    } else {
//                        Toast.makeText(CaptureSignature.this, "Unable to store the signature", Toast.LENGTH_SHORT).show();
//                    }
//                    if (addSvgSignatureToGallery(signature_pad.getSignatureSvg())) {
//                        Toast.makeText(CaptureSignature.this, "SVG Signature saved into the Gallery", Toast.LENGTH_SHORT).show();
//                    } else {
//                        Toast.makeText(CaptureSignature.this, "Unable to store the SVG signature", Toast.LENGTH_SHORT).show();
//                    }
                }
                break;
            case R.id.btn_reset:
                signature_pad.clear();
                break;

        }
    }

    private void upload() {


        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        signatureBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bao);
        byte[] ba = bao.toByteArray();
        encodedImage = Base64.encodeToString(ba, Base64.NO_WRAP);

        LogUtil.Print("base64", "-----" + encodedImage);

        CallSignatureUploadAPI();

        // Upload image to server
//        LogUtil.Print(TAG, "params=== userid:-"+ App.Utils.getString(Constants.user_id) + "company_ID:-" +App.Utils.getString(Constants.company_ID)
//                                + "controlNumber: "+ String.valueOf(getIntent().getExtras().getInt(Constants.controlNumber)) +"contactName:-"+et_name.getText().toString() +"publicStatus:--"+ p);

    }

    private void CallSignatureUploadAPI() {
        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.userID, userId);
            params.put(Constants.userName, App.Utils.getString(Constants.username));
            params.put(Constants.company_ID, App.Utils.getString(Constants.company_ID));
            params.put(Constants.controlNumber, String.valueOf(getIntent().getExtras().getInt(Constants.controlNumber)));
            params.put(Constants.drpDocumentTypeID, "" + list_doc.get(sp_doc.getSelectedItemPosition() - 1).getID());
            params.put(Constants.drpDocumentType, list_doc.get(sp_doc.getSelectedItemPosition() - 1).getDocName());
            params.put(Constants.publicStatus, p);
            params.put(Constants.contactName, et_name.getText().toString());
            params.put(Constants.base64Image, encodedImage);
//            params.put(Constants.driverID, App.Utils.getString(Constants.driverID));

            LogUtil.Print("Capture_Signature_params", "" + params);

            Call<GsonCompleted> call = request.updateCaptureSignature(params);
            cpb.setVisibility(View.VISIBLE);
            call.enqueue(new Callback<GsonCompleted>() {
                @Override
                public void onResponse(Call<GsonCompleted> call, Response<GsonCompleted> response) {
                    LogUtil.Print("response", response.toString());


                    GsonCompleted gson = response.body();
                    cpb.setVisibility(View.GONE);
                    if (response.body() == null) {
                        MyUtils.ShowAlert(CaptureSignature.this, "Falied to Capture Signature Container", getResources().getString(R.string.app_name));
                    } else {
                        if (gson.getStatus() == 1) {
                            if (listener != null) {
                                listener.onItemClickRefresh(true);
                            }
                            MyUtils.ShowSuccessAlert(CaptureSignature.this, "Capture Signature Container Successfully", getResources().getString(R.string.app_name));
//                            finish();
                        }
                    }
                }

                @Override
                public void onFailure(Call<GsonCompleted> call, Throwable t) {
                    LogUtil.Print("Error", t.getMessage());
                    cpb.setVisibility(View.GONE);
                    LogUtil.Print(TAG, "Failure ");
                    MyUtils.makeSnackbar(sp_doc, getResources().getString(R.string.text_server_error));
                }
            });

        } else {

            MyUtils.ShowAlert(CaptureSignature.this, getResources().getString(R.string.text_internet), getResources().getString(R.string.app_name));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void getTypeListAPIRequest() {
        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.company_ID, App.Utils.getString(Constants.company_ID));

            LogUtil.Print("Doc List ApiRequest params", "" + params);
//            Api. service = retrofit.create(RetrofitArrayAPI.class);
            Call<List<GsonDocList>> call = request.getDocList(params);

            if (list_doc.size() == 0) {
                progressBar_doc.setVisibility(View.VISIBLE);
                cpb.setVisibility(View.VISIBLE);
            }
            call.enqueue(new Callback<List<GsonDocList>>() {
                @Override
                public void onResponse(Call<List<GsonDocList>> call, Response<List<GsonDocList>> response) {
                    progressBar_doc.setVisibility(View.GONE);
                    cpb.setVisibility(View.GONE);
                    List<GsonDocList> gson = response.body();

                    if (list_doc.size() > 0)
                        list_doc.clear();

                    list_doc.addAll(gson);
                    FillTypeSpinnerData();
                }

                @Override
                public void onFailure(Call<List<GsonDocList>> call, Throwable t) {
                    if (list_doc.size() == 0) {
                        progressBar_doc.setVisibility(View.GONE);
                        cpb.setVisibility(View.GONE);
                    }
                    LogUtil.Print("===Doc List====", t.getMessage());
                    MyUtils.makeSnackbar(et_name, getResources().getString(R.string.text_server_error));
                }
            });

        } else {
            MyUtils.makeToast(getResources().getString(R.string.text_internet));
        }
    }

    private void FillTypeSpinnerData() {

        final List<String> temp_list = new ArrayList<>();
        temp_list.add("" + getResources().getString(R.string.text_select));
        for (int i = 0; i < list_doc.size(); i++) {
            temp_list.add("" + list_doc.get(i).getDocName());
        }

        adp_Type = new ArrayAdapter<>(CaptureSignature.this, R.layout.lv_spinner_status, R.id.tv_spn_pool, temp_list);
        adp_Type.setDropDownViewResource(R.layout.lv_dropdown);
        sp_doc.setAdapter(adp_Type);

    }



//    @Override
//    public void onRequestPermissionsResult(int requestCode,
//                                           @NonNull String permissions[], @NonNull int[] grantResults) {
//        switch (requestCode) {
//            case Constants.REQUEST_EXTERNAL_STORAGE: {
//                // If request is cancelled, the result arrays are empty.
//                if (grantResults.length <= 0
//                        || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
//                    Toast.makeText(CaptureSignature.this, "Cannot write images to external storage", Toast.LENGTH_SHORT).show();
//                }
//            }
//        }
//    }
//
//    public File getAlbumStorageDir(String albumName) {
//        // Get the directory for the user's public pictures directory.
//        File file = new File(Environment.getExternalStoragePublicDirectory(
//                Environment.DIRECTORY_PICTURES), albumName);
//        if (!file.mkdirs()) {
//            Log.e("SignaturePad", "Directory not created");
//        }
//        return file;
//    }
//
//    public void saveBitmapToJPG(Bitmap bitmap, File photo) throws IOException {
//        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
//        Canvas canvas = new Canvas(newBitmap);
//        canvas.drawColor(Color.WHITE);
//        canvas.drawBitmap(bitmap, 0, 0, null);
//        OutputStream stream = new FileOutputStream(photo);
//        newBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
//        stream.close();
//    }
//
//    public boolean addJpgSignatureToGallery(Bitmap signature) {
//        boolean result = false;
//        try {
//            File photo = new File(getAlbumStorageDir("SignaturePad"), String.format("Signature_%d.jpg", System.currentTimeMillis()));
//            saveBitmapToJPG(signature, photo);
//            scanMediaFile(photo);
//            result = true;
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return result;
//    }
//
//    private void scanMediaFile(File photo) {
//        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
//        Uri contentUri = Uri.fromFile(photo);
//        mediaScanIntent.setData(contentUri);
//        CaptureSignature.this.sendBroadcast(mediaScanIntent);
//    }
//
//    public boolean addSvgSignatureToGallery(String signatureSvg) {
//        boolean result = false;
//        try {
//            File svgFile = new File(getAlbumStorageDir("SignaturePad"), String.format("Signature_%d.svg", System.currentTimeMillis()));
//            OutputStream stream = new FileOutputStream(svgFile);
//            OutputStreamWriter writer = new OutputStreamWriter(stream);
//            writer.write(signatureSvg);
//            writer.close();
//            stream.flush();
//            stream.close();
//            scanMediaFile(svgFile);
//            result = true;
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return result;
//    }



}
