package com.freightsoftware.activity;

import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.freightsoftware.App;
import com.freightsoftware.Constants.Api;
import com.freightsoftware.Constants.Constants;
import com.freightsoftware.Interface.ApiInterface;
import com.freightsoftware.Interface.RefreshListener;
import com.freightsoftware.R;
import com.freightsoftware.Utility.LogUtil;
import com.freightsoftware.Utility.MyUtils;
import com.freightsoftware.model.GsonCompleted;
import com.freightsoftware.model.GsonNotesType;
import com.freightsoftware.model.GsonNotesType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Notes extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "Notes";
    public static ImageView iv_back, iv_more;
    ProgressBar cpb, progressBar_notes;
    /*header View*/
    private TextView tv_title;
    private LinearLayout lv_main_header;
    private CheckBox ck_check;
    private Spinner sp_notes;
    private EditText et_notes;
    private Button btn_save;
    String userId, p;

    public static List<GsonNotesType> list_note = new ArrayList<>();
    ArrayAdapter<String> adp_Type;

    public static RefreshListener listener;

    public static void onRefreshClickListener(RefreshListener listener1) {
        listener = listener1;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes);

        if ((App.Utils.getString(Constants.user_id)).equalsIgnoreCase("")) {
            userId = getIntent().getExtras().getString(Constants.user_id);
//            MyUtils.makeToast("ID:--"+userId);
        } else {
            userId = App.Utils.getString(Constants.user_id);
//            MyUtils.makeToast("SessionID:--"+userId);
        }
        HeaderView();
        findViewById();
        OnClickListener();
        if (list_note.size() == 0)
            getTypeListAPIRequest();
        else
            FillTypeSpinnerData();
    }

    private void getTypeListAPIRequest() {

        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.company_ID, App.Utils.getString(Constants.company_ID));

            LogUtil.Print("Notes List ApiRequest params", "" + params);
//            Api. service = retrofit.create(RetrofitArrayAPI.class);
            Call<List<GsonNotesType>> call = request.getNotesList(params);

            if (list_note.size() == 0) {
                progressBar_notes.setVisibility(View.VISIBLE);
                cpb.setVisibility(View.VISIBLE);
            }
            call.enqueue(new Callback<List<GsonNotesType>>() {
                @Override
                public void onResponse(Call<List<GsonNotesType>> call, Response<List<GsonNotesType>> response) {
                    progressBar_notes.setVisibility(View.GONE);
                    cpb.setVisibility(View.GONE);
                    List<GsonNotesType> gson = response.body();

                    if (list_note.size() > 0)
                        list_note.clear();

                    list_note.addAll(gson);
                    FillTypeSpinnerData();
                }

                @Override
                public void onFailure(Call<List<GsonNotesType>> call, Throwable t) {
                    if (list_note.size() == 0) {
                        progressBar_notes.setVisibility(View.GONE);
                        cpb.setVisibility(View.GONE);
                    }
                    MyUtils.makeSnackbar(sp_notes, getResources().getString(R.string.text_server_error));
                    LogUtil.Print("===Notes List====", t.getMessage());
                }
            });

        } else {
            MyUtils.makeToast(getResources().getString(R.string.text_internet));
        }
    }

    private void FillTypeSpinnerData() {
        final List<String> temp_list = new ArrayList<>();
        temp_list.add("" + getResources().getString(R.string.text_select));
        for (int i = 0; i < list_note.size(); i++) {
            temp_list.add("" + list_note.get(i).getNoteType());
        }

        adp_Type = new ArrayAdapter<>(Notes.this, R.layout.lv_spinner_status, R.id.tv_spn_pool, temp_list);
        adp_Type.setDropDownViewResource(R.layout.lv_dropdown);
        sp_notes.setAdapter(adp_Type);
    }

    private void HeaderView() {
        lv_main_header = findViewById(R.id.lv_main_header);
        tv_title = findViewById(R.id.tv_title);

        tv_title.setVisibility(View.VISIBLE);
        tv_title.setText(getResources().getString(R.string.text_notes));
        iv_back = findViewById(R.id.iv_back);
        iv_back.setVisibility(View.VISIBLE);
        iv_back.setOnClickListener(this);

    }

    private void findViewById() {
        cpb = findViewById(R.id.cpb);
        progressBar_notes = findViewById(R.id.progressBar_notes);
        ck_check = findViewById(R.id.ck_check);
        sp_notes = findViewById(R.id.sp_notes);
        et_notes = findViewById(R.id.et_notes);
        btn_save = findViewById(R.id.btn_save);
    }


    /**
     * OnClickListener of Views
     */
    private void OnClickListener() {
        ck_check.setOnClickListener(this);
        btn_save.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;
            case R.id.btn_save:
                if (et_notes.getText().toString().isEmpty()) {
                    App.Utils.ShowAlert(Notes.this, getResources().getString(R.string.text_err_note), getResources().getString(R.string.app_name));
                } else if (sp_notes.getSelectedItemPosition() == 0) {
                    sp_notes.requestFocus();
                    App.Utils.ShowAlert(Notes.this, getResources().getString(R.string.text_err_notes), getResources().getString(R.string.app_name));

                } else {
                    if (ck_check.isChecked()) {
//                        MyUtils.makeToast("true");
                        p = "true";
                    } else {
//                        MyUtils.makeToast("false");
                        p = "false";
                    }
                    CallUploadNotesAPI();
                }
                break;
        }
    }

    private void CallUploadNotesAPI() {
        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.userID, userId);
            params.put(Constants.userName, App.Utils.getString(Constants.username));
            params.put(Constants.controlNumber, String.valueOf(getIntent().getExtras().getInt(Constants.controlNumber)));
            params.put(Constants.userNotes, et_notes.getText().toString());
            params.put(Constants.contactName, "");
            int indexNote = 0;
            if (sp_notes.getSelectedItemPosition() > 0) {
                indexNote = sp_notes.getSelectedItemPosition() - 1;
            }
            params.put(Constants.drpDocumentTypeID, "" + list_note.get(indexNote).getId());
            params.put(Constants.publicStatus, p);

            LogUtil.Print("Notes_params", "" + params);

            Call<GsonCompleted> call = request.uploadNotes(params);
            cpb.setVisibility(View.VISIBLE);
            call.enqueue(new Callback<GsonCompleted>() {
                @Override
                public void onResponse(Call<GsonCompleted> call, Response<GsonCompleted> response) {
                    LogUtil.Print("response", response.toString());
                    GsonCompleted gson = response.body();
                    cpb.setVisibility(View.GONE);
                    if (response.body() == null) {
                        MyUtils.ShowAlert(Notes.this, "Falied to Upload Notes", getResources().getString(R.string.app_name));
                    } else {
                        if (gson.getStatus() == 1) {
                            if (listener != null) {
                                listener.onItemClickRefresh(true);
                            }
                            MyUtils.ShowSuccessAlert(Notes.this, "Notes Upload Successfully", getResources().getString(R.string.app_name));
                            //finish();
                        }
                    }
                }
                @Override
                public void onFailure(Call<GsonCompleted> call, Throwable t) {
                    LogUtil.Print("Error", t.getMessage());
                    cpb.setVisibility(View.GONE);
                    MyUtils.makeSnackbar(btn_save, getResources().getString(R.string.text_server_error));
                    LogUtil.Print(TAG, "Failure ");
                }
            });
        } else {
            MyUtils.ShowAlert(Notes.this, getResources().getString(R.string.text_internet), getResources().getString(R.string.app_name));
        }
    }

}