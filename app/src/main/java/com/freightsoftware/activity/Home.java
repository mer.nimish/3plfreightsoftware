package com.freightsoftware.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.freightsoftware.App;
import com.freightsoftware.Constants.Api;
import com.freightsoftware.Constants.Constants;
import com.freightsoftware.Interface.ApiInterface;
import com.freightsoftware.Interface.DialogButtonClickListener;
import com.freightsoftware.Interface.RefreshListener;
import com.freightsoftware.R;
import com.freightsoftware.Utility.AppUtils;
import com.freightsoftware.Utility.GPSTracker;
import com.freightsoftware.Utility.LogUtil;
import com.freightsoftware.Utility.MyUtils;
import com.freightsoftware.Utility.NavigatorManager;
import com.freightsoftware.fragment.ClockIn;
import com.freightsoftware.fragment.HomeFragment;
import com.freightsoftware.model.GsonCompleted;
import com.freightsoftware.model.GsonContainerList;
import com.freightsoftware.service.UploadTrackingDataToFirebaseBGService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.freightsoftware.Constants.Constants.logID;

public class Home extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback, RefreshListener, GoogleMap.OnMapLoadedCallback {

    public static final HashMap<String, LatLng> LANDMARKS = new HashMap<String, LatLng>();

    private static final String TAG = "Home";

    public static DrawerLayout drawer_layout;
    public static List<GsonContainerList> list_container = new ArrayList<>();

    private DatabaseReference ref;
    private GeoFire geoFire;

    private NavigationView navigation_view;
    private Bundle bundle = new Bundle();
    private String id;

    private GoogleMap mMap;

    private ImageView iv_user;
    private TextView tv_username, tv_home, tv_clock, tv_logout;
    private View view_home, view_clock, view_logout;
    private FrameLayout fl_main;
    private FragmentManager mFragmentManager;
    private FragmentTransaction ft;
    private Dialog DialogLogout;
    private DisplayMetrics metrics;

    private ImageView img_close;
    private TextView tv_yes, tv_no, tv_title_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        LogUtil.Print(TAG, "FCM-->" + MyUtils.getString(Constants.fcm_registration_id));

        if (checkPlayServices()) {
            if (!AppUtils.isLocationEnabled(Home.this)) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(Home.this);
                dialog.setMessage("Location not enabled!");
                dialog.setPositiveButton("Open location settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                });
                dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        finish();
                    }
                });
                dialog.show();
            }
            App.gt = new GPSTracker(Home.this);
            App.gt.getLocation();
            startService(new Intent(Home.this, UploadTrackingDataToFirebaseBGService.class));
            checkLocationPermission();
        } else {
            MyUtils.makeToast("Location not supported in this device");
        }
        if (getIntent() != null && getIntent().hasExtra(Constants.push)) {
            if (getIntent().getExtras().getInt(Constants.push) == Constants.fromPush) {
            } else {
                callContainerAPI();
            }
        }
        ShipmentComplete.onRefreshClickListener(this);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        ref = FirebaseDatabase.getInstance().getReference("MyLocation");
        geoFire = new GeoFire(ref);

        drawer_layout = (DrawerLayout) findViewById(R.id.drawer_layout);
        fl_main = findViewById(R.id.fl_main);
        navigation_view = findViewById(R.id.navigation_view);
        iv_user = (ImageView) findViewById(R.id.iv_user);
        tv_username = findViewById(R.id.tv_username);
        tv_home = findViewById(R.id.tv_home);
        tv_clock = findViewById(R.id.tv_clock);
        tv_logout = findViewById(R.id.tv_logout);
        view_home = findViewById(R.id.view_home);
        view_clock = findViewById(R.id.view_clock);
        view_logout = findViewById(R.id.view_logout);

        if ((App.Utils.getString(Constants.user_id)).equalsIgnoreCase("")) {
            id = App.Utils.getString(Constants.userID);
        } else {
            id = App.Utils.getString(Constants.user_id);
        }

        if ((App.Utils.getString(Constants.username)).equalsIgnoreCase("")) {
        } else {
            tv_username.setText(App.Utils.getString(Constants.username));
        }

        tv_home.setOnClickListener(this);
        tv_clock.setOnClickListener(this);
        tv_logout.setOnClickListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer_layout, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                float xPositionOpenDrawer = navigation_view.getWidth();
                float xPositionWindowContent = (slideOffset * xPositionOpenDrawer);
                fl_main.setX(xPositionWindowContent);
                fl_main.setX(xPositionWindowContent);
            }
        };
        drawer_layout.setDrawerListener(toggle);
        toggle.syncState();

        getHomeFragment();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_home:
                setTabBackgroundColor(1);
                drawer_layout.closeDrawer(GravityCompat.START);
                bundle.putString(Constants.user_id, String.valueOf(id));
                Fragment fragment = new HomeFragment();
                fragment.setArguments(bundle);
                Fragment_Replace(fragment);
                break;
            case R.id.tv_clock:
                setTabBackgroundColor(2);
                SideMenuSelection(0);
                break;
            case R.id.tv_logout:
                SideMenuSelection(1);
                break;
            case R.id.tv_no:
                drawer_layout.closeDrawers();
                if (DialogLogout.isShowing()) {
                    DialogLogout.dismiss();
                }
                break;
            case R.id.img_close:
                drawer_layout.closeDrawers();
                if (DialogLogout.isShowing()) {
                    DialogLogout.dismiss();
                }
                break;
            case R.id.tv_yes:
                drawer_layout.closeDrawers();
                CallLogoutAPI();
                if (DialogLogout.isShowing()) {
                    DialogLogout.dismiss();
                }
                break;
        }
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, Constants.PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
            }
            return false;
        }
        return true;
    }

    private void getHomeFragment() {
        setTabBackgroundColor(1);
        drawer_layout.closeDrawer(GravityCompat.START);
        bundle.putString(Constants.user_id, String.valueOf(id));
        Fragment fragment = new HomeFragment();
        fragment.setArguments(bundle);
        Fragment_Replace(fragment);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (getIntent() != null && getIntent().hasExtra(Constants.push)) {
            if (getIntent().getExtras().getInt(Constants.push) == Constants.fromPush) {
            } else {
                if (!MyUtils.getString(Constants.driverID).equals("")) {
                    callDrawCircle();
                }
            }
        }
        mMap.setOnMapLoadedCallback(this);
    }

    @Override
    public void onMapLoaded() {
        //onMapReady(mMap);
    }

    private void setTabBackgroundColor(int i) {
        view_home.setVisibility((i == 1) ? View.VISIBLE : View.GONE);
        view_clock.setVisibility((i == 2) ? View.VISIBLE : View.GONE);
        int intClr = ContextCompat.getColor(Home.this, R.color.nav_color);
        tv_home.setBackgroundColor((i == 1) ? intClr : 0);
        tv_clock.setBackgroundColor((i == 2) ? intClr : 0);
    }

    private void SideMenuSelection(int i) {
        if (i == 0) {
            drawer_layout.closeDrawers();
            bundle.putString(Constants.user_id, String.valueOf(id));
            Fragment fragment = new ClockIn();
            fragment.setArguments(bundle);
            Fragment_Replace(fragment);
        } else if (i == 1) {
            Dialog_Logout();
        }
    }

    public void Fragment_Replace(Fragment fragment) {
        mFragmentManager = getSupportFragmentManager();
        ft = mFragmentManager.beginTransaction();
        ft.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        ft.replace(R.id.fl_main, fragment);
        ft.addToBackStack(fragment.getTag());
        ft.commit();
    }

    private void Dialog_Logout() {
        metrics = getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        DialogLogout = new Dialog(Home.this);
        DialogLogout.requestWindowFeature(Window.FEATURE_NO_TITLE);
        DialogLogout.setCancelable(true);
        DialogLogout.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        DialogLogout.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        DialogLogout.setContentView(R.layout.dialog_layout);
        img_close = (ImageView) DialogLogout.findViewById(R.id.img_close);
        tv_title_text = (TextView) DialogLogout.findViewById(R.id.tv_title_text);
        tv_yes = (TextView) DialogLogout.findViewById(R.id.tv_yes);
        tv_no = (TextView) DialogLogout.findViewById(R.id.tv_no);
        tv_title_text.setText(getResources().getString(R.string.text_are_you_sure_to_logout));
        img_close.setOnClickListener(this);
        tv_yes.setOnClickListener(this);
        tv_no.setOnClickListener(this);
        DialogLogout.show();
        DialogLogout.getWindow().setLayout((6 * width) / 7, ViewGroup.LayoutParams.WRAP_CONTENT);
        DialogLogout.getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_dialog_rounded));
    }

    public void callDrawCircle() {
        LatLng dangerous_area = null;
        String con = null;
        for (final Map.Entry<String, LatLng> entry : LANDMARKS.entrySet()) {
            dangerous_area = new LatLng(entry.getValue().latitude, entry.getValue().longitude);
            mMap.addCircle(new CircleOptions()
                    .center(dangerous_area)
                    .radius(100)
                    .fillColor(0x220000FF)
                    .strokeColor(Color.RED)
                    .strokeWidth(5.0f)
            );
            String enterloc = entry.getKey();
            String[] enter = enterloc.split(":");
            String SentNdata = enter[0];
            String controler_number = enter[1];
            LogUtil.Print("SentNdata: ", SentNdata + "controler_number: " + controler_number);
            GeoQuery geoQuery = geoFire.queryAtLocation(new GeoLocation(dangerous_area.latitude, dangerous_area.longitude), 0.1f);
            geoQuery.addGeoQueryEventListener(new GeoQueryEventListener() {
                @Override
                public void onKeyEntered(String key, GeoLocation location) {
                    String enterloc = entry.getKey();
                    String[] enter = enterloc.split(":");
                    String id = enter[0];
                    String controler_number = enter[1];
                    String from = MyUtils.removelastcharacter(enter[2]);
                    String strGeoFenceFromArrive = "0,0,0,0";
                    String strGeoFenceFromLeave = "1,0,0,0";
                    String strGeoFenceToArrive = "1,1,0,0";
                    String strGeoFenceToLeave = "1,1,1,0";
                    if (from.equals("FromLocation")) {
                        String chkGeoFence = MyUtils.getSPGeoFenceStr(getApplicationContext(), controler_number);
                        Log.d("TAG_GeoFenceFlag_" + controler_number, chkGeoFence);
                        if (chkGeoFence.equals(strGeoFenceFromArrive)) {
                            CallShipmentLogAPI(controler_number, Constants.arrivedFromLocation);
                            CallsaveDriverGeoFenceAPI(id, "0");
                            MyUtils.setSPGeoFenceStr(getApplicationContext(), controler_number, strGeoFenceFromLeave);
                            Log.d("TAG_shipmentLog_" + controler_number, "From Arrive");
                            Log.d("TAG_saveDriverGeoFence_" + controler_number, "0");
                        }
                    } else if (from.equals("ToLocation")) {
                        String chkGeoFence = MyUtils.getSPGeoFenceStr(getApplicationContext(), controler_number);
                        Log.d("TAG_GeoFenceFlag_" + controler_number, chkGeoFence);
                        if (chkGeoFence.equals(strGeoFenceToArrive)) {
                            CallShipmentLogAPI(controler_number, Constants.arrivedtoLocation);
                            CallsaveDriverGeoFenceAPI(id, "2");
                            MyUtils.setSPGeoFenceStr(getApplicationContext(), controler_number, strGeoFenceToLeave);
                            Log.d("TAG_shipmentLog_" + controler_number, "To Arrive");
                            Log.d("TAG_saveDriverGeoFence_" + controler_number, "2");
                        }
                    }
                    MyUtils.sendNotification(Home.this, getResources().getString(R.string.app_name), String.format("%s Please Report To Dispatch Arrival", MyUtils.getString(Constants.username)));
                }

                @Override
                public void onKeyExited(String key) {
                    String enterloc = entry.getKey();
                    String[] enter = enterloc.split(":");
                    String id = enter[0];
                    String controler_number = enter[1];
                    String from = MyUtils.removelastcharacter(enter[2]);
                    String strGeoFenceFromArrive = "0,0,0,0";
                    String strGeoFenceFromLeave = "1,0,0,0";
                    String strGeoFenceToArrive = "1,1,0,0";
                    String strGeoFenceToLeave = "1,1,1,0";
                    if (from.equals("FromLocation")) {
                        String chkGeoFence = MyUtils.getSPGeoFenceStr(getApplicationContext(), controler_number);
                        Log.d("TAG_GeoFenceFlag_" + controler_number, chkGeoFence);
                        if (chkGeoFence.equals(strGeoFenceFromLeave)) {
                            CallShipmentLogAPI(controler_number, Constants.leaveFromLocation);
                            CallsaveDriverGeoFenceAPI(id, "1");
                            MyUtils.setSPGeoFenceStr(getApplicationContext(), controler_number, strGeoFenceToArrive);
                            Log.d("TAG_shipmentLog_" + controler_number, "From Leave");
                            Log.d("TAG_saveDriverGeoFence_" + controler_number, "1");
                        }
                    } else if (from.equals("ToLocation")) {
                        String chkGeoFence = MyUtils.getSPGeoFenceStr(getApplicationContext(), controler_number);
                        Log.d("TAG_GeoFenceFlag_" + controler_number, chkGeoFence);
                        if (chkGeoFence.equals(strGeoFenceToLeave)) {
                            CallShipmentLogAPI(controler_number, Constants.leavetoLocation);
                            CallsaveDriverGeoFenceAPI(id, "3");
                            MyUtils.setSPGeoFenceStr(getApplicationContext(), controler_number, "1,1,1,1");
                            Log.d("TAG_shipmentLog_" + controler_number, "To Leave");
                            Log.d("TAG_saveDriverGeoFence_" + controler_number, "3");
                        }
                    }
                    LogUtil.Print("EXIT", String.format("%s is no longer in the the dangerous area", entry.getKey()));
                    MyUtils.sendNotification(Home.this, getResources().getString(R.string.app_name), String.format("%s Please Report To Dispatch Departure", MyUtils.getString(Constants.username)));
                }

                @Override
                public void onKeyMoved(String key, GeoLocation location) {
                    Log.d("MOVE", String.format("%s moved within the dangerous area [%f/%f]", key, location.latitude, location.longitude));
                }

                @Override
                public void onGeoQueryReady() {
                }

                @Override
                public void onGeoQueryError(DatabaseError error) {
                    Log.e("ERROR", "" + error);
                }
            });
        }
    }

    private void callContainerAPI() {
        if (App.Utils.IsInternetOn()) {
            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.driverID, App.Utils.getString(Constants.driverID));

            LogUtil.Print(TAG, "ContainerList ApiRequest params Home" + params);

            Call<List<GsonContainerList>> call = request.getContainerList(params);
            call.enqueue(new Callback<List<GsonContainerList>>() {
                @Override
                public void onResponse(Call<List<GsonContainerList>> call, Response<List<GsonContainerList>> response) {
                    //cpb.setVisibility(View.GONE);
                    List<GsonContainerList> gson = response.body();
                    if (response.body() == null) {
                    } else {
                        if (list_container.size() > 0) {
                            list_container.clear();
                        }
                        list_container.addAll(gson);
                        int ldata = 1;
                        for (int i = 0; i < list_container.size(); i++) {
                            if (list_container.get(i).getToCoordinates() == null) {
                            } else {
                                String latlng = list_container.get(i).getToCoordinates();
                                String[] lat = latlng.split(":");
                                String lati = lat[0];
                                String longe = lat[1];
                                LogUtil.Print(TAG, "Lat====" + lati + "Lng====" + longe);
                                LANDMARKS.put(list_container.get(i).getId() + ":" + list_container.get(i).getControlNumber() + ":" + String.valueOf("ToLocation" + ldata), new LatLng(Double.valueOf(lati), Double.valueOf(longe)));
                                ldata++;
                            }
                            if (list_container.get(i).getFromCoordinates() != null) {
                                String fromlatlog = list_container.get(i).getFromCoordinates();
                                String[] fromlat = fromlatlog.split(":");
                                String fromati = fromlat[0];
                                String fromlonge = fromlat[1];
                                LANDMARKS.put(list_container.get(i).getId() + ":" + list_container.get(i).getControlNumber() + ":" + String.valueOf("FromLocation" + ldata), new LatLng(Double.valueOf(fromati), Double.valueOf(fromlonge)));
                                ldata++;
                            }
                        }
                        LogUtil.Print(TAG, "Landmark:--" + LANDMARKS);
                        LogUtil.Print(TAG, "Landmark:--" + list_container);
                        onMapReady(mMap);
                    }
                }

                @Override
                public void onFailure(Call<List<GsonContainerList>> call, Throwable t) {
                    if (list_container.size() == 0)
                        LogUtil.Print("===ContainerList====", t.getMessage());
                }
            });
        } else {
            MyUtils.ShowAlert(Home.this, getResources().getString(R.string.text_internet), getResources().getString(R.string.app_name));
        }
    }

    private void CallShipmentLogAPI(String controler_number, String logID) {
        if (App.Utils.IsInternetOn()) {
            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.userID, id);
            params.put(Constants.controlNumber, controler_number);
            params.put(Constants.userName, App.Utils.getString(Constants.username));
            params.put(Constants.logID, logID);
            LogUtil.Print(TAG, "ShipmentLg_Submit_params" + params);
            Call<GsonCompleted> call = request.saveShipmentLog(params);
            call.enqueue(new Callback<GsonCompleted>() {
                @Override
                public void onResponse(Call<GsonCompleted> call, Response<GsonCompleted> response) {
                    LogUtil.Print("response", response.toString());
                    GsonCompleted gson = response.body();
                    LogUtil.Print(TAG, "Responce===" + response.body());
                    if (response.body() == null) {
                        MyUtils.ShowAlert(Home.this, "Falied to Submit Shipment Log", getResources().getString(R.string.app_name));
                    } else {
                        int newLogID = App.Utils.getInt(Constants.logID);
                        newLogID++;
                        MyUtils.putInt(Constants.logID, newLogID);
                        LogUtil.Print(TAG, "Shimpment Status: " + gson.getStatus());
                    }
                }

                @Override
                public void onFailure(Call<GsonCompleted> call, Throwable t) {
                    LogUtil.Print("Error", t.getMessage());
                    LogUtil.Print(TAG, "Failure ");
                }
            });
        } else {
            MyUtils.ShowAlert(Home.this, getResources().getString(R.string.text_internet), getResources().getString(R.string.app_name));
        }
    }

    private void CallsaveDriverGeoFenceAPI(String id, String s) {
        if (App.Utils.IsInternetOn()) {
            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.id, id);
            params.put(Constants.logType, s);
            LogUtil.Print(TAG, "CallsaveDriverGeoFenceAPI" + params);
            Call<GsonCompleted> call = request.saveDriverGeoFence(params);
            call.enqueue(new Callback<GsonCompleted>() {
                @Override
                public void onResponse(Call<GsonCompleted> call, Response<GsonCompleted> response) {
                    LogUtil.Print("response", response.toString());
                    GsonCompleted gson = response.body();
                    LogUtil.Print(TAG, "Responce===" + response.body());
                    if (gson.getStatus() == 0) {
                        LogUtil.Print(TAG, "Falied to Submit Geo Fences: " + gson.getStatus());
                    } else {
                        LogUtil.Print(TAG, "Submit Geo Fences: " + gson.getStatus());
                    }
                }

                @Override
                public void onFailure(Call<GsonCompleted> call, Throwable t) {
                    LogUtil.Print("Error", t.getMessage());
                    LogUtil.Print(TAG, "Failure ");
                }
            });
        } else {
            MyUtils.ShowAlert(Home.this, getResources().getString(R.string.text_internet), getResources().getString(R.string.app_name));
        }
    }

    private void CallLogoutAPI() {
        if (App.Utils.IsInternetOn()) {
            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.userID, id);
            LogUtil.Print("Logout_params", "" + params);
            Call<GsonCompleted> call = request.driverLogout(params);
            call.enqueue(new Callback<GsonCompleted>() {
                @Override
                public void onResponse(Call<GsonCompleted> call, Response<GsonCompleted> response) {
                    LogUtil.Print("response", response.toString());
                    GsonCompleted gson = response.body();
                    LogUtil.Print(TAG, "Responce===" + response.body());
                    if (response.body() == null) {
                        MyUtils.ShowAlert(Home.this, "Falied to Logout", getResources().getString(R.string.app_name));
                    } else {
                        if (gson.getStatus() == 1) {
                            stopService(new Intent(Home.this, UploadTrackingDataToFirebaseBGService.class));
                            int newLogID = App.Utils.getInt(logID);
                            String username = MyUtils.getString(Constants.userEmail);
                            String password = MyUtils.getString(Constants.userPassword);
                            String fcm_registration_id;
                            fcm_registration_id = MyUtils.getString(Constants.fcm_registration_id);
                            newLogID++;
                            MyUtils.ClearAllPreferences();
                            MyUtils.putString(Constants.userEmail, username);
                            MyUtils.putString(Constants.fcm_registration_id, fcm_registration_id);
                            MyUtils.putString(Constants.userPassword, password);
                            MyUtils.putInt(logID, newLogID);
                            NavigatorManager.startNewActivity(Home.this, new Intent(Home.this, Login.class));
                            finish();
                        }
                    }
                }

                @Override
                public void onFailure(Call<GsonCompleted> call, Throwable t) {
                    LogUtil.Print("Error", t.getMessage());
                    LogUtil.Print(TAG, "Failure ");
                }
            });
        } else {
            MyUtils.ShowAlert(Home.this, getResources().getString(R.string.text_internet), getResources().getString(R.string.app_name));
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START);
        } else {
            if (mFragmentManager.getBackStackEntryCount() == 0) {
                if (mFragmentManager.findFragmentById(R.id.fl_main) instanceof HomeFragment) {
                    finish();
                } else
                    super.onBackPressed();
            } else {
                if (mFragmentManager.findFragmentById(R.id.fl_main) instanceof ClockIn) {
                    getHomeFragment();
                } else if (mFragmentManager.findFragmentById(R.id.fl_main) instanceof HomeFragment) {
                    finish();
                } else
                    mFragmentManager.popBackStack();
            }
        }
    }

    @Override
    public void onItemClickRefresh(boolean flag) {
        if (flag) {
            list_container.clear();
            callContainerAPI();
        }
    }

    private void checkLocationPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            MyUtils.showPermissionsDialog(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, Constants.MY_PERMISSIONS_REQUEST_LOCATION);
            return;
        }
        App.gt = new GPSTracker(Home.this);
        App.gt.getLocation();
        startService(new Intent(Home.this, UploadTrackingDataToFirebaseBGService.class));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        LogUtil.Print(TAG, "requestCode " + requestCode);
        LogUtil.Print(TAG, "permissions " + Arrays.toString(permissions));
        switch (requestCode) {
            case Constants.MY_PERMISSIONS_REQUEST_LOCATION:
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        recreate();
                        checkLocationPermission();
                        mMap.setMyLocationEnabled(true);
                    } else {
                        MyUtils.showConfirmationDialog(Home.this, new DialogButtonClickListener() {
                                    @Override
                                    public void onPositiveButtonClick() {
                                        checkLocationPermission();
                                    }

                                    @Override
                                    public void onNegativeButtonClick() {
                                        finish();
                                    }
                                }, getResources().getString(R.string.text_location_permission_msg),
                                getResources().getString(R.string.text_ok),
                                getResources().getString(R.string.text_exit));
                    }
                }
                break;
        }
    }

}