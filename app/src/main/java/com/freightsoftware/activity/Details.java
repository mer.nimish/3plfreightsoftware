package com.freightsoftware.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.freightsoftware.App;
import com.freightsoftware.Constants.Api;
import com.freightsoftware.Constants.Constants;
import com.freightsoftware.Interface.ApiInterface;
import com.freightsoftware.Interface.RefreshListener;
import com.freightsoftware.R;
import com.freightsoftware.Utility.LogUtil;
import com.freightsoftware.Utility.MyUtils;
import com.freightsoftware.Utility.NavigatorManager;
import com.freightsoftware.model.GsonContainerList;
import com.freightsoftware.model.GsonDetails;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Details extends AppCompatActivity implements View.OnClickListener, RefreshListener {

    private static final String TAG = "Details";
    public static ImageView iv_back, iv_more;
    LinearLayout dim;
    PopupMenu popup;
    GsonContainerList gsonContainerList;
    TextView tv_cotainer_code, tv_move_type, tv_type, tv_container, tv_size, tv_to_location, tv_from_location, tv_from, tv_to_name, tv_from_time, tv_to_time, tv_to_ref, tv_from_ref;
    ImageView iv_map, iv_map_to;
    ProgressBar cpb;
    int controlNumber;
    String fromLocation, toLocation;
    /*header View*/
    private TextView tv_title;
    private LinearLayout lv_main_header, lv_container, lv_size;

    //new
    LinearLayout lv_booking, lv_pin, lv_seal, lv_release, lv_location, lv_shipment, lv_terminal, lv_fromt, lv_appt;
    TextView tv_booking, tv_pin, tv_seal, tv_release, tv_location, tv_shipment, tv_terminal, tv_fromt, tv_appt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        HeaderView();
        findViewById();

        main();

        //ShipmentComplete.onRefreshClickListener(this);
        //UpdateShipmentStatus.onRefreshClickListener(this);
        UpdateContainerStatus.onRefreshClickListener(this);
        //EmptyContainer.onRefreshClickListener(this);
        //CaptureSignature.onRefreshClickListener(this);
        //CaptureImage.onRefreshClickListener(this);
        //Notes.onRefreshClickListener(this);
    }

    private void HeaderView() {
        lv_main_header = findViewById(R.id.lv_main_header);
        tv_title = findViewById(R.id.tv_title);

        tv_title.setVisibility(View.VISIBLE);
        tv_title.setText(getResources().getString(R.string.text_details));
        iv_back = findViewById(R.id.iv_back);
        iv_back.setVisibility(View.VISIBLE);
        iv_back.setOnClickListener(this);

        iv_more = findViewById(R.id.iv_more);
        iv_more.setVisibility(View.VISIBLE);
        iv_more.setOnClickListener(this);
    }

    private void findViewById() {
        dim = (LinearLayout) findViewById(R.id.dim);
        cpb = findViewById(R.id.cpb);
        lv_container = findViewById(R.id.lv_container);
        lv_size = findViewById(R.id.lv_size);
        tv_cotainer_code = findViewById(R.id.tv_cotainer_code);
        tv_move_type = findViewById(R.id.tv_move_type);
        tv_type = findViewById(R.id.tv_type);
        tv_container = findViewById(R.id.tv_container);
        tv_size = findViewById(R.id.tv_size);
        tv_to_location = findViewById(R.id.tv_to_location);
        tv_from_location = findViewById(R.id.tv_from_location);
        tv_from = findViewById(R.id.tv_from);
        tv_to_name = findViewById(R.id.tv_to_name);
        iv_map = findViewById(R.id.iv_map);
        iv_map_to = findViewById(R.id.iv_map_to);
        iv_map.setOnClickListener(this);
        iv_map_to.setOnClickListener(this);


        lv_booking = findViewById(R.id.lv_booking);
        tv_booking = findViewById(R.id.tv_booking);
        lv_pin = findViewById(R.id.lv_pin);
        tv_pin = findViewById(R.id.tv_pin);
        lv_seal = findViewById(R.id.lv_seal);
        tv_seal = findViewById(R.id.tv_seal);
        lv_release = findViewById(R.id.lv_release);
        tv_release = findViewById(R.id.tv_release);
        lv_location = findViewById(R.id.lv_location);
        tv_location = findViewById(R.id.tv_location);
        lv_shipment = findViewById(R.id.lv_shipment);
        tv_shipment = findViewById(R.id.tv_shipment);
        lv_terminal = findViewById(R.id.lv_terminal);
        tv_terminal = findViewById(R.id.tv_terminal);
        lv_fromt = findViewById(R.id.lv_fromt);
        tv_fromt = findViewById(R.id.tv_fromt);
        lv_appt = findViewById(R.id.lv_appt);
        tv_appt = findViewById(R.id.tv_appt);
        tv_from_time = findViewById(R.id.tv_from_time);
        tv_to_time = findViewById(R.id.tv_to_time);
        tv_to_ref = findViewById(R.id.tv_to_ref);
        tv_from_ref = findViewById(R.id.tv_from_ref);
    }

    private void main() {

//        lv_size.setVisibility(View.GONE);
        CallDetailsAPI();
//        MyUtils.makeToast(""+getIntent().getExtras().getInt(Constants.id));
//        if (getIntent() != null) {
//            String Data = getIntent().getStringExtra(Constants.data);
//            gsonContainerList  = new GsonBuilder().create().fromJson(Data, GsonContainerList.class);
//            tv_cotainer_code.setText("# "+gsonContainerList.getClientCN());
//            tv_move_type.setText(gsonContainerList.getMoveType());
//            tv_type.setText(gsonContainerList.getShipmentType());
//            String detail = gsonContainerList.getFreightDetails();
//            String[] separated = detail.split("#");
//
//            tv_container.setText("#"+separated[1]);
//            tv_size.setText(separated[0]);
//
//        }
    }

    private void CallDetailsAPI() {
        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.id, "" + getIntent().getExtras().getInt(Constants.id));
            params.put(Constants.freightShipment, "" + getIntent().getExtras().getBoolean(Constants.freightShipment));
            LogUtil.Print("Details_params", "" + params);

            Call<GsonDetails> call = request.getContainerDetails(params);
            cpb.setVisibility(View.VISIBLE);
            call.enqueue(new Callback<GsonDetails>() {
                @Override
                public void onResponse(Call<GsonDetails> call, Response<GsonDetails> response) {
                    LogUtil.Print("response", response.toString());
                    cpb.setVisibility(View.GONE);

                    if (getIntent().getExtras().getBoolean(Constants.freightShipment)) {
                        if (response.body() == null) {

                            MyUtils.makeToast("Null");
                        } else {
                            GsonDetails gson = response.body();

                            tv_cotainer_code.setText("# " + gson.getClientCN());
                            tv_move_type.setText(gson.getMoveType());
                            tv_type.setText(gson.getTransactionType());

//                            if (gson.getContainerNumber() == "" || gson.getContainerNumber() == null)
                            lv_container.setVisibility(View.GONE);
//                            else {
//                                lv_container.setVisibility(View.VISIBLE);
//                                tv_container.setText("#" + gson.getContainerNumber());
//                            }


                            if (gson.getFreightDetails().equals(""))
                                lv_size.setVisibility(View.GONE);
                            else {
                                lv_size.setVisibility(View.VISIBLE);
                                tv_size.setText(gson.getFreightDetails());
                            }
                            tv_to_location.setText(gson.getToAddress());
                            tv_to_name.setText(gson.getToName());
                            tv_from.setText(gson.getFromName());

                            tv_from_location.setText(gson.getFromAddress());
                            controlNumber = gson.getControlNumber();
                            fromLocation = gson.getFromCoordinates();
                            toLocation = gson.getToCoordinates();
//                            tv_booking.setText(gson.getBookingNumber());
                            lv_booking.setVisibility(View.GONE);
                            lv_pin.setVisibility(View.GONE);
                            lv_seal.setVisibility(View.GONE);
                            lv_release.setVisibility(View.GONE);
                            lv_location.setVisibility(View.GONE);
                            lv_shipment.setVisibility(View.GONE);
                            lv_terminal.setVisibility(View.GONE);
                            lv_fromt.setVisibility(View.GONE);
                            lv_appt.setVisibility(View.GONE);
                            if (gson.getFromDate() == null && gson.getFromTime() == null)
                                tv_from_time.setVisibility(View.GONE);
                            else {
                                tv_from_time.setVisibility(View.VISIBLE);
                                tv_from_time.setText("Appt:" + gson.getFromDate() + " Time: " + gson.getFromTime());
                            }
                            if (gson.getToDate() == null && gson.getToTime() == null)
                                tv_to_time.setVisibility(View.GONE);
                            else {
                                tv_to_time.setVisibility(View.VISIBLE);
                                tv_to_time.setText("Appt:" + gson.getToDate() + " Time: " + gson.getToTime());
                            }
//                            if (gson.getFromDate().equals("") && gson.getFromTime().equals(""))
//                                tv_from_time.setVisibility(View.GONE);
//                            else {
//                                tv_from_time.setVisibility(View.VISIBLE);
//                                tv_from_time.setText("Appt:" + gson.getFromDate() + " Time: " + gson.getFromTime());
//                            }
//                            if (gson.getToDate().equals("") && gson.getToTime().equals(""))
//                                tv_to_time.setVisibility(View.GONE);
//                            else {
//                                tv_to_time.setVisibility(View.VISIBLE);
//                                tv_to_time.setText("Appt:" + gson.getToDate() + " Time: " + gson.getToTime());
//                            }
                            if (gson.getToRef().equals("")) {
                                tv_to_ref.setVisibility(View.GONE);
                            } else {
                                tv_to_ref.setVisibility(View.VISIBLE);
                                String data = gson.getToRef();
                                String[] items = data.split("\\|");
                                StringBuilder sb = new StringBuilder();
                                String prefix = "";
                                for (String str : items) {
                                    sb.append(prefix);
                                    prefix = "\n";
                                    sb.append(str);
                                }
//                                StringBuilder sb = new StringBuilder();
//                                for (String item : items)
//                                {
//                                    System.out.println("item = " + item);
//                                    sb.append(item+"\n");
//
//                                }
                                tv_to_ref.setText(sb.toString());
                            }
                            if (gson.getFromRef().equals("")) {
                                tv_from_ref.setVisibility(View.GONE);
                            } else {
                                tv_from_ref.setVisibility(View.VISIBLE);
                                String data = gson.getFromRef();
                                String[] items = data.split("\\|");
                                String ref = null;
                                StringBuilder sb = new StringBuilder();
                                String prefix = "";
                                for (String str : items) {
                                    sb.append(prefix);
                                    prefix = "\n";
                                    sb.append(str);
                                }
//                                StringBuilder sb = new StringBuilder();
//                                for (String item : items)
//                                {
//                                    System.out.println("item = " + item);
//                                    sb.append(item+"\n");
//                                }
                                tv_from_ref.setText(sb.toString());
                            }
                        }
                    } else {
                        GsonDetails gson = response.body();

                        tv_cotainer_code.setText("# " + gson.getClientCN());
                        tv_move_type.setText(gson.getMoveType());
                        tv_type.setText(gson.getTransactionType());
                        tv_container.setText(gson.getContainerNumber());
//                        tv_size.setText(gson.getFreightDetails());
                        tv_to_location.setText(gson.getToAddress());
                        tv_to_name.setText(gson.getToName());
                        tv_from.setText(gson.getFromName());
                        tv_from_location.setText(gson.getFromAddresss());
                        controlNumber = gson.getControlNumber();
                        fromLocation = gson.getFromCoordinates();
                        toLocation = gson.getToCoordinates();

                        if (gson.getFreightDetails().equals(""))
                            lv_size.setVisibility(View.GONE);
                        else {
                            lv_size.setVisibility(View.VISIBLE);
                            tv_size.setText(gson.getFreightDetails());
                        }

                        if (gson.getBookingNumber().equals(""))
                            lv_booking.setVisibility(View.GONE);
                        else {
                            lv_booking.setVisibility(View.VISIBLE);
                            tv_booking.setText(gson.getBookingNumber());
                        }

                        if (gson.getPin().equals(""))
                            lv_pin.setVisibility(View.GONE);
                        else {
                            lv_pin.setVisibility(View.VISIBLE);
                            tv_pin.setText(gson.getPin());
                        }

                        if (gson.getSealNumber().equals(""))
                            lv_seal.setVisibility(View.GONE);
                        else {
                            lv_seal.setVisibility(View.VISIBLE);
                            tv_seal.setText(gson.getSealNumber());
                        }

                        if (gson.getRailReleaseNumber().equals(""))
                            lv_release.setVisibility(View.GONE);
                        else {
                            lv_release.setVisibility(View.VISIBLE);
                            tv_release.setText(gson.getRailReleaseNumber());
                        }

                        if (gson.getRailLocation().equals(""))
                            lv_location.setVisibility(View.GONE);
                        else {
                            lv_location.setVisibility(View.VISIBLE);
                            tv_location.setText(gson.getRailLocation());
                        }

                        if (gson.getSteamShipline().equals(""))
                            lv_shipment.setVisibility(View.GONE);
                        else {
                            lv_shipment.setVisibility(View.VISIBLE);
                            tv_shipment.setText(gson.getSteamShipline());
                        }
//
//                        if (gson.getTerminalApptDate().equals(""))
                        lv_terminal.setVisibility(View.GONE);
//                        else {
//                            lv_terminal.setVisibility(View.VISIBLE);
//                            tv_terminal.setText(""+gson.getTerminalApptDate());
//                        }

//                        if (gson.getTerminalAppTimes().equals(""))
                        lv_fromt.setVisibility(View.GONE);
//                        else {
//                            lv_fromt.setVisibility(View.VISIBLE);
//                            tv_fromt.setText(""+gson.getTerminalAppTimes());
//                        }

//                        if (gson.getTerminalAppointmentNum().equals(""))
                        lv_appt.setVisibility(View.GONE);
//                        else {
//                            lv_appt.setVisibility(View.VISIBLE);
//                            tv_appt.setText("#" +gson.getTerminalAppointmentNum());
//                        }

                        if (gson.getFromDate() == null && gson.getFromTime() == null)
                            tv_from_time.setVisibility(View.GONE);
                        else {
                            tv_from_time.setVisibility(View.VISIBLE);
                            tv_from_time.setText("Appt:" + gson.getFromDate() + " Time: " + gson.getFromTime());
                        }
                        if (gson.getToDate() == null && gson.getToTime() == null)
                            tv_to_time.setVisibility(View.GONE);
                        else {
                            tv_to_time.setVisibility(View.VISIBLE);
                            tv_to_time.setText("Appt:" + gson.getToDate() + " Time: " + gson.getToTime());
                        }

//                        if (gson.getFromDate().equals("") && gson.getFromTime().equals("") && gson.getFromDate().equals(null) && gson.getFromTime().equals(null))
//                            tv_from_time.setVisibility(View.GONE);
//                        else {
//                            tv_from_time.setVisibility(View.VISIBLE);
//                            tv_from_time.setText("Appt:" + gson.getFromDate() + " Time: " + gson.getFromTime());
//                        }
//                        if (gson.getToDate().equals("") && gson.getToTime().equals("") && gson.getToDate().equals(null) && gson.getToTime().equals(null))
//                            tv_to_time.setVisibility(View.GONE);
//                        else {
//                            tv_to_time.setVisibility(View.VISIBLE);
//                            tv_to_time.setText("Appt:" + gson.getToDate() + " Time: " + gson.getToTime());
//                        }
                        if (gson.getToRef().equals("")) {
                            tv_to_ref.setVisibility(View.GONE);
                        } else {
                            tv_to_ref.setVisibility(View.VISIBLE);
                            String data = gson.getToRef();
                            String[] items = data.split("\\|");
                            StringBuilder sb = new StringBuilder();
                            String prefix = "";
                            for (String str : items) {
                                sb.append(prefix);
                                prefix = "\n";
                                sb.append(str);
                            }
//                            StringBuilder sb = new StringBuilder();
//                            for (String item : items)
//                            {
//                                System.out.println("item = " + item);
//                                sb.append(item+"\n");
//
//                            }
                            tv_to_ref.setText(sb.toString());
                        }
                        if (gson.getFromRef().equals("")) {
                            tv_from_ref.setVisibility(View.GONE);
                        } else {
                            tv_from_ref.setVisibility(View.VISIBLE);
                            String data = gson.getFromRef();
                            String[] items = data.split("\\|");
                            String ref = null;
                            StringBuilder sb = new StringBuilder();
                            String prefix = "";
                            for (String str : items) {
                                sb.append(prefix);
                                prefix = "\n";
                                sb.append(str);
                            }
//                            number = sb.toString();
//                            StringBuilder sb = new StringBuilder();
//                            for (String item : items)
//                            {
//                                System.out.println("item = " + item);
//                                sb.append(item+"\n");
//                            }
                            tv_from_ref.setText(sb.toString());
                        }
                    }

                }

                @Override
                public void onFailure(Call<GsonDetails> call, Throwable t) {
                    LogUtil.Print("Error", t.getMessage());
                    cpb.setVisibility(View.GONE);
                    MyUtils.makeSnackbar(tv_to_location, getResources().getString(R.string.text_server_error));
                    LogUtil.Print(TAG, "Failure ");
                }
            });

        } else {

            MyUtils.ShowAlert(Details.this, getResources().getString(R.string.text_internet), getResources().getString(R.string.app_name));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.iv_back:
                onBackPressed();
                break;
            case R.id.iv_more:
                showFilterPopup(v);
                break;
            case R.id.iv_map:
                String fromlat = null, fromlon = null, tolat = null, tolon = null;
                if (fromLocation != null) {
                    String[] fromlatlon = fromLocation.split(":");
                    fromlat = fromlatlon[0];
                    fromlon = fromlatlon[1];
                    LogUtil.Print(TAG, "FromLat====" + fromlat + "FromLng====" + fromlon);
                }
                if (toLocation != null) {
                    String[] tolatlon = toLocation.split(":");
                    tolat = tolatlon[0];
                    tolon = tolatlon[1];
                    LogUtil.Print(TAG, "toLat====" + tolat + "toLng====" + tolon);
                }

                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr=" + fromlat + "," + fromlon + "&daddr=" + tolat + "," + tolon));
                startActivity(intent);

                break;
            case R.id.iv_map_to:
                String fromlat1 = null, fromlon1 = null, tolat1 = null, tolon1 = null;
                if (fromLocation != null) {
                    String[] fromlatlon = fromLocation.split(":");
                    fromlat1 = fromlatlon[0];
                    fromlon1 = fromlatlon[1];
                    LogUtil.Print(TAG, "FromLat====" + fromlat1 + "FromLng====" + fromlon1);
                }
                if (toLocation != null) {
                    String[] tolatlon = toLocation.split(":");
                    tolat1 = tolatlon[0];
                    tolon1 = tolatlon[1];
                    LogUtil.Print(TAG, "toLat====" + tolat1 + "toLng====" + tolon1);
                }

                Intent intent1 = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr=" + fromlat1 + "," + fromlon1 + "&daddr=" + tolat1 + "," + tolon1));
                startActivity(intent1);

                break;

        }
    }

    private void showFilterPopup(View v) {
        popup = new PopupMenu(Details.this, v);
        // Inflate the menu from xml
        popup.inflate(R.menu.poupup_menu);


        //Visibility set of menu
        Menu popupMenu = popup.getMenu();
        popupMenu.findItem(R.id.it_shipmentdetail).setVisible(false);
        if (getIntent().getExtras().getBoolean(Constants.freightShipment)) {
            popupMenu.findItem(R.id.it_freight_details).setVisible(true);
            popupMenu.findItem(R.id.it_update_container).setVisible(false);
            popupMenu.findItem(R.id.it_empty_container).setVisible(false);
        } else {
            popupMenu.findItem(R.id.it_freight_details).setVisible(false);
            popupMenu.findItem(R.id.it_update_container).setVisible(true);
            popupMenu.findItem(R.id.it_empty_container).setVisible(true);
        }

        // Setup menu item selection

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.it_home:
                        Intent home = new Intent(Details.this, Home.class);
                        NavigatorManager.startNewActivity(Details.this, home);
                        return true;
                    case R.id.it_shipmentdetail:
//                        Intent geo = new Intent(Details.this, MapsActivity.class);
//                        NavigatorManager.startNewActivity(Details.this, geo);
                        popup.dismiss();
                        return true;
                    case R.id.it_captureimage:
                        Intent i = new Intent(Details.this, CaptureImage.class);
                        i.putExtra(Constants.controlNumber, controlNumber);
//                        i.putExtra(Constants.user_id, userId);
                        NavigatorManager.startNewActivity(Details.this, i);
                        return true;
                    case R.id.it_capture_signature:
                        Intent sign = new Intent(Details.this, CaptureSignature.class);
                        sign.putExtra(Constants.controlNumber, controlNumber);
//                        sign.putExtra(Constants.user_id, userId);
                        NavigatorManager.startNewActivity(Details.this, sign);
                        return true;
                    case R.id.it_shipmentstatus:
                        Intent upship = new Intent(Details.this, UpdateShipmentStatus.class);
                        upship.putExtra(Constants.controlNumber, controlNumber);
                        NavigatorManager.startNewActivity(Details.this, upship);
                        return true;
                    case R.id.it_update_container:
                        Intent upcontainer = new Intent(Details.this, UpdateContainerStatus.class);
                        upcontainer.putExtra(Constants.controlNumber, controlNumber);
                        NavigatorManager.startNewActivity(Details.this, upcontainer);
                        return true;
                    case R.id.it_waiting_time:
                        Intent waiting = new Intent(Details.this, WaitingTime.class);
                        waiting.putExtra(Constants.controlNumber, controlNumber);
                        waiting.putExtra(Constants.freightShipment, getIntent().getExtras().getBoolean(Constants.freightShipment));
                        NavigatorManager.startNewActivity(Details.this, waiting);
                        return true;
                    case R.id.it_freight_details:
                        Intent detail = new Intent(Details.this, FreightDetails.class);
                        detail.putExtra(Constants.freightShipment, getIntent().getExtras().getBoolean(Constants.freightShipment));
                        detail.putExtra(Constants.cn, getIntent().getExtras().getInt(Constants.cn));
                        detail.putExtra(Constants.controlNumber, getIntent().getExtras().getInt(Constants.controlNumber));
                        NavigatorManager.startNewActivity(Details.this, detail);
                        return true;
                    case R.id.it_empty_container:
                        Intent emptycontainer = new Intent(Details.this, EmptyContainer.class);
                        emptycontainer.putExtra(Constants.controlNumber, controlNumber);
                        NavigatorManager.startNewActivity(Details.this, emptycontainer);
                        return true;
                    case R.id.it_notes:
                        Intent notes = new Intent(Details.this, Notes.class);
                        notes.putExtra(Constants.controlNumber, controlNumber);
                        notes.putExtra(Constants.id, getIntent().getExtras().getInt(Constants.id));
                        NavigatorManager.startNewActivity(Details.this, notes);
                        return true;
                    case R.id.it_completed:
                        Intent complete = new Intent(Details.this, ShipmentComplete.class);
                        complete.putExtra(Constants.id, getIntent().getExtras().getInt(Constants.id));
                        NavigatorManager.startNewActivity(Details.this, complete);
                        return true;
                    default:
                        return false;
                }
            }

        });
        // Handle dismissal with: popup.setOnDismissListener(...);
        // Show the menu


        dim.setVisibility(View.VISIBLE);
        popup.show();

        popup.setOnDismissListener(new PopupMenu.OnDismissListener() {
            @Override
            public void onDismiss(PopupMenu menu) {
                dim.setVisibility(View.INVISIBLE);
            }
        });

    }

    @Override
    public void onItemClickRefresh(boolean flag) {
        if (flag) {
            CallDetailsAPI();
        }
    }
}