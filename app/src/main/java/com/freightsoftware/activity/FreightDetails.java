package com.freightsoftware.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.freightsoftware.App;
import com.freightsoftware.Constants.Api;
import com.freightsoftware.Constants.Constants;
import com.freightsoftware.Interface.ApiInterface;
import com.freightsoftware.Interface.RecyclerViewItemClickListener;
import com.freightsoftware.Interface.RefreshListener;
import com.freightsoftware.R;
import com.freightsoftware.Utility.LogUtil;
import com.freightsoftware.Utility.MyUtils;
import com.freightsoftware.adapter.FreightDetailsAdapter;
import com.freightsoftware.model.GsonDeleteDims;
import com.freightsoftware.model.GsonFreightDetails;
import com.freightsoftware.model.GsonFreightDetailsList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FreightDetails extends AppCompatActivity implements View.OnClickListener, RecyclerViewItemClickListener {

    private static final String TAG = "FreightDetails";
    public static ImageView iv_back;
    public static List<GsonFreightDetailsList> list_details = new ArrayList<>();
    public static List<GsonFreightDetails> list_type = new ArrayList<>();
    ArrayAdapter<String> adp_Type;
    ProgressBar progressBar_type, cpb;
    FreightDetailsAdapter fAdp;
    String id,p;
    /*header View*/
    private TextView tv_title;
    private LinearLayout lv_main_header;
    private EditText et_qty, et_weight, et_length, et_width, et_height, et_dim, et_desription;
    private CheckBox ck_check;
    private Spinner sp_type;
    private Button btn_add;
    private RecyclerView rv_details;
    private RecyclerView.LayoutManager mLayoutManager;
    String userID;

    public static RefreshListener listener;

    public static void onRefreshClickListener(RefreshListener listener1) {
        listener = listener1;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_freight_details);
        if ((App.Utils.getString(Constants.user_id)).equalsIgnoreCase("")) {
            userID = App.Utils.getString(Constants.userID);

        } else {
            userID = App.Utils.getString(Constants.user_id);
        }
        HeaderView();
        findViewById();
        OnClickListener();
        main();

        if (list_type.size() == 0)
            getTypeListAPIRequest();
        else
            FillTypeSpinnerData();

        CallFreightDtailsAPI();


    }

    private void getTypeListAPIRequest() {

        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.company_ID, App.Utils.getString(Constants.company_ID));
            params.put(Constants.freightShipment, ""+getIntent().getExtras().getBoolean(Constants.freightShipment));

            LogUtil.Print("Freight type List ApiRequest params", "" + params);
//            Api. service = retrofit.create(RetrofitArrayAPI.class);
            Call<List<GsonFreightDetails>> call = request.getFreightDetails(params);

            if (list_type.size() == 0) {
                progressBar_type.setVisibility(View.VISIBLE);
                cpb.setVisibility(View.VISIBLE);
            }
            call.enqueue(new Callback<List<GsonFreightDetails>>() {
                @Override
                public void onResponse(Call<List<GsonFreightDetails>> call, Response<List<GsonFreightDetails>> response) {
                    progressBar_type.setVisibility(View.GONE);
                    cpb.setVisibility(View.GONE);
                    List<GsonFreightDetails> gson = response.body();

                    if (list_type.size() > 0)
                        list_type.clear();

                    list_type.addAll(gson);
                    FillTypeSpinnerData();
                }

                @Override
                public void onFailure(Call<List<GsonFreightDetails>> call, Throwable t) {
                    if (list_type.size() == 0) {
                        progressBar_type.setVisibility(View.GONE);
                        cpb.setVisibility(View.GONE);
                    }
                    LogUtil.Print("===Notes List====", t.getMessage());
                }
            });

        } else {
            MyUtils.makeToast(getResources().getString(R.string.text_internet));
        }
    }

    private void FillTypeSpinnerData() {
        final List<String> temp_list = new ArrayList<>();
        temp_list.add("" + getResources().getString(R.string.text_select));
        for (int i = 0; i < list_type.size(); i++) {
            temp_list.add("" + list_type.get(i).getDrpTypeDesc());
        }

        adp_Type = new ArrayAdapter<>(FreightDetails.this, R.layout.lv_spinner_status, R.id.tv_spn_pool, temp_list);
        adp_Type.setDropDownViewResource(R.layout.lv_dropdown);
        sp_type.setAdapter(adp_Type);
    }



    private void HeaderView() {
        lv_main_header = findViewById(R.id.lv_main_header);
        tv_title = findViewById(R.id.tv_title);

        tv_title.setVisibility(View.VISIBLE);
        tv_title.setText(getResources().getString(R.string.text_freight_details));
        iv_back = findViewById(R.id.iv_back);
        iv_back.setVisibility(View.VISIBLE);
        iv_back.setOnClickListener(this);

    }

    private void findViewById() {
        cpb = findViewById(R.id.cpb);
        progressBar_type = findViewById(R.id.progressBar_type);
        et_qty = findViewById(R.id.et_qty);
        ck_check = findViewById(R.id.ck_check);
        sp_type = findViewById(R.id.sp_type);
        et_weight = findViewById(R.id.et_weight);
        et_length = findViewById(R.id.et_length);
        et_width = findViewById(R.id.et_width);
        et_height = findViewById(R.id.et_height);
        et_dim = findViewById(R.id.et_dim);
        et_desription = findViewById(R.id.et_desription);
        btn_add = findViewById(R.id.btn_add);
        rv_details = findViewById(R.id.rv_details);
        et_width.setText("0");
        et_height.setText("0");
        et_length.setText("0");
    }


    private void OnClickListener() {
        btn_add.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;
            case R.id.btn_add:
                if (et_qty.getText().toString().isEmpty()) {
                    MyUtils.ShowAlert(FreightDetails.this,getResources().getString(R.string.text_err_quantity), getResources().getString(R.string.app_name));
                } else if (sp_type.getSelectedItemPosition() == 0) {
                    MyUtils.ShowAlert(FreightDetails.this,getResources().getString(R.string.text_err_type), getResources().getString(R.string.app_name));
                } else if (et_weight.getText().toString().isEmpty()) {
                    MyUtils.ShowAlert(FreightDetails.this,getResources().getString(R.string.text_err_weight), getResources().getString(R.string.app_name));
                } else if (et_length.getText().toString().isEmpty()) {
                    MyUtils.ShowAlert(FreightDetails.this,getResources().getString(R.string.text_err_length), getResources().getString(R.string.app_name));
                } else if (et_height.getText().toString().isEmpty()) {
                    MyUtils.ShowAlert(FreightDetails.this,getResources().getString(R.string.text_err_height), getResources().getString(R.string.app_name));
                } else if (et_dim.getText().toString().isEmpty()) {
                    MyUtils.ShowAlert(FreightDetails.this,getResources().getString(R.string.text_err_dim), getResources().getString(R.string.app_name));
                } else {
                    if (ck_check.isChecked()) {
//                        MyUtils.makeToast("true");
                        p = "true";
                    } else {
//                        MyUtils.makeToast("false");
                        p = "false";
                    }
                    CallAddDimsAPI();
                }
                break;
        }

    }



    private void main() {
        mLayoutManager = new LinearLayoutManager(FreightDetails.this);
        rv_details.setLayoutManager(mLayoutManager);
        fAdp = new FreightDetailsAdapter(FreightDetails.this, list_details);
        fAdp.setOnRecyclerViewItemClickListener(this);
       /* int space = getResources().getDimensionPixelSize(R.dimen.margin_5dp);
        rv_menu.addItemDecoration(new SpacesItemDecoration(space));*/
        rv_details.setAdapter(fAdp);

    }

    private void CallAddDimsAPI() {
        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.id, "0");
            params.put(Constants.userID, userID);
            params.put(Constants.userName, App.Utils.getString(Constants.username));
            params.put(Constants.controlNumber, String.valueOf(getIntent().getExtras().getInt(Constants.controlNumber)));
//            params.put(Constants.cn, String.valueOf(getIntent().getExtras().getInt(Constants.cn)));
            params.put(Constants.dimFactor, et_dim.getText().toString());
            params.put(Constants.pieces, et_qty.getText().toString());
            params.put(Constants.txtDesc, et_desription.getText().toString());
            params.put(Constants.l, et_length.getText().toString());
            params.put(Constants.w, et_width.getText().toString());
            params.put(Constants.h, et_height.getText().toString());
            params.put(Constants.statedWeight, et_weight.getText().toString());
            params.put(Constants.drpID, list_type.get(sp_type.getSelectedItemPosition() - 1).getId());
            params.put(Constants.drpDesc, list_type.get(sp_type.getSelectedItemPosition() - 1).getDrpTypeDesc());
            params.put(Constants.hazMat, p);
//            params.put(Constants.driverID, App.Utils.getString(Constants.driverID));

            LogUtil.Print("Freight_Add_params", "" + params);

            Call<GsonDeleteDims> call = request.addDims(params);
            cpb.setVisibility(View.VISIBLE);
            call.enqueue(new Callback<GsonDeleteDims>() {
                @Override
                public void onResponse(Call<GsonDeleteDims> call, Response<GsonDeleteDims> response) {
                    LogUtil.Print("response", response.toString());


                    GsonDeleteDims gson = response.body();
                    cpb.setVisibility(View.GONE);
                    if (response.body() == null) {
                        MyUtils.ShowAlert(FreightDetails.this, "Falied to Add Dims", getResources().getString(R.string.app_name));
                    } else {
                        if (!gson.getDimID().equals("")) {
                            if (listener != null) {
                                listener.onItemClickRefresh(true);
                            }
                            CallFreightDtailsAPI();
                            MyUtils.ShowSuccessAlert(FreightDetails.this, "Add Dims Successfully", getResources().getString(R.string.app_name));
//                            finish();
                        }
                    }
                }

                @Override
                public void onFailure(Call<GsonDeleteDims> call, Throwable t) {
                    LogUtil.Print("Error", t.getMessage());
                    cpb.setVisibility(View.GONE);
                    LogUtil.Print(TAG, "Failure ");
                }
            });

        } else {

            MyUtils.ShowAlert(FreightDetails.this, getResources().getString(R.string.text_internet), getResources().getString(R.string.app_name));
        }
    }

    private void CallFreightDtailsAPI() {
        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.controlNumber, String.valueOf(getIntent().getExtras().getInt(Constants.controlNumber)));

            LogUtil.Print("DetailsList ApiRequest params", "" + params);
//            Api. service = retrofit.create(RetrofitArrayAPI.class);
            Call<List<GsonFreightDetailsList>> call = request.getFreightList(params);

            if (list_details.size() == 0) {
                cpb.setVisibility(View.VISIBLE);
            }
            call.enqueue(new Callback<List<GsonFreightDetailsList>>() {
                @Override
                public void onResponse(Call<List<GsonFreightDetailsList>> call, Response<List<GsonFreightDetailsList>> response) {
                    cpb.setVisibility(View.GONE);
                    List<GsonFreightDetailsList> gson = response.body();

                    if (list_details.size() > 0)
                        list_details.clear();

                    list_details.addAll(gson);
                    fAdp.notifyDataSetChanged();
                    et_dim.setText(list_details.get(0).getDimFactor());
                    LogUtil.Print(TAG, "Gson:--" + gson);
                }

                @Override
                public void onFailure(Call<List<GsonFreightDetailsList>> call, Throwable t) {
                    if (list_details.size() == 0) {
                        cpb.setVisibility(View.GONE);
                    }
                    list_details.clear();
//                    MyUtils.ShowAlert(FreightDetails.this, getResources().getString(R.string.text_internet), getResources().getString(R.string.app_name));
                    LogUtil.Print("===FreightList====", t.getMessage());
                }
            });

        } else {
            MyUtils.ShowAlert(FreightDetails.this, getResources().getString(R.string.text_internet), getResources().getString(R.string.app_name));
        }
    }

    @Override
    public void onItemClick(final int position, int flag, View view) {
        if (flag == Constants.ITEM_DELETE) {
//            id = list_details.get(position).getId();
            LogUtil.Print(TAG, "ID===" + list_details.get(position).getId() + "====CN====" + String.valueOf(getIntent().getExtras().getInt(Constants.cn)));

            AlertDialog.Builder builder = new AlertDialog.Builder(FreightDetails.this);
            builder.setMessage(R.string.text_delete_confirm)
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.text_yes), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            CallDeleteDimsAPI(list_details.get(position).getId());
                            dialog.cancel();
                        }
                    })
                    .setNegativeButton(getResources().getString(R.string.text_no), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    private void CallDeleteDimsAPI(String id) {
        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.id, id);
            params.put(Constants.controlNumber, String.valueOf(getIntent().getExtras().getInt(Constants.controlNumber)));
//            params.put(Constants.cn, String.valueOf(getIntent().getExtras().getInt(Constants.cn)));
            params.put(Constants.userID, userID);
            params.put(Constants.userName, App.Utils.getString(Constants.username));
//            params.put(Constants.driverID, App.Utils.getString(Constants.driverID));

            LogUtil.Print("Freight_Delete_params", "" + params);

            Call<GsonDeleteDims> call = request.deleteDims(params);
            cpb.setVisibility(View.VISIBLE);
            call.enqueue(new Callback<GsonDeleteDims>() {
                @Override
                public void onResponse(Call<GsonDeleteDims> call, Response<GsonDeleteDims> response) {
                    LogUtil.Print("response", response.toString());


                    GsonDeleteDims gson = response.body();
                    cpb.setVisibility(View.GONE);
                    if (response.body() == null) {
                        MyUtils.ShowAlert(FreightDetails.this, "Falied to Delete Dims", getResources().getString(R.string.app_name));
                    } else {
                        if (!gson.getDimID().equals("")) {
                            if (listener != null) {
                                listener.onItemClickRefresh(true);
                            }
                            CallFreightDtailsAPI();
                            finish();
                        }
                    }
                }

                @Override
                public void onFailure(Call<GsonDeleteDims> call, Throwable t) {
                    LogUtil.Print("Error", t.getMessage());
                    cpb.setVisibility(View.GONE);
                    LogUtil.Print(TAG, "Failure ");
                }
            });

        } else {

            MyUtils.ShowAlert(FreightDetails.this, getResources().getString(R.string.text_internet), getResources().getString(R.string.app_name));
        }
    }
}
