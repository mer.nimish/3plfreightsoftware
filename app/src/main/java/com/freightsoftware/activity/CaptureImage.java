package com.freightsoftware.activity;

import android.Manifest;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.freightsoftware.App;
import com.freightsoftware.BuildConfig;
import com.freightsoftware.Constants.Api;
import com.freightsoftware.Constants.Constants;
import com.freightsoftware.Interface.ApiInterface;
import com.freightsoftware.Interface.RefreshListener;
import com.freightsoftware.R;
import com.freightsoftware.Utility.CropActivity;
import com.freightsoftware.Utility.LogUtil;
import com.freightsoftware.Utility.MyUtils;
import com.freightsoftware.model.GsonCompleted;
import com.freightsoftware.model.GsonDocList;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CaptureImage extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "CaptureImage";
    public static ImageView iv_back, iv_more;
    public static List<GsonDocList> list_doc = new ArrayList<>();
    public static RefreshListener listener;
    final int CAMERA_REQUEST = 0, ACTION_REQUEST_GALLERY = 1;
    ProgressBar progressBar_doc, cpb;
    ArrayAdapter<String> adp_Type;
    //image
    String ImagePath = "";
    String encodedImage = "";
    Bitmap photo;
    String p;
    int file_size;
    String userId;
    /*header View*/
    private TextView tv_title;
    private LinearLayout lv_main_header;
    private Button btn_done;
    private Spinner sp_doc;
    private CheckBox ck_check;
    private ImageView iv_upload;
    private Uri mImageCaptureUri;

    public static void onRefreshClickListener(RefreshListener listener1) {
        listener = listener1;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capture_image);

        if ((App.Utils.getString(Constants.user_id)).equalsIgnoreCase("")) {
            userId = App.Utils.getString(Constants.userID);
//            MyUtils.makeToast("ID:--"+userId);
        } else {
            userId = App.Utils.getString(Constants.user_id);
//            MyUtils.makeToast("SessionID:--"+userId);
        }

        HeaderView();
        findViewById();
        OnClickListener();
        if (list_doc.size() == 0)
            getTypeListAPIRequest();
        else
            FillTypeSpinnerData();

    }

    private void HeaderView() {
        lv_main_header = findViewById(R.id.lv_main_header);
        tv_title = findViewById(R.id.tv_title);

        tv_title.setVisibility(View.VISIBLE);
        tv_title.setText(getResources().getString(R.string.text_capture_image));
        iv_back = findViewById(R.id.iv_back);
        iv_back.setVisibility(View.VISIBLE);
        iv_back.setOnClickListener(this);

    }

    private void findViewById() {
        cpb = findViewById(R.id.cpb);
        progressBar_doc = findViewById(R.id.progressBar_doc);
        btn_done = findViewById(R.id.btn_done);
        sp_doc = findViewById(R.id.sp_doc);
        ck_check = findViewById(R.id.ck_check);
        iv_upload = findViewById(R.id.iv_upload);
    }

    /**
     * OnClickListener of Views
     */
    private void OnClickListener() {

        btn_done.setOnClickListener(this);
        iv_upload.setOnClickListener(this);
        ck_check.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.iv_back:
                onBackPressed();
                break;
            case R.id.btn_done:
                if (sp_doc.getSelectedItemPosition() == 0) {
                    sp_doc.requestFocus();
                    MyUtils.ShowAlert(this, getResources().getString(R.string.text_err_doc), getResources().getString(R.string.app_name));
                } else if (encodedImage.equals("")) {
                    MyUtils.ShowAlert(this, getResources().getString(R.string.text_err_Image), getResources().getString(R.string.app_name));
                } else {
                    if (ck_check.isChecked()) {
//                        MyUtils.makeToast("true");
                        p = "true";
                    } else {
//                        MyUtils.makeToast("false");
                        p = "false";
                    }

                    CallImageAPI();

                }

                break;
            case R.id.iv_upload:
                chkPermission();
                break;

        }
    }

    private void upload() {
        // Image location URL
        LogUtil.Print("path", "----------------" + ImagePath);

        // Image
        Bitmap bm = BitmapFactory.decodeFile(ImagePath);
        ByteArrayOutputStream bao = new ByteArrayOutputStream();

//        bm = getResizedBitmap(bm, 1055);
        bm = resize(bm, 815,1055);
        bm.compress(Bitmap.CompressFormat.JPEG, 100, bao);
        //save image for testing to check image width or height
//        SaveImage(bm);
        byte[] ba = bao.toByteArray();
        encodedImage = Base64.encodeToString(ba, Base64.NO_WRAP);

        LogUtil.Print("Original base64", "-----" + encodedImage);

        // Upload image to server
        LogUtil.Print("Original   dimensions", bm.getWidth() + " " + bm.getHeight());

    }

    private void SaveImage(Bitmap finalBitmap) {



        String root = Environment.getExternalStorageDirectory().getAbsolutePath();
        File myDir = new File(root + "/saved_images");
        myDir.mkdirs();
        String fname = "IMG_" + "FreightSofteware" + "_"
                + System.currentTimeMillis() + ".jpg";

        File file = new File (myDir, fname);
        if (file.exists ()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static Bitmap resize(Bitmap image, int maxWidth, int maxHeight) {
        Bitmap background = Bitmap.createBitmap(maxWidth, maxHeight, Bitmap.Config.ARGB_8888);

        float originalWidth = image.getWidth();
        float originalHeight = image.getHeight();

        Canvas canvas = new Canvas(background);

        float scale = maxWidth / originalWidth;

        float xTranslation = 0.0f;
        float yTranslation = (maxHeight - originalHeight * scale) / 2.0f;

        Matrix transformation = new Matrix();
        transformation.postTranslate(xTranslation, yTranslation);
        transformation.preScale(scale, scale);

        Paint paint = new Paint();
        paint.setFilterBitmap(true);

        canvas.drawBitmap(image, transformation, paint);

        return background;
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    private void CallImageAPI() {
        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.userID, userId);
            params.put(Constants.userName, App.Utils.getString(Constants.username));
            params.put(Constants.company_ID, App.Utils.getString(Constants.company_ID));
            params.put(Constants.controlNumber, String.valueOf(getIntent().getExtras().getInt(Constants.controlNumber)));
            params.put(Constants.drpDocumentTypeID, "" + list_doc.get(sp_doc.getSelectedItemPosition() - 1).getID());
            params.put(Constants.drpDocumentType, list_doc.get(sp_doc.getSelectedItemPosition() - 1).getDocName());
            params.put(Constants.fileSize, "" + file_size);
            params.put(Constants.publicStatus, p);
            params.put(Constants.base64Image, encodedImage);
//            params.put(Constants.driverID, App.Utils.getString(Constants.driverID));

            LogUtil.Print("Update_Capture_Image_params", "" + params);

            Call<GsonCompleted> call = request.updateCaptureImage(params);
            cpb.setVisibility(View.VISIBLE);
            call.enqueue(new Callback<GsonCompleted>() {
                @Override
                public void onResponse(Call<GsonCompleted> call, Response<GsonCompleted> response) {
                    LogUtil.Print("response", response.toString());


                    GsonCompleted gson = response.body();
                    cpb.setVisibility(View.GONE);
                    if (response.body() == null) {

                        MyUtils.ShowAlert(CaptureImage.this, "Falied to Capture Image Container".toString(), getResources().getString(R.string.app_name));
                    } else {
                        if (gson.getStatus() == 1) {
                            if (listener != null) {
                                listener.onItemClickRefresh(true);
                            }
                            MyUtils.ShowSuccessAlert(CaptureImage.this, "Capture Image Successfully", getResources().getString(R.string.app_name));
//                            finish();
                        }
                    }
                }

                @Override
                public void onFailure(Call<GsonCompleted> call, Throwable t) {
                    LogUtil.Print("Error", t.getMessage());
                    cpb.setVisibility(View.GONE);
                    LogUtil.Print(TAG, "Failure ");
                    MyUtils.makeSnackbar(ck_check, getResources().getString(R.string.text_server_error));
                }
            });

        } else {

            MyUtils.ShowAlert(CaptureImage.this, getResources().getString(R.string.text_internet), getResources().getString(R.string.app_name));
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    private void getTypeListAPIRequest() {
        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.company_ID, App.Utils.getString(Constants.company_ID));

            LogUtil.Print("Doc List ApiRequest params", "" + params);
//            Api. service = retrofit.create(RetrofitArrayAPI.class);
            Call<List<GsonDocList>> call = request.getDocList(params);

            if (list_doc.size() == 0) {
                progressBar_doc.setVisibility(View.VISIBLE);
                cpb.setVisibility(View.VISIBLE);
            }
            call.enqueue(new Callback<List<GsonDocList>>() {
                @Override
                public void onResponse(Call<List<GsonDocList>> call, Response<List<GsonDocList>> response) {
                    progressBar_doc.setVisibility(View.GONE);
                    cpb.setVisibility(View.GONE);
                    List<GsonDocList> gson = response.body();

                    if (list_doc.size() > 0)
                        list_doc.clear();

                    list_doc.addAll(gson);
                    FillTypeSpinnerData();
                }

                @Override
                public void onFailure(Call<List<GsonDocList>> call, Throwable t) {
                    if (list_doc.size() == 0) {
                        progressBar_doc.setVisibility(View.GONE);
                        cpb.setVisibility(View.GONE);
                    }
                    MyUtils.makeSnackbar(sp_doc, getResources().getString(R.string.text_server_error));
                    LogUtil.Print("===Doc List====", t.getMessage());
                }
            });

        } else {
            MyUtils.makeToast(getResources().getString(R.string.text_internet));
        }
    }

    private void FillTypeSpinnerData() {

        final List<String> temp_list = new ArrayList<>();
        temp_list.add("" + getResources().getString(R.string.text_select));
        for (int i = 0; i < list_doc.size(); i++) {
            temp_list.add("" + list_doc.get(i).getDocName());
        }

        adp_Type = new ArrayAdapter<>(CaptureImage.this, R.layout.lv_spinner_status, R.id.tv_spn_pool, temp_list);
        adp_Type.setDropDownViewResource(R.layout.lv_dropdown);
        sp_doc.setAdapter(adp_Type);

    }


    /* camera permission*/
    private void chkPermission() {
        if (!(App.Utils.checkPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) &&
                App.Utils.checkPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE))) {
            App.Utils.showPermissionsDialog(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE}, Constants.REQUEST_CODE_STORAGE_PERMISSION);
            return;
        }
        if (!(App.Utils.checkPermission(this, Manifest.permission.CAMERA))) {

            App.Utils.showPermissionsDialog(this, new String[]{Manifest.permission.CAMERA},
                    Constants.REQUEST_CODE_CAMERA);
            return;
        }
        ShowImageChooserDialog();
    }

    /**
     * show Image chose Dialog
     */
    private void ShowImageChooserDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.app_name));
        CharSequence option[] = new CharSequence[]{getResources().getString(R.string.text_gallery), getResources().getString(R.string.text_camera)};
        builder.setItems(option, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    dialog.cancel();
                    LaunchGallery();
                } else {
                    dialog.cancel();
                    LaunchCamera();
                }
            }
        }).setPositiveButton(getResources().getString(R.string.text_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                dialog.cancel();
            }
        });
        builder.show();
    }

    /**
     * LaunchGallery
     */
    public void LaunchGallery() {
        Intent i = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(i,
                ACTION_REQUEST_GALLERY);


    }

//    /**
//     * LaunchCamera
//     */
//    public void LaunchCamera() {
//        // Check Camera
//        if (getApplicationContext().getPackageManager().hasSystemFeature(
//                PackageManager.FEATURE_CAMERA)) {
//            // Open default camera
//            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
//
//            // start the image capture Intent
//            startActivityForResult(intent, CAMERA_REQUEST);
//
//        } else {
//            Toast.makeText(getApplication(), "Camera not supported", Toast.LENGTH_LONG).show();
//        }
//
//    }

    /**
     * LaunchCamera
     */
    public void LaunchCamera() {
        Intent cameraIntent = new Intent(
                MediaStore.ACTION_IMAGE_CAPTURE);
        String filename = "IMG_" + "FreightSofteware" + "_"
                + System.currentTimeMillis() + ".jpg";
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, filename);
        File file = new File(Environment
                .getExternalStorageDirectory()
                .getAbsolutePath()
                + "/" + filename);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            cameraIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            mImageCaptureUri = FileProvider.getUriForFile(CaptureImage.this,
                    BuildConfig.APPLICATION_ID + ".provider",
                    file);
        } else {
            mImageCaptureUri = Uri.fromFile(file);
        }

        cameraIntent.putExtra(
                MediaStore.EXTRA_OUTPUT, mImageCaptureUri);

        startActivityForResult(cameraIntent, CAMERA_REQUEST);

    }

    /**
     * beginCrop method
     *
     * @param source
     */
    private void beginCrop(Uri source) {
        File file = null;
        file = new File(Constants.App_ROOT_FOLDER);
        file.mkdirs();


        file = new File(Constants.App_ROOT_FOLDER + "/" + Constants.IMAGE_FILE_NAME_PREFIX.replace("X", App.Utils.GetCurrentNanoTime()));

        System.out.println("file::" + file);
        Uri destination = Uri.fromFile(file);
        // Crop.of(source, destination).asSquare().start(this);
        startCrop(source);
    }

    /**
     * startCrop method.
     *
     * @param imageUri
     */
    private void startCrop(Uri imageUri) {
        LogUtil.Print(TAG, "imageUri..." + imageUri);
        Intent intent = new Intent(this, CropActivity.class);
        intent.setData(imageUri);
        intent.putExtra(Constants.FLAG_IS_SQUARE, false);
        startActivityForResult(intent, Constants.FLAG_CROP);
    }

    /**
     * get absolute path from URI
     *
     * @param contentUri
     * @return
     */
    public String getRealPathFromURI(Uri contentUri) {
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            Cursor cursor = this.managedQuery(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } catch (Exception e) {
            return contentUri.getPath();
        }
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        LogUtil.Print(TAG, "requestCode=======" + requestCode);
        LogUtil.Print(TAG, "resultCode=======" + resultCode);
        LogUtil.Print(TAG, "data=======" + data);
        if (requestCode == CAMERA_REQUEST
                && resultCode == RESULT_OK) {

            LogUtil.Print(TAG, "Inside camera Result=======");
//                mImageCaptureUri = data.getData();
            ImagePath = getRealPathFromURI(mImageCaptureUri);
            LogUtil.Print(TAG, "" + ImagePath);

            beginCrop(mImageCaptureUri);

//            Uri selectedImage;
//
//            selectedImage = data.getData();
//            photo = (Bitmap) data.getExtras().get("data");
//
//            ImagePath = getRealPathFromURI(selectedImage);
//            File file = new File(ImagePath);
//            file_size = Integer.parseInt(String.valueOf(file.length() / 1024));
//            LogUtil.Print(TAG, "FileSize===" + file_size);
//
//            // Cursor to get image uri to display
//
//            String[] filePathColumn = {MediaStore.Images.Media.DATA};
//            Cursor cursor = getContentResolver().query(selectedImage,
//                    filePathColumn, null, null, null);
//            cursor.moveToFirst();
//
//            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
//            ImagePath = cursor.getString(columnIndex);
//            cursor.close();
//
//            Bitmap photo = (Bitmap) data.getExtras().get("data");
//
//            iv_upload.setImageBitmap(photo);
//            upload();


        } else if (requestCode == ACTION_REQUEST_GALLERY
                && resultCode == RESULT_OK) {
            LogUtil.Print(TAG, "Inside gallery Result=======");


            try {

                Uri mGalleryImageCaptureUri = data.getData();
                mImageCaptureUri = data.getData();
                ImagePath = getRealPathFromURI(mGalleryImageCaptureUri);
                LogUtil.Print(TAG, "gallery====>" + ImagePath);
//                File file = new File(ImagePath);
//                if (file.exists()) {
//                    iv_product.setImageBitmap(App.Utils
//                            .RotateImageAfterPick(ImagePath));
//                    beginCrop(mImageCaptureUri);
//                } else {
//                    ImagePath = "";
//
//                    App.Utils.ShowAlert(this, getResources().getString(R.string.dialog_file_not_found), getResources().getString(R.string.text_title));
//                }
                beginCrop(mImageCaptureUri);
                LogUtil.Print(TAG, "ImagePath gallery.." + ImagePath);
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
                App.Utils.ShowAlert(this, getResources().getString(R.string.dialog_file_not_found), getResources().getString(R.string.app_name));
            }

//            try {
//
//                Uri mGalleryImageCaptureUri = data.getData();
//                mImageCaptureUri = data.getData();
//                ImagePath = getRealPathFromURI(mGalleryImageCaptureUri);
//                LogUtil.Print(TAG, "gallery====>" + ImagePath);
//
//                File file = new File(ImagePath);
//                file_size = Integer.parseInt(String.valueOf(file.length() / 1024));
//                LogUtil.Print(TAG, "FileSize===" + file_size);
//                iv_upload.setImageBitmap(App.Utils.RotateImageAfterPick(ImagePath));
//                upload();
////                Bitmap selectedImageBitmap = null;
////                try {
////                    selectedImageBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), mImageCaptureUri);
////                } catch (IOException e) {
////                    e.printStackTrace();
////                }
////                iv_upload.setImageBitmap(selectedImageBitmap);
////                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
////                selectedImageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
////                byte[] byteArrayImage = byteArrayOutputStream.toByteArray();
////                encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
////                LogUtil.Print(TAG, "Base64Image===" + encodedImage);
//                LogUtil.Print(TAG, "ImagePath gallery.." + ImagePath);
//            } catch (Exception e) {
//                // TODO: handle exception
//                e.printStackTrace();
//                App.Utils.ShowAlert(this, getResources().getString(R.string.dialog_file_not_found), getResources().getString(R.string.app_name));
//            }

        } else if (requestCode == Constants.FLAG_CROP
                && resultCode == this.RESULT_OK) {
            LogUtil.Print(TAG, "Inside FLAG_CROP=======");

            try {
                LogUtil.Print(TAG, "Inside FLAG_CROP===image====" + data.getStringExtra(Constants.image));
                ImagePath = data.getStringExtra(Constants.image);
                LogUtil.Print(TAG, "Inside FLAG_CROP==ImagePath=====" + ImagePath);
                File file = new File(ImagePath);
                if (file.exists()) {
                    file_size = Integer.parseInt(String.valueOf(file.length() / 1024));
                    LogUtil.Print(TAG, "Original FileSize===" + file_size);
                    iv_upload.setImageBitmap(App.Utils.RotateImageAfterPick(ImagePath));
                    upload();


//                    iv_product.setImageBitmap(App.Utils
//                            .RotateImageAfterPick(ImagePath));
                } else {
                    ImagePath = "";

                    App.Utils.ShowAlert(this, getResources().getString(R.string.dialog_file_not_found), getResources().getString(R.string.app_name));
                }

            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
                LogUtil.Print(TAG, "" + e);
                App.Utils.ShowAlert(this, getResources().getString(R.string.dialog_file_not_found), getResources().getString(R.string.app_name));
            }

        }
    }
}
