package com.freightsoftware.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.freightsoftware.App;
import com.freightsoftware.Constants.Api;
import com.freightsoftware.Constants.Constants;
import com.freightsoftware.Interface.ApiInterface;
import com.freightsoftware.Interface.RefreshListener;
import com.freightsoftware.R;
import com.freightsoftware.Utility.LogUtil;
import com.freightsoftware.Utility.MyUtils;
import com.freightsoftware.model.GsonCompleted;
import com.freightsoftware.model.GsonShipmentStatusList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateShipmentStatus extends AppCompatActivity implements View.OnClickListener {


    private static final String TAG = "UpdateShipmentStatus";
    public static ImageView iv_back, iv_more;
    public static List<GsonShipmentStatusList> list_status = new ArrayList<>();
    public static RefreshListener listener;
    Spinner sp_status;
    Button btn_update;
    ProgressBar progressBar_status, cpb;
    ArrayAdapter<String> adp_Type;
    /*header View*/
    private TextView tv_title;
    private LinearLayout lv_main_header;
    String userID;

    public static void onRefreshClickListener(RefreshListener listener1) {
        listener = listener1;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_shipment_status);

        if ((App.Utils.getString(Constants.user_id)).equalsIgnoreCase("")) {
            userID = App.Utils.getString(Constants.userID);

        } else {
            userID = App.Utils.getString(Constants.user_id);
        }

        HeaderView();
        findViewById();
        OnClickListener();
        if (list_status.size() == 0)
            getTypeListAPIRequest();
        else
            FillTypeSpinnerData();

//        MyUtils.makeToast(getIntent().getExtras().getString(Constants.controlNumber));
    }


    private void HeaderView() {
        lv_main_header = findViewById(R.id.lv_main_header);
        tv_title = findViewById(R.id.tv_title);

        tv_title.setVisibility(View.VISIBLE);
        tv_title.setText(getResources().getString(R.string.text_update_shipment_status));
        iv_back = findViewById(R.id.iv_back);
        iv_back.setVisibility(View.VISIBLE);
        iv_back.setOnClickListener(this);

    }

    private void findViewById() {
        cpb = findViewById(R.id.cpb);
        progressBar_status = findViewById(R.id.progressBar_status);
        sp_status = findViewById(R.id.sp_status);
        btn_update = findViewById(R.id.btn_update);

    }

    /**
     * OnClickListener of Views
     */
    private void OnClickListener() {

        btn_update.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.iv_back:
                onBackPressed();
                break;
            case R.id.btn_update:
                if (sp_status.getSelectedItemPosition() == 0) {
                    sp_status.requestFocus();
                    MyUtils.ShowAlert(UpdateShipmentStatus.this, getResources().getString(R.string.text_err_update_status), getResources().getString(R.string.app_name));
                } else {
                    callUpdateShipmentAPI();
                }
                break;

        }
    }

    private void callUpdateShipmentAPI() {
        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.controlNumber, "" + getIntent().getExtras().getInt(Constants.controlNumber));
            params.put(Constants.drpStatusID, "" + list_status.get(sp_status.getSelectedItemPosition() - 1).getId());
            params.put(Constants.userID, userID);
            params.put(Constants.userName, App.Utils.getString(Constants.username));
            LogUtil.Print("Update_Shipment_status_params", "" + params);

            Call<GsonCompleted> call = request.getUpdateShipmentStatus(params);
            cpb.setVisibility(View.VISIBLE);
            call.enqueue(new Callback<GsonCompleted>() {
                @Override
                public void onResponse(Call<GsonCompleted> call, Response<GsonCompleted> response) {
                    LogUtil.Print("response", response.toString());


                    GsonCompleted gson = response.body();
                    cpb.setVisibility(View.GONE);
                    if (gson.getStatus() == 1) {
                        if (listener != null) {
                            listener.onItemClickRefresh(true);
                        }
                        MyUtils.ShowSuccessAlert(UpdateShipmentStatus.this, "Successfully Update Shipmment Status", getResources().getString(R.string.app_name));
//                        finish();
                    }
                }

                @Override
                public void onFailure(Call<GsonCompleted> call, Throwable t) {
                    LogUtil.Print("Error", t.getMessage());
                    cpb.setVisibility(View.GONE);
                    MyUtils.makeSnackbar(btn_update, getResources().getString(R.string.text_server_error));
                    LogUtil.Print(TAG, "Failure ");
                }
            });

        } else {

            MyUtils.ShowAlert(UpdateShipmentStatus.this, getResources().getString(R.string.text_internet), getResources().getString(R.string.app_name));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    private void getTypeListAPIRequest() {
        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.company_ID, MyUtils.getString(Constants.company_ID));

            LogUtil.Print("Shipment Status List ApiRequest params", "" + params);
//            Api. service = retrofit.create(RetrofitArrayAPI.class);
            Call<List<GsonShipmentStatusList>> call = request.getShipStatusList(params);

            if (list_status.size() == 0) {
                progressBar_status.setVisibility(View.VISIBLE);
                cpb.setVisibility(View.VISIBLE);
            }
            call.enqueue(new Callback<List<GsonShipmentStatusList>>() {
                @Override
                public void onResponse(Call<List<GsonShipmentStatusList>> call, Response<List<GsonShipmentStatusList>> response) {
                    progressBar_status.setVisibility(View.GONE);
                    cpb.setVisibility(View.GONE);
                    List<GsonShipmentStatusList> gson = response.body();

                    if (list_status.size() > 0)
                        list_status.clear();

                    list_status.addAll(gson);
                    FillTypeSpinnerData();
                }

                @Override
                public void onFailure(Call<List<GsonShipmentStatusList>> call, Throwable t) {
                    if (list_status.size() == 0) {
                        progressBar_status.setVisibility(View.GONE);
                        cpb.setVisibility(View.GONE);
                    }
                    LogUtil.Print("===Shipment Status List====", t.getMessage());
                }
            });

        } else {
            MyUtils.ShowAlert(UpdateShipmentStatus.this, getResources().getString(R.string.text_internet), getResources().getString(R.string.app_name));
        }
    }

    private void FillTypeSpinnerData() {

        final List<String> temp_list = new ArrayList<>();
        temp_list.add("" + getResources().getString(R.string.text_select));
        for (int i = 0; i < list_status.size(); i++) {
            temp_list.add("" + list_status.get(i).getStatusDescription());
        }

        adp_Type = new ArrayAdapter<>(UpdateShipmentStatus.this, R.layout.lv_spinner_status, R.id.tv_spn_pool, temp_list);
        adp_Type.setDropDownViewResource(R.layout.lv_dropdown);
        sp_status.setAdapter(adp_Type);

    }

}
