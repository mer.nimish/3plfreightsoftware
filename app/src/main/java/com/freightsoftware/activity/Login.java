package com.freightsoftware.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.freightsoftware.App;
import com.freightsoftware.Constants.Api;
import com.freightsoftware.Constants.Constants;
import com.freightsoftware.Interface.ApiInterface;
import com.freightsoftware.R;
import com.freightsoftware.Utility.LogUtil;
import com.freightsoftware.Utility.MyUtils;
import com.freightsoftware.Utility.NavigatorManager;
import com.freightsoftware.model.GsonLogin;
import com.freightsoftware.model.GsonSaveFcm;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Login extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "Login";
    EditText et_username, et_password;
    CheckBox ck_check;
    Button btn_login;
    ProgressBar cpb;
    int logId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.RegisterActivityForBugsense(Login.this);
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            window.setStatusBarColor(Color.TRANSPARENT);
//            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        setContentView(R.layout.activity_login);

        if (MyUtils.getInt(Constants.logID) == -1) {
            logId = 100;
        } else {
            logId = MyUtils.getInt(Constants.logID);
        }

        findViewById();
    }

    private void findViewById() {
        cpb = findViewById(R.id.cpb);
        et_username = findViewById(R.id.et_username);
        et_password = findViewById(R.id.et_password);
        ck_check = findViewById(R.id.ck_check);
        btn_login = findViewById(R.id.btn_login);
        btn_login.setOnClickListener(this);
        ck_check.setOnClickListener(this);

        if (!MyUtils.getString(Constants.userEmail).equals("")) {
            et_username.setText(MyUtils.getString(Constants.userEmail));
        }
        if (!MyUtils.getString(Constants.userPassword).equals("")) {
            et_password.setText(MyUtils.getString(Constants.userPassword));
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ck_check:

                break;
            case R.id.btn_login:

                if (et_username.getText().toString().isEmpty()) {
                    App.Utils.ShowAlert(Login.this, getResources().getString(R.string.text_err_username), getResources().getString(R.string.app_name));
                } else if (!App.Utils.IsValidEmail(et_username.getText().toString())) {

                    App.Utils.ShowAlert(Login.this, getResources().getString(R.string.text_valid_email_empty), getResources().getString(R.string.app_name));
                } else if (et_password.getText().toString().trim().isEmpty()) {
                    App.Utils.ShowAlert(Login.this, getResources().getString(R.string.text_err_password), getResources().getString(R.string.app_name));

                } else if (et_password.getText().length() < 6) {
                    App.Utils.ShowAlert(Login.this, getResources().getString(R.string.text_password_limit), getResources().getString(R.string.app_name));

                } else {
                    CallLoginAPI();
                }

//                Intent i  = new Intent(Login.this, Home.class);
//                startActivity(i);
                break;

        }

    }

    private void CallLoginAPI() {
        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.userName, et_username.getText().toString());
            params.put(Constants.userPassword, et_password.getText().toString());
            LogUtil.Print("login_params", "" + params);

            Call<GsonLogin> call = request.getJSONLogin(params);
            cpb.setVisibility(View.VISIBLE);
            call.enqueue(new Callback<GsonLogin>() {
                @Override
                public void onResponse(Call<GsonLogin> call, Response<GsonLogin> response) {
                    LogUtil.Print("response", response.toString());


                    GsonLogin gson = response.body();
                    LogUtil.Print(TAG, "" + response.body());
                    cpb.setVisibility(View.GONE);
                    if ((gson.getUserName()).equalsIgnoreCase("")) {
                        MyUtils.ShowAlert(Login.this, "Invalid UserName or Password", getResources().getString(R.string.app_name));

                    } else {
                        CallSaveFcmAPI(gson);
                    }

                }


                @Override
                public void onFailure(Call<GsonLogin> call, Throwable t) {
                    LogUtil.Print("Error", t.getMessage());
                    cpb.setVisibility(View.GONE);
                    MyUtils.makeSnackbar(btn_login, getResources().getString(R.string.text_server_error));
                    LogUtil.Print(TAG, "Failure ");
                }
            });

        } else {

            MyUtils.ShowAlert(Login.this, getResources().getString(R.string.text_internet), getResources().getString(R.string.app_name));
        }
    }

    private void CallSaveFcmAPI(final GsonLogin gson) {
        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.userID, "" + gson.getUserID());
            params.put(Constants.deviceType, "" + Constants.DEVICE_TYPE_ANDROID);
            params.put(Constants.token, MyUtils.getString(Constants.fcm_registration_id));
            LogUtil.Print("Save FCM params", "" + params);

            Call<GsonSaveFcm> call = request.saveFcmUserAPI(params);
            cpb.setVisibility(View.VISIBLE);
            call.enqueue(new Callback<GsonSaveFcm>() {
                @Override
                public void onResponse(Call<GsonSaveFcm> call, Response<GsonSaveFcm> response) {
                    LogUtil.Print("response", response.toString());


                    GsonSaveFcm gson1 = response.body();
                    LogUtil.Print(TAG, "" + response.body());
                    cpb.setVisibility(View.GONE);
                    if (gson1.getStatus() == 0) {
                        MyUtils.ShowAlert(Login.this, getResources().getString(R.string.text_server_error), getResources().getString(R.string.app_name));
                    } else {
                        if (ck_check.isChecked()) {
                            MyUtils.putString(Constants.user_id, gson.getUserID());
                            MyUtils.putString(Constants.driverID, gson.getDriverID());
                            MyUtils.putString(Constants.company_ID, gson.getCompanyID());
                            MyUtils.putString(Constants.username, gson.getUserName());
                            MyUtils.putInt(Constants.logID, logId);
                            LogUtil.Print(TAG, "CompanyID: " + gson.getCompanyID());
                            LogUtil.Print(TAG, "UserName: " + gson.getUserName());
                            LogUtil.Print(TAG, "DriverID: " + gson.getDriverID());
                            LogUtil.Print(TAG, "UserID: " + gson.getUserID());
                            LogUtil.Print(TAG, "logID: " + logId);
                            Intent i1 = new Intent(Login.this, Home.class);
                            i1.putExtra(Constants.push, Constants.fromSplash);
                            NavigatorManager.startNewActivity(Login.this, i1);
                            finish();
                        } else {
                            MyUtils.putString(Constants.userEmail, et_username.getText().toString());
                            MyUtils.putString(Constants.userPassword, et_password.getText().toString());
                            MyUtils.putString(Constants.userID, gson.getUserID());
                            MyUtils.putString(Constants.driverID, gson.getDriverID());
                            MyUtils.putString(Constants.company_ID, gson.getCompanyID());
                            MyUtils.putString(Constants.username, gson.getUserName());
                            MyUtils.putInt(Constants.logID, logId);
//                            LogUtil.Print(TAG, "userPassword: " + MyUtils.getString(Constants.userPassword)+" userEmail "+ MyUtils.getString(Constants.userEmail));
                            Intent i = new Intent(Login.this, Home.class);
                            i.putExtra(Constants.push, Constants.fromSplash);
//                            i.putExtra(Constants.user_id, String.valueOf(gson.getUserID()));
                            NavigatorManager.startNewActivity(Login.this, i);
                            finish();
                        }

                    }

                }


                @Override
                public void onFailure(Call<GsonSaveFcm> call, Throwable t) {
                    LogUtil.Print("Error", t.getMessage());
                    cpb.setVisibility(View.GONE);
                    MyUtils.makeSnackbar(btn_login, getResources().getString(R.string.text_server_error));
                    LogUtil.Print(TAG, "Failure ");
                }
            });

        } else {

            MyUtils.ShowAlert(Login.this, getResources().getString(R.string.text_internet), getResources().getString(R.string.app_name));
        }
    }
}
