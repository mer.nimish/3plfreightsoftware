package com.freightsoftware.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.freightsoftware.App;
import com.freightsoftware.Constants.Api;
import com.freightsoftware.Constants.Constants;
import com.freightsoftware.Interface.ApiInterface;
import com.freightsoftware.Interface.RefreshListener;
import com.freightsoftware.R;
import com.freightsoftware.Utility.LogUtil;
import com.freightsoftware.Utility.MyUtils;
import com.freightsoftware.Utility.NavigatorManager;
import com.freightsoftware.fragment.ClockIn;
import com.freightsoftware.fragment.HomeFragment;
import com.freightsoftware.model.GsonCompleted;
import com.freightsoftware.model.GsonContainerList;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.freightsoftware.Constants.Constants.logID;

public class HomeOld extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, RefreshListener {

    public static final HashMap<String, LatLng> LANDMARKS = new HashMap<String, LatLng>();
    //play Service Laction
    private static final int MY_PERMISSION_REQUEST_CODE = 2204;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 1989;
    private static final String TAG = "Home";
    public static int notifyID = 9001;
    public static String ADMIN_CHANNEL_ID = "com.freightsoftware";
    public static DrawerLayout drawer_layout;
    public static List<GsonContainerList> list_container = new ArrayList<>();
    private static int UPDATE_INTERVAL = 5000;
    private static int FATEST_INTERVAL = 3000;
    private static int DISPLACEMET = 10;
    protected ArrayList<Geofence> mGeofenceList;
    DatabaseReference ref;
    GeoFire geoFire;
    Marker mCurrent;
    NavigationView navigation_view;
    Bundle bundle = new Bundle();
    String id;
    //    ProgressBar cpb;
    //Geo Fencing
    private GoogleMap mMap;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private Double lastLat = 0.0, lastLng = 0.0;
    /*header view*/
    private ImageView iv_user;
    private TextView tv_username, tv_home, tv_clock, tv_logout;
    private View view_home, view_clock, view_logout;
    private FrameLayout fl_main;
    private FragmentManager mFragmentManager;
    private FragmentTransaction ft;
    private Dialog CustomDialog, DialogLogout;
    private DisplayMetrics metrics;
    /*Dialog*/
    private ImageView img_close;
    private TextView tv_yes, tv_no, tv_title_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        if (getIntent() != null) {
            if (getIntent().getExtras().getInt(Constants.push) == Constants.fromPush) {
//                MyUtils.makeToast("From Push");
            } else {
                callContainerAPI();
            }
        }
        ShipmentComplete.onRefreshClickListener(this);
        //intialization of map
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        ref = FirebaseDatabase.getInstance().getReference("MyLocation");
        geoFire = new GeoFire(ref);

        setUpLocation();
        if ((App.Utils.getString(Constants.user_id)).equalsIgnoreCase("")) {
            id = App.Utils.getString(Constants.userID);

        } else {
            id = App.Utils.getString(Constants.user_id);
        }


        findViewById();
        OnClickListener();
        getHomeFragment();
        main();
    }

    private void callContainerAPI() {

        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.driverID, App.Utils.getString(Constants.driverID));

            LogUtil.Print(TAG, "ContainerList ApiRequest params Home" + params);
//            Api. service = retrofit.create(RetrofitArrayAPI.class);
            Call<List<GsonContainerList>> call = request.getContainerList(params);


            call.enqueue(new Callback<List<GsonContainerList>>() {
                @Override
                public void onResponse(Call<List<GsonContainerList>> call, Response<List<GsonContainerList>> response) {
//                    cpb.setVisibility(View.GONE);
                    List<GsonContainerList> gson = response.body();

                    if (response.body() == null) {

                    } else {
                        if (list_container.size() > 0)
                            list_container.clear();

                        list_container.addAll(gson);
                        //get Latitude,Longitute from API
                        int ldata = 1;
                        for (int i = 0; i < list_container.size(); i++) {
                            if (list_container.get(i).getToCoordinates() == null) {

                            } else {

                                String latlng = list_container.get(i).getToCoordinates();
                                String[] lat = latlng.split(":");
                                String lati = lat[0];
                                String longe = lat[1];
                                LogUtil.Print(TAG, "Lat====" + lati + "Lng====" + longe);
                                LANDMARKS.put(list_container.get(i).getId() + ":" + list_container.get(i).getControlNumber() + ":" + String.valueOf("ToLocation" + ldata), new LatLng(Double.valueOf(lati), Double.valueOf(longe)));
//                            Controler_number.put(String.valueOf("FromLocation:"+ldata), String.valueOf(list_container.get(i).getControlNumber()));
                                ldata++;
                            }
                            if (list_container.get(i).getFromCoordinates() != null) {
                                String fromlatlog = list_container.get(i).getFromCoordinates();
                                String[] fromlat = fromlatlog.split(":");
                                String fromati = fromlat[0];
                                String fromlonge = fromlat[1];
                                LANDMARKS.put(list_container.get(i).getId() + ":" + list_container.get(i).getControlNumber() + ":" + String.valueOf("FromLocation" + ldata), new LatLng(Double.valueOf(fromati), Double.valueOf(fromlonge)));
//                            Controler_number.put(String.valueOf("FromLocation:"+ldata), String.valueOf(list_container.get(i).getControlNumber()));
                                ldata++;
                            }


                        }

                        LogUtil.Print(TAG, "Landmark:--" + LANDMARKS);
                        LogUtil.Print(TAG, "Landmark:--" + list_container);
                        onMapReady(mMap);

                    }

                }

                @Override
                public void onFailure(Call<List<GsonContainerList>> call, Throwable t) {
                    if (list_container.size() == 0)
                        LogUtil.Print("===ContainerList====", t.getMessage());
                }
            });


        } else {
            MyUtils.ShowAlert(HomeOld.this, getResources().getString(R.string.text_internet), getResources().getString(R.string.app_name));
        }
    }

    private void callDrawCircle() {
        LatLng dangerous_area = null;
        String con = null;
        for (final Map.Entry<String, LatLng> entry : LANDMARKS.entrySet()) {
            //Create Dangerous Area
            dangerous_area = new LatLng(entry.getValue().latitude, entry.getValue().longitude);

            mMap.addCircle(new CircleOptions()
                    .center(dangerous_area)
                    .radius(100) //in meter
                    .fillColor(0x220000FF)
                    .strokeColor(Color.RED)
                    .strokeWidth(5.0f)
            );

            String enterloc = entry.getKey();
            String[] enter = enterloc.split(":");
            String SentNdata = enter[0];
            String controler_number = enter[1];
            LogUtil.Print("SentNdata: ", SentNdata + "controler_number: " + controler_number);

            //Add GeoQuery here
            //0.1f = 0.1km = 100 m
            GeoQuery geoQuery = geoFire.queryAtLocation(new GeoLocation(dangerous_area.latitude, dangerous_area.longitude), 0.1f);
            geoQuery.addGeoQueryEventListener(new GeoQueryEventListener() {
                @Override
                public void onKeyEntered(String key, GeoLocation location) {

                    String enterloc = entry.getKey();
                    String[] enter = enterloc.split(":");
                    String id = enter[0];
                    String controler_number = enter[1];
                    String from = MyUtils.removelastcharacter(enter[2]);

                    if (from.equals("FromLocation")) {
                        CallShipmentLogAPI(controler_number, Constants.arrivedFromLocation);

                    } else if (from.equals("ToLocation")) {
                        CallShipmentLogAPI(controler_number, Constants.arrivedtoLocation);
                    }
                    CallsaveDriverGeoFenceAPI(id, "0");
//                    MyUtils.sendNotification(Home.this,getResources().getString(R.string.app_name), String.format("%s Near By %s area", MyUtils.getString(Constants.username), SentNdata));
                    MyUtils.sendNotification(HomeOld.this, getResources().getString(R.string.app_name), String.format("%s Please Report To Dispatch Arrival", MyUtils.getString(Constants.username)));
                }

                @Override
                public void onKeyExited(String key) {
                    String enterloc = entry.getKey();
                    String[] enter = enterloc.split(":");
                    String id = enter[0];
                    String controler_number = enter[1];
                    String from = MyUtils.removelastcharacter(enter[2]);
                    if (from.equals("FromLocation")) {
                        CallShipmentLogAPI(controler_number, Constants.leaveFromLocation);
                    } else if (from.equals("ToLocation")) {
                        CallShipmentLogAPI(controler_number, Constants.leavetoLocation);
                    }
                    CallsaveDriverGeoFenceAPI(id, "1");
                    LogUtil.Print("EXIT", String.format("%s is no longer in the the dangerous area", entry.getKey()));
                    MyUtils.sendNotification(HomeOld.this, getResources().getString(R.string.app_name), String.format("%s Please Report To Dispatch Departure", MyUtils.getString(Constants.username)));
//                    sendNotification("GeoFences", String.format("%s is no longer in the the dangerous area", entry.getKey()));
                }

                @Override
                public void onKeyMoved(String key, GeoLocation location) {
                    Log.d("MOVE", String.format("%s moved within the dangerous area [%f/%f]", key, location.latitude, location.longitude));
                }

                @Override
                public void onGeoQueryReady() {

                }

                @Override
                public void onGeoQueryError(DatabaseError error) {
                    Log.e("ERROR", "" + error);
                }
            });
        }

    }

    private void CallsaveDriverGeoFenceAPI(String id, String s) {

        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.id, id);
            params.put(Constants.logType, s);
            LogUtil.Print(TAG, "CallsaveDriverGeoFenceAPI" + params);

            Call<GsonCompleted> call = request.saveDriverGeoFence(params);
//            cpb.setVisibility(View.VISIBLE);
            call.enqueue(new Callback<GsonCompleted>() {
                @Override
                public void onResponse(Call<GsonCompleted> call, Response<GsonCompleted> response) {
                    LogUtil.Print("response", response.toString());


                    GsonCompleted gson = response.body();
                    LogUtil.Print(TAG, "Responce===" + response.body());
//                    cpb.setVisibility(View.GONE);
                    if (gson.getStatus() == 0) {
                        LogUtil.Print(TAG, "Falied to Submit Geo Fences: " + gson.getStatus());
//                        MyUtils.ShowAlert(Home.this, "Falied to Submit Geo Fences", getResources().getString(R.string.app_name));
                    } else {
                        LogUtil.Print(TAG, "Submit Geo Fences: " + gson.getStatus());
                    }
                }


                @Override
                public void onFailure(Call<GsonCompleted> call, Throwable t) {
                    LogUtil.Print("Error", t.getMessage());
//                    cpb.setVisibility(View.GONE);
                    LogUtil.Print(TAG, "Failure ");
                }
            });

        } else {

            MyUtils.ShowAlert(HomeOld.this, getResources().getString(R.string.text_internet), getResources().getString(R.string.app_name));
        }

    }

    private void main() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer_layout, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            public void onDrawerClosed(View view) {
                /*getActionBar().setTitle(mTitle);*/
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                /*getActionBar().setTitle(mDrawerTitle);*/
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);


                float xPositionOpenDrawer = navigation_view.getWidth();
                float xPositionWindowContent = (slideOffset * xPositionOpenDrawer);
                fl_main.setX(xPositionWindowContent);
                //drawermenu.setX(xPositionWindowContent);
                fl_main.setX(xPositionWindowContent);
            }
        };
        drawer_layout.setDrawerListener(toggle);
        toggle.syncState();


    }

    private void findViewById() {

        drawer_layout = (DrawerLayout) findViewById(R.id.drawer_layout);
        fl_main = findViewById(R.id.fl_main);
//        cpb = findViewById(R.id.cpb);
        navigation_view = findViewById(R.id.navigation_view);

        /*drawer*/
        iv_user = (ImageView) findViewById(R.id.iv_user);
        tv_username = findViewById(R.id.tv_username);
        tv_home = findViewById(R.id.tv_home);
        tv_clock = findViewById(R.id.tv_clock);
        tv_logout = findViewById(R.id.tv_logout);

        view_home = findViewById(R.id.view_home);
        view_clock = findViewById(R.id.view_clock);
        view_logout = findViewById(R.id.view_logout);

        if ((App.Utils.getString(Constants.username)).equalsIgnoreCase("")) {

        } else {
            tv_username.setText(App.Utils.getString(Constants.username));
        }

    }

    /**
     * OnClickListener of Views
     */
    private void OnClickListener() {

        tv_home.setOnClickListener(this);
        tv_clock.setOnClickListener(this);
        tv_logout.setOnClickListener(this);

    }

    /**
     * get Home Fragment initially
     */
    private void getHomeFragment() {
        setTabBackgroundColor(1);
        drawer_layout.closeDrawer(GravityCompat.START);
        bundle.putString(Constants.user_id, String.valueOf(id));
        Fragment fragment = new HomeFragment();
        fragment.setArguments(bundle);

        Fragment_Replace(fragment);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.tv_home:
                setTabBackgroundColor(1);
                drawer_layout.closeDrawer(GravityCompat.START);
                bundle.putString(Constants.user_id, String.valueOf(id));
                Fragment fragment = new HomeFragment();
                fragment.setArguments(bundle);
                Fragment_Replace(fragment);
                break;

            case R.id.tv_clock:
                setTabBackgroundColor(2);
                SideMenuSelection(0);
                break;

            case R.id.tv_logout:
//                setTabBackgroundColor(3);
                SideMenuSelection(1);
                break;
            case R.id.tv_no:
                drawer_layout.closeDrawers();
                if (DialogLogout.isShowing()) {
                    DialogLogout.dismiss();
                }
                break;
            case R.id.img_close:
                drawer_layout.closeDrawers();
                if (DialogLogout.isShowing()) {
                    DialogLogout.dismiss();
                }
                break;
            case R.id.tv_yes:
                drawer_layout.closeDrawers();

                CallLogoutAPI();

                if (DialogLogout.isShowing()) {
                    DialogLogout.dismiss();
                }

//                CallLogoutApi();
                break;

        }

    }

    private void CallLogoutAPI() {
        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
//            params.put(Constants.controlNumber, App.createRequestBody(String.valueOf(getIntent().getExtras().getInt(Constants.controlNumber))));

            params.put(Constants.userID, id);


            LogUtil.Print("Logout_params", "" + params);

            Call<GsonCompleted> call = request.driverLogout(params);
//            cpb.setVisibility(View.VISIBLE);
            call.enqueue(new Callback<GsonCompleted>() {
                @Override
                public void onResponse(Call<GsonCompleted> call, Response<GsonCompleted> response) {
                    LogUtil.Print("response", response.toString());


                    GsonCompleted gson = response.body();
                    LogUtil.Print(TAG, "Responce===" + response.body());
//                    cpb.setVisibility(View.GONE);
                    if (response.body() == null) {
                        MyUtils.ShowAlert(HomeOld.this, "Falied to Logout", getResources().getString(R.string.app_name));
                    } else {
                        if (gson.getStatus() == 1) {
                            stopLocationUpdate();
                            int newLogID = App.Utils.getInt(logID);
                            String username = MyUtils.getString(Constants.userEmail);
                            String password = MyUtils.getString(Constants.userPassword);
                            newLogID++;
                            MyUtils.ClearAllPreferences();
                            MyUtils.putString(Constants.userEmail, username);
                            MyUtils.putString(Constants.userPassword, password);
                            MyUtils.putInt(logID, newLogID);
                            NavigatorManager.startNewActivity(HomeOld.this, new Intent(HomeOld.this, Login.class));
                            finish();
                        }
                    }
                }


                @Override
                public void onFailure(Call<GsonCompleted> call, Throwable t) {
                    LogUtil.Print("Error", t.getMessage());
//                    cpb.setVisibility(View.GONE);
                    LogUtil.Print(TAG, "Failure ");
                }
            });

        } else {

            MyUtils.ShowAlert(HomeOld.this, getResources().getString(R.string.text_internet), getResources().getString(R.string.app_name));
        }
    }

    private void setTabBackgroundColor(int i) {
        view_home.setVisibility((i == 1) ? View.VISIBLE : View.GONE);
        view_clock.setVisibility((i == 2) ? View.VISIBLE : View.GONE);
//        view_logout.setVisibility((i == 3) ? View.VISIBLE : View.GONE);

        int intClr = ContextCompat.getColor(HomeOld.this, R.color.nav_color);

        tv_home.setBackgroundColor((i == 1) ? intClr : 0);
        tv_clock.setBackgroundColor((i == 2) ? intClr : 0);
//        tv_logout.setBackgroundColor((i == 3) ? intClr : 0);
    }

    private void SideMenuSelection(int i) {
        if (i == 0) {
            drawer_layout.closeDrawers();
//            Intent geo = new Intent(Home.this, MapsActivity.class);
//            NavigatorManager.startNewActivity(Home.this, geo);
            bundle.putString(Constants.user_id, String.valueOf(id));
            Fragment fragment = new ClockIn();
            fragment.setArguments(bundle);
            Fragment_Replace(fragment);
        } else if (i == 1) {
            /* Logout */
            Dialog_Logout();
        }
    }

    /**
     * method for replacement of fragment
     *
     * @param fragment
     */
    public void Fragment_Replace(Fragment fragment) {
        mFragmentManager = getSupportFragmentManager();
        ft = mFragmentManager.beginTransaction();
        ft.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        ft.replace(R.id.fl_main, fragment);
        ft.addToBackStack(fragment.getTag());
        ft.commit();

    }

    @Override
    public void onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START);
        } else {
            if (mFragmentManager.getBackStackEntryCount() == 0) {
                // and then you define a method allowBackPressed with the logic to allow back pressed or not
                if (mFragmentManager.findFragmentById(R.id.fl_main) instanceof HomeFragment) {
                    finish();
                } else
                    super.onBackPressed();
            } else {
                if (mFragmentManager.findFragmentById(R.id.fl_main) instanceof ClockIn) {
                    getHomeFragment();
                } else if (mFragmentManager.findFragmentById(R.id.fl_main) instanceof HomeFragment) {
                    finish();
                } else
                    mFragmentManager.popBackStack();
            }
        }
    }

    /**
     * Dialog for Logout
     */
    private void Dialog_Logout() {
        metrics = getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        DialogLogout = new Dialog(HomeOld.this);
        DialogLogout.requestWindowFeature(Window.FEATURE_NO_TITLE);
        DialogLogout.setCancelable(true);
        DialogLogout.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        DialogLogout.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        DialogLogout.setContentView(R.layout.dialog_layout);
        img_close = (ImageView) DialogLogout.findViewById(R.id.img_close);
        tv_title_text = (TextView) DialogLogout.findViewById(R.id.tv_title_text);
        tv_yes = (TextView) DialogLogout.findViewById(R.id.tv_yes);
        tv_no = (TextView) DialogLogout.findViewById(R.id.tv_no);
        tv_title_text.setText(getResources().getString(R.string.text_are_you_sure_to_logout));
        img_close.setOnClickListener(this);
        tv_yes.setOnClickListener(this);
        tv_no.setOnClickListener(this);

        DialogLogout.show();
        DialogLogout.getWindow().setLayout((6 * width) / 7, ViewGroup.LayoutParams.WRAP_CONTENT);
        DialogLogout.getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_dialog_rounded));

    }

    //geo Fencing

    //Press ctr+O

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (checkPlayServices()) {
                        buildGoogleApiClient();
                        createLocationRequest();
                        displayLocation();
                    }
                }
                break;
        }
    }

    private void setUpLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.ACCESS_CHECKIN_PROPERTIES,
                    Manifest.permission.ACCESS_FINE_LOCATION
            }, MY_PERMISSION_REQUEST_CODE);
        } else {
            if (checkPlayServices()) {
                buildGoogleApiClient();
                createLocationRequest();
                displayLocation();
            }
        }

    }

    private void displayLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            final double latitude = mLastLocation.getLatitude();
            final double longitude = mLastLocation.getLongitude();

            //Update to Firebase
            geoFire.setLocation(MyUtils.getString(Constants.driverID), new GeoLocation(latitude, longitude),
                    new GeoFire.CompletionListener() {
                        @Override
                        public void onComplete(String key, DatabaseError error) {
                            //Add marker
                            if (mCurrent != null)
                                mCurrent.remove(); //remove old marker
                            mCurrent = mMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(latitude, longitude))
                                    .title("You"));
                            //Move Camera to this poaition
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 12.0f));
                        }
                    });


            Log.d("GeoFences", String.format("Your location was changes : %f / %f", latitude, longitude));

        } else {
            Log.d("GeoFences", "can not get your location");
        }
    }

    private void SaveDriverLatLon(double latitude, double longitude) {
        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.driverID, App.Utils.getString(Constants.driverID));
            params.put(Constants.latitude, "" + latitude);
            params.put(Constants.longitude, "" + longitude);
//            params.put(Constants.driverID, App.Utils.getString(Constants.driverID));

            LogUtil.Print("SaveDriverLatLon_params", "" + params);

            Call<GsonCompleted> call = request.saveDriverLatLon(params);
//            cpb.setVisibility(View.VISIBLE);
            call.enqueue(new Callback<GsonCompleted>() {
                @Override
                public void onResponse(Call<GsonCompleted> call, Response<GsonCompleted> response) {
                    LogUtil.Print("response", response.toString());


                    GsonCompleted gson = response.body();
//                    cpb.setVisibility(View.GONE);
                    if (response.body() == null) {
                        MyUtils.ShowAlert(HomeOld.this, "Falied to Save Driver Location", getResources().getString(R.string.app_name));
                    } else {
//                        MyUtils.makeToast("success");
                    }
                }

                @Override
                public void onFailure(Call<GsonCompleted> call, Throwable t) {
                    LogUtil.Print("Error", t.getMessage());
//                    cpb.setVisibility(View.GONE);
                    LogUtil.Print(TAG, "Failure ");
                }
            });

        } else {

            MyUtils.ShowAlert(HomeOld.this, getResources().getString(R.string.text_internet), getResources().getString(R.string.app_name));
        }
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FATEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(DISPLACEMET);
    }

    private void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode))
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            else {
                Toast.makeText(this, "This Device is not supported", Toast.LENGTH_SHORT).show();
                finish();
            }
            return false;
        }
        return true;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (getIntent() != null) {
            if (getIntent().getExtras().getInt(Constants.push) == Constants.fromPush) {
//                MyUtils.makeToast("From Push");
            } else {
                if (!MyUtils.getString(Constants.driverID).equals("")) {
                    callDrawCircle();
                }
            }
        }


    }


    private void CallShipmentLogAPI(String controler_number, String logID) {
        if (App.Utils.IsInternetOn()) {

            ApiInterface request = Api.retrofit.create(ApiInterface.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.userID, id);
            params.put(Constants.controlNumber, controler_number);
            params.put(Constants.userName, App.Utils.getString(Constants.username));
            params.put(Constants.logID, logID);
            LogUtil.Print(TAG, "ShipmentLg_Submit_params" + params);

            Call<GsonCompleted> call = request.saveShipmentLog(params);
//            cpb.setVisibility(View.VISIBLE);
            call.enqueue(new Callback<GsonCompleted>() {
                @Override
                public void onResponse(Call<GsonCompleted> call, Response<GsonCompleted> response) {
                    LogUtil.Print("response", response.toString());


                    GsonCompleted gson = response.body();
                    LogUtil.Print(TAG, "Responce===" + response.body());
//                    cpb.setVisibility(View.GONE);
                    if (response.body() == null) {
                        MyUtils.ShowAlert(HomeOld.this, "Falied to Submit Shipment Log", getResources().getString(R.string.app_name));
                    } else {
                        int newLogID = App.Utils.getInt(Constants.logID);
                        newLogID++;
                        MyUtils.putInt(Constants.logID, newLogID);
                        LogUtil.Print(TAG, "Shimpment Status: " + gson.getStatus());
//                        MyUtils.makeToast("Shiment Log Successfully");
                    }
                }


                @Override
                public void onFailure(Call<GsonCompleted> call, Throwable t) {
                    LogUtil.Print("Error", t.getMessage());
//                    cpb.setVisibility(View.GONE);
                    LogUtil.Print(TAG, "Failure ");
                }
            });

        } else {

            MyUtils.ShowAlert(HomeOld.this, getResources().getString(R.string.text_internet), getResources().getString(R.string.app_name));
        }

    }

    /*private void sendNotification(String title, String content) {
        Notification.Builder builder = new Notification.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setContentTitle(title)
                .setContentText(content);
        NotificationManager manager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        Intent intent = new Intent(this, Home.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_IMMUTABLE);
        builder.setContentIntent(contentIntent);
        Notification notification = builder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.defaults |= Notification.DEFAULT_SOUND;

        manager.notify(new Random().nextInt(), notification);
    }*/

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mLastLocation != null) {
            if (!MyUtils.getString(Constants.driverID).equals("")) {
                if (getIntent().getExtras().getInt(Constants.push) == Constants.fromPush) {

                } else {
                    if (lastLat != location.getLatitude() && lastLng != location.getLongitude()) {
                        //check app is background
                        lastLat = location.getLatitude();
                        lastLng = location.getLongitude();
//                double latitude = mLastLocation.getLatitude();
//                double longitude = mLastLocation.getLongitude();

                        SaveDriverLatLon(lastLat, lastLng);
                    }
                }

            }

        }
        displayLocation();
    }

    private void stopLocationUpdate() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);

    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        LogUtil.Print(TAG, "onConnected");
        displayLocation();
        startLocationUpdates();
    }

    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

    }


    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LogUtil.Print(TAG, "Destroy");
        mGoogleApiClient.connect();

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onItemClickRefresh(boolean flag) {
        if (flag) {
            list_container.clear();
            callContainerAPI();
        }
    }
}

